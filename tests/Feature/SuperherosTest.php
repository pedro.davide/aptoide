<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SuperherosTest extends TestCase
{
    public function testCreateSuperheroAndList()
    {
        $this
            ->post('/api/superheros', [
                'name' => 'test',
                'nickname' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/superheros')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateSuperheroAndListWithFilters()
    {
        $this
            ->post('/api/superheros', [
                'name' => 'test',
                'nickname' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/superheros?name=xpto')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateSuperhero() {
        $this
            ->post('/api/superheros', [
                'nickname' => 'test',
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('superheros', ['name' => 'test']);
    }

    public function testCreateSuperheroWithTeamsAndPower() {
        $this
            ->createTeam('xpto', $team)
            ->createPower('xpto', $power)
            ->post('/api/superheros', [
                'nickname' => 'test',
                'name' => 'test',
                'teams' => [
                    $team->toArray()
                ],
                'powers' => [
                    $power->toArray()
                ]
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('superheros', ['name' => 'test']);
    }

    public function testShowSuperhero() {
        $this
            ->createSuperhero($superhero, 'test','testnickname')
            ->get('/api/superheros/' . $superhero->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }

    public function testUpdateSuperhero() {
        $this
            ->createSuperhero($superhero, 'test','testnickname')
            ->put('/api/superheros/' . $superhero->id, ['name' => 'xpto'])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('superheros', ['id' => $superhero->id, 'name' => 'xpto']);
    }

    public function testDeleteSuperhero() {
        $this
            ->createSuperhero($superhero, 'test', 'testnickname')
            ->delete('/api/superheros/' . $superhero->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }
}
