<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PowersTest extends TestCase
{
    public function testCreatePowersAndList()
    {
        $this
            ->post('/api/powers', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/powers')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreatePowersAndListWithFilters()
    {
        $this
            ->post('/api/powers', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/powers?name=xpto')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreatePower() {
        $this
            ->post('/api/powers', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('powers', ['name' => 'test']);
    }

    public function testShowPower() {
        $this
            ->createPower('test', $power)
            ->get('/api/powers/' . $power->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }

    public function testUpdatePower() {
        $this
            ->createPower('test', $power)
            ->put('/api/powers/' . $power->id, ['name' => 'xpto'])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('powers', ['id' => $power->id, 'name' => 'xpto']);
    }

    public function testDeletePower() {
        $this
            ->createPower('test', $power)
            ->delete('/api/powers/' . $power->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }
}
