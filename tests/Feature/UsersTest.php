<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{

    public function testCreateUsersAndList()
    {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->post('/users', [
                'name' => 'test',
                'email' => $email . '@s.com',
                'password' => '12345678',
                'password_confirmation' => '12345678'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/users')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateUsersAndListWithFilters()
    {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->post('/users', [
                'name' => 'test',
                'email' => $email . '@s.com',
                'password' => '12345678',
                'password_confirmation' => '12345678'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/users?name=xpto')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateUser() {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->post('/users', [
                'name' => 'test',
                'email' => $email . '@s.com',
                'password' => '12345678',
                'password_confirmation' => '12345678'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('users', ['name' => 'test']);
    }

    public function testShowUser() {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->createUser('test', $email . '@x.com','123$5678', $user)
            ->get('/users/' . $user->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }

    public function testUpdateUser() {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->createUser('test', $email . '@x.com','123$5678', $user)
            ->put('/users/' . $user->id, ['name' => 'xpto'])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('users', ['id' => $user->id, 'name' => 'xpto']);
    }

    public function testDeleteUser() {
        $email = uniqid();

        $this
            ->asAdmin($user)
            ->createUser('test', $email . '@x.com','123$5678', $user)
            ->delete('/users/' . $user->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }

}
