<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeamsTest extends TestCase
{
    public function testCreateTeamsAndList()
    {
        $this
            ->post('/api/teams', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/teams')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateTeamsAndListWithFilters()
    {
        $this
            ->post('/api/teams', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this
            ->get('/api/teams?name=xpto')
            ->assertStatus(200)
            ->assertJsonStructure(['items', 'result'])
            ->isOk();
    }

    public function testCreateTeam() {
        $this
            ->post('/api/teams', [
                'name' => 'test'
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('teams', ['name' => 'test']);
    }

    public function testShowTeam() {
        $this
            ->createTeam('test', $team)
            ->get('/api/teams/' . $team->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }

    public function testUpdateTeam() {
        $this
            ->createTeam('test', $team)
            ->put('/api/teams/' . $team->id, ['name' => 'xpto'])
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();

        $this->assertDatabaseHas('teams', ['id' => $team->id, 'name' => 'xpto']);
    }

    public function testDeleteTeam() {
        $this
            ->createTeam('test', $team)
            ->delete('/api/teams/' . $team->id)
            ->assertStatus(200)
            ->assertJsonStructure(['item', 'result'])
            ->isOk();
    }
}
