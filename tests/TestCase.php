<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createTeam($name, &$team){
        $repo = $this->app->make(\App\Repos\TeamsRepo::class);

        $team = $repo->save([
            'name' => $name
        ])->getResult();

        return $this;
    }

    public function createPower($name, &$power){
        $repo = $this->app->make(\App\Repos\PowersRepo::class);

        $power = $repo->save([
            'name' => $name
        ])->getResult();

        return $this;
    }

    public function createUser($name, $email, $password, &$user){
        $repo = $this->app->make(\App\Repos\UsersRepo::class);

        $user = $repo->save([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ])->getResult();

        return $this;
    }

    public function createSuperhero(&$superhero, $name, $nickname, $teams = [], $powers = []){
        $repo = $this->app->make(\App\Repos\SuperherosRepo::class);

        $superhero = $repo->save([
            'name' => $name,
            'nickname' => $nickname,
            'teams' => $teams,
            'powers' => $powers
        ])->getResult();

        return $this;
    }

    public function asAdmin(&$user) {
        $this->createUser('admin', uniqid() . '@s.com','123456' , $user);
        $this->actingAs($user);
        return $this;
    }
}
