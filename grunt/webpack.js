'use strict';

module.exports = function (path, webpack) {
  return {
    src: {
      entry: {
        vendor: path.resolve(__dirname, '../public/modules/vendor.js'),
        app: path.resolve(__dirname, '../public/modules/app.js')
      },
      output: {
        path: path.resolve(__dirname, '../public/bundles/'),
        publicPath: 'bundles/',
        filename: '[name].js'
      },
      resolve: {
        alias: {
          'moduleDir':            path.resolve(__dirname, '../public/modules'),
          'componentDir':         path.resolve(__dirname, '../public/components'),
          'bowerDir':             path.resolve(__dirname, '../bower_components'),

          'angular': path.resolve(__dirname, '../bower_components/angular/angular'),
          'angular-animate': path.resolve(__dirname, '../bower_components/angular-animate/angular-animate'),
          'angular-bootstrap': path.resolve(__dirname, '../bower_components/angular-bootstrap/ui-bootstrap-tpls'),
          'angular-bootstrap-nav-tree': path.resolve(__dirname, '../bower_components/angular-bootstrap-nav-tree/dist/abn_tree_directive'),
          'angular-echarts': path.resolve(__dirname, '../bower_components/angular-echarts/dist/angular-echarts'),
          'angular-file-upload': path.resolve(__dirname, '../bower_components/angular-file-upload/angular-file-upload'),
          'angular-resource': path.resolve(__dirname, '../bower_components/angular-resource/angular-resource'),
          'angular-sanitize': path.resolve(__dirname, '../bower_components/angular-sanitize/angular-sanitize'),
          'angular-strap': path.resolve(__dirname, '../bower_components/angular-strap/dist/angular-strap'),
          'angular-summernote': path.resolve(__dirname, '../bower_components/angular-summernote/dist/angular-summernote'),
          'angular-touch': path.resolve(__dirname, '../bower_components/angular-touch/angular-touch'),
          'angular-ui-calendar': path.resolve(__dirname, '../bower_components/angular-ui-calendar/src/calendar'),
          'angular-ui-grid': path.resolve(__dirname, '../bower_components/angular-ui-grid/ui-grid'),
          'angular-ui-map': path.resolve(__dirname, '../bower_components/angular-ui-map/ui-map'),
          'angular-ui-router': path.resolve(__dirname, '../bower_components/angular-ui-router/release/angular-ui-router'),
          'angular-ui-select': path.resolve(__dirname, '../bower_components/angular-ui-select/dist/select'),
          'angular-ui-utils': path.resolve(__dirname, '../bower_components/angular-ui-utils/ui-utils'),
          'angular-xeditable': path.resolve(__dirname, '../bower_components/angular-xeditable/dist/js/xeditable'),
          'ngstorage': path.resolve(__dirname, '../bower_components/ngstorage/ngStorage'),
          'angular-breadcrumb': path.resolve(__dirname, '../bower_components/angular-breadcrumb/dist/angular-breadcrumb'),
          'angular-bootstrap-show-errors': path.resolve(__dirname, '../bower_components/angular-bootstrap-show-errors/src/showErrors'),
          'angular-confirm-field': path.resolve(__dirname, '../bower_components/angular-confirm-field/app/package/js/angular-confirm-field'),
          'ng-gadaterangepicker': path.resolve(__dirname, '../bower_components/ng-gadaterangepicker/src/js/gaDatePickerRange'),
          'ng-file-upload': path.resolve(__dirname, '../bower_components/ng-file-upload/ng-file-upload-all'),
          'angular-loading-bar': path.resolve(__dirname, '../bower_components/angular-loading-bar/build/loading-bar'),
          'angular-drag-and-drop-lists': path.resolve(__dirname, '../bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists'),
          'ng-letter-avatar': path.resolve(__dirname, '../bower_components/ng-letter-avatar/ngletteravatar'),
          'echarts': path.resolve(__dirname, '../bower_components/echarts/build/dist/echarts-all'),
          'summernote': path.resolve(__dirname, '../bower_components/summernote/dist/summernote'),
          'angular-confirm': path.resolve(__dirname, '../bower_components/angular-confirm/dist/angular-confirm.min'),
          'angular-paging': path.resolve(__dirname, '../bower_components/angular-paging/dist/paging.min'),
          'angular-timeago': path.resolve(__dirname, '../bower_components/angular-timeago/dist/angular-timeago'),

          'nvd3':                 path.resolve(__dirname, '../bower_components/nvd3/build/nv.d3'),
          'angular-nvd3':                 path.resolve(__dirname, '../bower_components/angular-nvd3/dist/angular-nvd3'),

          'd3': path.resolve(__dirname, '../bower_components/d3/d3.min'),
          'rickshaw': path.resolve(__dirname, '../bower_components/rickshaw/rickshaw'),
          'angular-rickshaw': path.resolve(__dirname, '../bower_components/angular-rickshaw/rickshaw'),

          'angular-send-feedback': path.resolve(__dirname, '../bower_components/angular-send-feedback/dist/angular-send-feedback'),
          'html2canvas': path.resolve(__dirname, '../bower_components/html2canvas/build/html2canvas'),

          'ng-dialog': path.resolve(__dirname, '../bower_components/ng-dialog/js/ngDialog.min'),
          'angular-img-cropper': path.resolve(__dirname, '../bower_components/angular-img-cropper/dist/angular-img-cropper.min'),

          'angular-nestable': path.resolve(__dirname, '../bower_components/angular-nestable/src/angular-nestable'),
          'angular-ui-tree': path.resolve(__dirname, '../bower_components/angular-ui-tree/dist/angular-ui-tree.min'),

          'jquery': path.resolve(__dirname, '../bower_components/jquery/dist/jquery'),
          'jquery.nestable': path.resolve(__dirname, '../bower_components/jquery.nestable/jquery.nestable'),
          'tether': path.resolve(__dirname, '../bower_components/tether/dist/js/tether'),
          'bootstrap': path.resolve(__dirname, '../bower_components/bootstrap/dist/js/bootstrap'),
          'pace': path.resolve(__dirname, '../bower_components/PACE/pace'),

          'moment': path.resolve(__dirname, '../bower_components/moment/moment.js'),
          'underscore': path.resolve(__dirname, '../bower_components/underscore/underscore.js'),
          'lodash': path.resolve(__dirname, '../bower_components/lodash/dist/lodash'),

          'angular-bootstrap-colorpicker': path.resolve(__dirname, '../bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min'),
          'angular-multi-select': path.resolve(__dirname, '../bower_components/isteven-angular-multiselect/isteven-multi-select'),

          'datetimepicker': path.resolve(__dirname, '../bower_components/angularjs-bootstrap-datetimepicker/src/js/datetimepicker'),
          'datetimepicker.templates': path.resolve(__dirname, '../bower_components/angularjs-bootstrap-datetimepicker/src/js/datetimepicker.templates'),
          'ng-tags-input': path.resolve(__dirname, '../bower_components/ng-tags-input/ng-tags-input')
        },
        extensions: ['', '.js', '.json']
      },

      plugins: [
        new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.js' })
      ],

      module: {
        loaders: [
          { test: /\.css$/, loader: 'style-loader!css-loader' },
          { test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?prefix=font/&limit=5000&mimetype=application/font-woff' },
          { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader?prefix=font/' },
          //{ test: /\.js$/, loader: 'strip-loader?strip[]=console.log' },
          { test: /[\/]angular\.js$/, loader: 'exports?angular' },
          {
            test: /\.html$/,
            loader: 'html',
            exclude: /node_modules/
          },{
            test: /\.less$/,
            loader: 'style!css!less',
            exclude: /node_modules/
          }
        ]
      }
    }
  };
};