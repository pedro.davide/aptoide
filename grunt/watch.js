'use strict';

module.exports = function (path) {
  return {
    js: {
      files: [
        'gruntfile.js',
        'grunt/*.js',
        'public/modules/**/*.js',
        'public/components/**/*.js'
      ],
      tasks: ['jshint', 'clean:bundles', 'webpack:src'],
      options: {
        spawn : false,
        livereload: true
      }
    },
    less: {
      files: [
        'public/assets/styles/less/**/*.less',
        'public/components/**/*.less',
      ],
      tasks: ['less', 'autoprefixer', 'clean:bundles', 'webpack:src'],
      options: {
        spawn : false,
        livereload: true
      }
    },
    livereload: {
      options: {
        spawn : false,
        livereload: true
      },
      files: [
        'public/**/*.html',
        'public/assets/images/**/*.{png,jpg,jpeg,gif,webp,svg}'
      ]
    }
  };
};