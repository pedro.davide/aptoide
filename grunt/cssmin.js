'use strict';

module.exports = function (path) {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: 'public/dist/assets/styles/',
        src: ['*.css', '!*.min.css'],
        dest: 'public/dist/assets/styles/'
      }]
    },
    docs: {
      files: [{
        expand: true,
        cwd: 'docs/assets/styles/',
        src: ['*.css', '!*.min.css'],
        dest: 'docs/assets/styles/'
      }]
    }
  };
};