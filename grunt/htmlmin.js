'use strict';

module.exports = function (path) {
  return {
    dist: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      files: [{
        expand: true,
        cwd: 'public/dist/',
        src: ['**/*.html'],
        dest: 'public/dist/'
      }]
    }
  };
};