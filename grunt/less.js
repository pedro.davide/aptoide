'use strict';

module.exports = function (path) {
  return {
    app: {
      files: {
        'public/assets/styles/layout.css': 'public/assets/styles/less/layout.less',
        'public/assets/styles/component.css': 'public/assets/styles/less/component.less'
      }
    },

    libs: {
      expand: true,
      src: 'public/components/**/*.less',
      ext: '.css'
    }
  };
};