'use strict';

module.exports = function (path) {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: 'public/dist/assets/images/',
        src: ['**/*.{png,jpg,gif}'],
        dest: 'public/dist/assets/images/'
      }]
    }
  };
};