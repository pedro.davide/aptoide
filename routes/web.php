<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'HomeController@dashboard');

    Route::resource('users', 'UsersController');
    Route::post('/users/bulk/delete', 'UsersController@bulkDelete');
});

Route::group(['prefix' => 'api'], function () {
    Route::resource('powers', 'PowersController');
    Route::post('/powers/bulk/delete', 'PowersController@bulkDelete');
    Route::resource('superheros', 'SuperherosController');
    Route::post('/superheros/bulk/delete', 'SuperherosController@bulkDelete');
    Route::resource('teams', 'TeamsController');
    Route::post('/teams/bulk/delete', 'TeamsController@bulkDelete');
});