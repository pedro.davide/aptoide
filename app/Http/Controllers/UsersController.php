<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Libs\Filters\SearchFilter;
use App\Libs\RestResponse\RestResponse;
use App\Libs\UploadManager\UploadManager;
use App\Models\Action;
use App\Models\Resource;
use App\Models\SiteUser;
use App\Models\User;
use App\Repos\RolesRepo;
use App\Repos\SitesRepo;
use App\Repos\SiteUsersRepo;
use App\Repos\UsersRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class UsersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UsersRepo $usersRepo)
    {
        $filters = [
            new SearchFilter('name', $request->get('name'))
        ];

        return $this
            ->make(RestResponse::OK)
            ->items($usersRepo->getAll($filters, $request->pagination))
            ->json();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersRepo $usersRepo)
    {
        $result = $usersRepo->save($request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UsersRepo $usersRepo, $id)
    {
        $result = $usersRepo->get($id);

        return $this->make(RestResponse::OK)
            ->item($result)
            ->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersRepo $usersRepo, $id)
    {
        $result = $usersRepo->update($id, $request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersRepo $usersRepo, Request $request, $id)
    {
        $result = $usersRepo->delete($id);

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    public function bulkDelete(Request $request, UsersRepo $usersRepo)
    {
        $result = $usersRepo->bulkDelete($request->get('items'));

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }
}
