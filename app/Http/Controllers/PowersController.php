<?php

namespace App\Http\Controllers;

use App\Libs\Filters\SearchFilter;
use App\Libs\RestResponse\RestResponse;
use App\Repos\PowersRepo;
use Illuminate\Http\Request;

class PowersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PowersRepo $powersRepo)
    {
        $filters = [
            new SearchFilter('name', $request->get('name'))
        ];

        return $this
            ->make(RestResponse::OK)
            ->items($powersRepo->getAll($filters, $request->pagination))
            ->json();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PowersRepo $powersRepo)
    {
        $result = $powersRepo->save($request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PowersRepo $powersRepo, $id)
    {
        $result = $powersRepo->get($id);

        return $this->make(RestResponse::OK)
            ->item($result)
            ->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PowersRepo $powersRepo, $id)
    {
        $result = $powersRepo->update($id, $request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PowersRepo $powersRepo, $id)
    {
        $result = $powersRepo->delete($id);

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    public function bulkDelete(Request $request, PowersRepo $powersRepo)
    {
        $result = $powersRepo->bulkDelete($request->get('items'));

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }
}
