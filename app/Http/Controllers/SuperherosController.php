<?php

namespace App\Http\Controllers;

use App\Libs\Filters\SearchFilter;
use App\Libs\RestResponse\RestResponse;
use App\Repos\PowersRepo;
use App\Repos\SuperherosRepo;
use App\Repos\TeamsRepo;
use Illuminate\Http\Request;

class SuperherosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SuperherosRepo $superherosRepo)
    {
        $filters = [
            new SearchFilter('name', $request->get('name'))
        ];

        return $this
            ->make(RestResponse::OK)
            ->items($superherosRepo->getAll($filters, $request->pagination))
            ->json();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TeamsRepo $teamsRepo, PowersRepo $powersRepo)
    {
        return $this->make(RestResponse::OK)
            ->addCollection('teams', $teamsRepo->getAll())
            ->addCollection('powers', $powersRepo->getAll())
            ->json();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SuperherosRepo $superherosRepo)
    {
        $result = $superherosRepo->save($request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SuperherosRepo $superherosRepo, TeamsRepo $teamsRepo, PowersRepo $powersRepo, $id)
    {
        $result = $superherosRepo->get($id);

        return $this->make(RestResponse::OK)
            ->item($result)
            ->addCollection('teams', $teamsRepo->getAll())
            ->addCollection('powers', $powersRepo->getAll())
            ->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuperherosRepo $superherosRepo, $id)
    {
        $result = $superherosRepo->update($id, $request->all());

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuperherosRepo $superherosRepo, $id)
    {
        $result = $superherosRepo->delete($id);

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }

    public function bulkDelete(Request $request, SuperherosRepo $superherosRepo)
    {
        $result = $superherosRepo->bulkDelete($request->get('items'));

        return $this->make(RestResponse::OK)
            ->result($result)
            ->json();
    }
}
