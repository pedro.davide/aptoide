<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libs\RestResponse\RestResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class BaseController extends Controller
{
    protected function make($state = null)
    {
        return new RestResponse(Auth::user(), $state);
    }
}