<?php

namespace App\Http\Middleware;

use App\Libs\Http\Pagination;
use Closure;

class PaginationCapture {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $request->pagination = new Pagination(
            $request->get('page', 1),
            $request->get('limit', 30),
            $request->get('order','-id'),
            preg_match("/-(.*)/", $request->get('order','-id')) ? 'desc' : 'asc');
        return $next($request);
    }
}