<?php

namespace App\Models;

use App\Libs\BaseEloquentBuilder;
use App\Libs\BaseQueryBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

trait BaseModel
{
    /**
     * @return GuiidyEloquentBuilder
     */
    public static function query() {
        return (new static)->newQuery();
    }

    protected function newBaseQueryBuilder() {
        $conn = $this->getConnection();
        $grammar = $conn->getQueryGrammar();
        return new BaseQueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    public function carbon($field){
        return Carbon::createFromTimestampUTC(strtotime($this->$field));
    }
}