<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Superhero extends Model
{
    use ValidatingTrait;
    use BaseModel;
    use SoftDeletes;

    protected $table = 'superheros';

    protected $fillable = ['name', 'nickname', 'first_appearing_at'];

    protected $rules = [
        'name' => 'required',
        'nickname' => 'required'
    ];

    public function Powers() {
        return $this->belongsToMany(Power::class, 'superheros_powers');
    }

    public function Teams() {
        return $this->belongsToMany(Team::class, 'superheros_teams');
    }
}
