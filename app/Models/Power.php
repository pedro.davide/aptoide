<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Power extends Model
{
    use ValidatingTrait;
    use BaseModel;
    use SoftDeletes;

    protected $table = 'powers';

    protected $fillable = ['name'];

    protected $rules = [
        'name' => 'required'
    ];
}
