<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class SuperheroTeam extends Model
{
    use ValidatingTrait;
    use BaseModel;
    use SoftDeletes;

    protected $table = 'superheros_teams';

    protected $fillable = ['team_id', 'superhero_id'];

    protected $rules = [
        'team_id' => 'required',
        'superhero_id' => 'required'
    ];

    public function Team() {
        return $this->belongsTo(Team::class);
    }

    public function Superhero() {
        return $this->belongsTo(Superhero::class);
    }
}
