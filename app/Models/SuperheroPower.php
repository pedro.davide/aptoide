<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class SuperheroPower extends Model
{
    use ValidatingTrait;
    use BaseModel;
    use SoftDeletes;

    protected $table = 'superheros_powers';

    protected $fillable = ['power_id', 'superhero_id'];

    protected $rules = [
        'power_id' => 'required',
        'superhero_id' => 'required'
    ];

    public function Power() {
        return $this->belongsTo(Power::class);
    }

    public function Superhero() {
        return $this->belongsTo(Superhero::class);
    }
}
