<?php
namespace App\Libs\RestResponse;

use App\Libs\Repo\DeferredRepoResult;
use App\Libs\Repo\RepoActionResult;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 31/03/15
 * Time: 09:54
 */
class RestResponse {
    const OK = 'OK';
    const NOK = 'NOK';
    const DENIED = 'DENIED';

    private $statusCodes = [
        RestResponse::OK => 200,
        RestResponse::NOK => 422,
        RestResponse::DENIED => 401
    ];

    protected $state;
    protected $item;
    protected $items;

    protected $meta = [
        'collections' => [],
        'user' => null,
    ];
    protected $errors;

    public function __construct($user, $state = null) {
        $user = $user ?: new User();
        $userAttr = new \stdClass();
        $userAttr->impersonated = (Session::get('originalUser', null)) ? Session::get('originalUser', null) : null;
        $userAttr->avatar = md5($user->email);
        $userAttr->name = $user->name;
        $this->meta['user'] = $userAttr;
        $this->meta['env'] = config('app.env');
        $this->state = $state;
    }

    public function item($item) {
        $this->item = $item;
        return $this;
    }

    public function items($items, $total = null, $page = null, $perPage = null) {
        if ($items instanceof DeferredRepoResult) {
            $this->items = $items;
        } else {
            $this->items = [];
            $this->items['items'] = $items;
            if ($total != null) {
                $this->items['items_total'] = $total;
            }
            if ($perPage) {
                $this->items['items_perpage'] = $perPage;
            }
            if ($page) {
                $this->items['items_page'] = $page;
            }
        }

        return $this;
    }

    public function addCollection($name, $items) {
        $this->meta['collections'][$name] = $items;
        return $this;
    }

    public function result(RepoActionResult $result) {
        $this->item = $result->item;
        $this->errors = $result->errors;
        return $this;
    }

    public function errors($errors) {
        $this->errors = $errors;
        return $this;
    }

    public function json() {

        if ($this->errors) {
            return Response::json([
                'result' => RestResponse::NOK,
                'item' => $this->item,
                'errors' => $this->errors
            ], $this->statusCodes[RestResponse::NOK]);
        }

        $result = [
            'result' => $this->state
        ];


        if ($this->state == RestResponse::OK && $this->item) {
            if ($this->item instanceof DeferredRepoResult) {
                $this->item->resolve();
                $result['item'] = $this->item->getResult();
            } else {
                $result['item'] = $this->item;
            }
        } elseif ($this->state == RestResponse::OK && $this->items != null) {
            if ($this->items instanceof DeferredRepoResult) {
                $this->items->resolve();
                $items = [
                    'items' => $this->items->getResult(),
                    'pagination' => [
                        'order' => ($this->items->pagination->direction == 'desc' ? "-" : "") . $this->items->pagination->order,
                        'total' => $this->items->total,
                        'limit' => $this->items->pagination ? $this->items->pagination->perPage : 30,
                        'page' => $this->items->pagination ? $this->items->pagination->page : 1,
                        'variants' => [30,50,100]
                    ]
                ];
            } else {
                $items = $this->items;
            }
            $result = array_merge($result, $items);
        }
        $result['meta'] = [];
        foreach ($this->meta['collections'] as $name => $collection) {
            if ($collection instanceof DeferredRepoResult) {
                $collection->resolve();

                $items = $collection->getResult();
            } else {
                $items = $collection;
            }
            $result['meta']['collections'][$name] = $items;
        }
        $result['meta'] = array_merge($result['meta'], [
            'user' => $this->meta['user'],
            'env' => config('app.env')
        ]);
        if ($this->state == RestResponse::NOK) {
            $result['errors'] = $this->errors;
        }

        return Response::json($result, $this->statusCodes[$this->state]);
    }
}