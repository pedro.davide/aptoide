<?php

namespace App\Libs\Filters;

use Illuminate\Support\Facades\DB;

class PriceFilter
{
    private $name;
    private $value;
    public function __construct($name,$value){
        $this->name = $name;
        $this->value = $value;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if (!empty($this->value)) {
            $price = explode(',', $this->value);

            $query->where($this->name, '<=', $price[1])
                ->where($this->name, '>=', $price[0]);
        }
    }
}