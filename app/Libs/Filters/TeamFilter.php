<?php

namespace App\Libs\Filters;

use App\Models\UserTeam;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TeamFilter
{
    private $value;

    public function __construct($value) {
        $this->value = $value;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if ($this->value != '0') {
            if (Auth::check()) {
                $user_teams = UserTeam::query()
                    ->where('site_user_id', Auth::user()->id)
                    ->get()
                    ->map(function($item) {
                        return $item->team_id;
                    })
                    ->toArray();

                if (!empty($user_teams)) {
                    $query->where(function($q) use ($user_teams) {
                        $q->whereIn('home_team_id', $user_teams)
                          ->orWhereIn('away_team_id', $user_teams);
                    });
                }
            }
        }
    }
}