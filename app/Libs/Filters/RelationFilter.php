<?php
namespace App\Libs\Filters;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;

class RelationFilter
{
    private $name;
    private $value;
    private $relation;
    private $field;

    public function __construct($name, $relation, $field, $value) {
        $this->name = $name;
        $this->value = $value;
        $this->relation = $relation;
        $this->field = $field;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if (!empty($this->value)) {
            $model = new $this->relation();

            $ids = $model::query()
                ->where($this->field, $this->value )
                ->get()
                ->map(function($item) { return $item->id; })
                ->toArray();

            $query->whereIn($this->name, $ids);
        }
    }
}