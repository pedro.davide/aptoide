<?php

namespace App\Libs\Filters;

use Illuminate\Support\Facades\DB;

class DateRangeFilter
{
    private $name;
    private $from;
    private $to;
    public function __construct($name,$from,$to){
        $this->name = $name;
        $this->from = $from;
        $this->to = $to;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if ((!empty($this->name)) && (!empty($this->from)) && (!empty($this->to))) {
            $query->where($this->name,'>=', date('Y-m-d', strtotime($this->from)));
            $query->where($this->name,'<=', date('Y-m-d', strtotime($this->to)));
        }
    }
}