<?php
namespace App\Libs\Filters;
class DateFilter
{
    private $name;
    private $value;
    private $operator;
    public function __construct($name,$operator,$value){
        $this->name = $name;
        $this->value = $value;
        $this->operator = $operator;
    }
    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if(!empty($this->value)){
            $query->where($this->name,$this->operator,$this->value);
        }
    }
}