<?php

namespace App\Libs\Filters;

use Illuminate\Support\Facades\DB;

class ExactFilter
{
    private $name;
    private $value;
    public function __construct($name,$value){
        $this->name = $name;
        $this->value = $value;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if (!empty($this->value)) {
            $query->where($this->name,'=',$this->value);
        }
        else {
            if ($this->value == '0') {
                $query->where($this->name,'=',$this->value);
            }
        }
    }
}