<?php

namespace App\Libs\Filters;

use Illuminate\Support\Facades\DB;

class RangeFilter
{
    private $name;
    private $from;
    private $to;
    public function __construct($name,$from,$to){
        $this->name = $name;
        $this->from = $from;
        $this->to = $to;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if(!empty($this->name)){
            $query->where($this->name,'>=', date('Y-m-d 00:00:00', strtotime($this->from)));
            $query->where($this->name,'<=', date('Y-m-d 23:59:59', strtotime($this->to)));
        }
    }
}