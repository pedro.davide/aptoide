<?php
namespace App\Libs\Filters;
use Illuminate\Support\Facades\DB;

class NullFilter
{
    private $name;
    private $value;
    public function __construct($name){
        $this->name = $name;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if(!empty($this->name)){
            $query->whereNull($this->name);
        }
    }
}