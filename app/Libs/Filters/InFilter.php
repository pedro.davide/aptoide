<?php

namespace App\Libs\Filters;

use Illuminate\Support\Facades\DB;

class InFilter
{
    private $name;
    private $value;
    public function __construct($name,$value){
        $this->name = $name;
        $this->value = $value;
    }

    public function applyToQuery(\Illuminate\Database\Query\Builder $query){
        if (!empty($this->value)) {
            if (!is_array($this->value)) {
                $this->value = explode(',', $this->value);
            }

            $query->whereIn($this->name, $this->value);
        }
    }
}