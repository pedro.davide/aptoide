<?php

namespace App\Libs;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class SiEloquentBuilder
 *
 * @package App\Libs\Repo
 */
class BaseEloquentBuilder extends Builder {
    public function addFilters(array $filters = [], $table = null) {
        return parent::addFilters($filters,$table);
    }
}