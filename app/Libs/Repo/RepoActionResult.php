<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 15:22
 */

namespace App\Libs\Repo;


class RepoActionResult implements DeferredRepoResult {
    public $item;
    public $errors;

    public function __construct($item, $errors = null) {
        $this->item = $item;
        $this->errors = $errors;
    }

    /**
     * Actually resolves/executes query
     *
     * @return void
     */
    public function resolve() {
        // NOOP
    }

    public function getResult() {
        return $this->item;
    }
}