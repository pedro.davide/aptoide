<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 20/08/15
 * Time: 19:01
 */

namespace App\Libs\Repo;


use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Validator;

trait Model{


    public $messages = [
        'required' => 'required'
    ];

    public function getHydratedFields() {
        return ['id', 'name'];
    }

    public function getHydrated() {
        return array_merge($this->getHydratedFields(), $this->foreignKeys ?: []);
    }

    public function newEloquentBuilder($query) {
        return new EloquentBuilder($query);
    }

    public function setDirty(){
        $this->original = [];
    }

    /**
     * @return EloquentBuilder
     */
    public static function query() {
        return (new static)->newQuery();
    }

    protected function newBaseQueryBuilder() {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new QueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    public function getRules() {
        return $this->rules;
    }

    public function validate($errors, $scenario = 'generic') {

        $validator = Validator::make($this->toArray(), $this->getRules()[$scenario], $this->messages);

        if ($validator->fails()) {
            array_merge($errors, $validator->messages()
                ->toArray());
        }
        return $errors;
    }

    public static function getClassName() {
        return (new \ReflectionClass(static::class))->getShortName();
    }

    public static function onCreating($obj) {
    }

    public static function onCreated($obj) {
    }

    public static function onUpdating($obj) {
    }

    public static function onUpdated($obj) {
    }

    public static function onDeleting($obj) {
    }

    public static function onDeleted($obj) {
    }

    public static function onSaving($obj) {
    }

    public static function onSaved($obj) {
    }

    public static function boot() {
        parent::boot();
        static::creating(function ($obj) {
            return static::onCreating($obj);
        });
        static::created(function ($obj) {
            return static::onCreated($obj);
        });
        static::updating(function ($obj) {
            return static::onUpdating($obj);
        });
        static::updated(function ($obj) {
            return static::onUpdated($obj);
        });
        static::saving(function ($obj) {
            return static::onSaving($obj);
        });
        static::saved(function ($obj) {
            return static::onSaved($obj);
        });
        static::deleting(function ($obj) {
            return static::onDeleting($obj);
        });
        static::deleted(function ($obj) {
            return static::onDeleted($obj);
        });
    }

    public function insertOnDuplicateUpdate(array $attributes,array $update)
    {
        $this->fill($attributes);

        if ($this->usesTimestamps()) {
            $this->updateTimestamps();
        }

        $attributes = $this->getAttributes();

        $query = $this->newBaseQueryBuilder();
        $processor = $query->getProcessor();
        $grammar = $query->getGrammar();

        $table = $grammar->wrapTable($this->getTable());
        $keyName = $this->getKeyName();
        $columns = $grammar->columnize(array_keys($attributes));
        $insertValues = $grammar->parameterize($attributes);

        $updateValues = [];

        foreach ($update as $k => $v) {
            $updateValues[] = sprintf("%s = %s", $grammar->wrap($k), $v instanceof Expression ? $v : "'{$v}'");
        }

        $updateValues = join(',', $updateValues);

        $sql = "insert into {$table} ({$columns}) values ({$insertValues}) on duplicate key update {$updateValues}";

        $id = $processor->processInsertGetId($query, $sql, array_values($attributes));

        $this->setAttribute($keyName, $id);

        return $this;
    }
}