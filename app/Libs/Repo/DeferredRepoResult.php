<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 11:14
 */

namespace App\Libs\Repo;


/**
 * Interface DeferredRepoResult
 *
 * @package App\Libs\Repo
 */
interface DeferredRepoResult {
    /**
     * Actually resolves/executes query
     *
     * @return void
     */
    public function resolve();

    public function getResult();

}