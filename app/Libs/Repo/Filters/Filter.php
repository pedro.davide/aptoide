<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 18/09/15
 * Time: 17:49
 */

namespace App\Libs\Repo\Filters;


class Filter {
    public $field;
    public $value;
    public $operator;

    const MATCH = 'LIKE';

    public function __construct($field, $value, $operator = '=') {
        $this->field = $field;
        $this->value = $value;
        $this->operator = $operator;
    }
}