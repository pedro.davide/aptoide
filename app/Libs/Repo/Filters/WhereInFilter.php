<?php

namespace App\Libs\Repo\Filters;

class WhereInFilter extends Filter
{
    public $field;
    public $value;

    public function __construct($field,$value) {
        $this->field = $field;
        $this->value = $value;
    }

}