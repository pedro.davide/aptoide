<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 18/09/15
 * Time: 17:49
 */

namespace App\Libs\Repo\Filters;


class DateRangeFilter extends Filter {
    public $field;
    public $valueFrom;
    public $valueTo;

    public function __construct($field, $valueFrom, $valueTo) {
        $this->field = $field;
        $this->valueFrom = $valueFrom;
        $this->valueTo = $valueTo;
    }

    public function hasValue(){
        return !empty($this->valueFrom) && !empty($this->valueTo);
    }
}