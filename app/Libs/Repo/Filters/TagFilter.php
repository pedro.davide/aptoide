<?php

namespace App\Libs\Repo\Filters;

class TagFilter extends Filter
{
    public $field;
    public $value;

    public function __construct($field, $value) {
        $this->field = $field;
        $this->value = $value;
    }

}