<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 18/09/15
 * Time: 17:49
 */

namespace App\Libs\Repo\Filters;


class InSetFilter extends Filter {
    public $field;
    public $value;

    public function __construct($field, $value) {
        $this->field = $field;
        $this->value = array_filter(explode(',', $value));
    }
}