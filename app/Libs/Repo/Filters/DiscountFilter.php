<?php

namespace App\Libs\Repo\Filters;

class DiscountFilter extends Filter
{
    public $new;
    public $old;
    public $value;

    public function __construct($new, $old, $value) {
        $this->new = $new;
        $this->old = $old;
        $this->value = $value;
    }

}