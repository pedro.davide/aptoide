<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 18/09/15
 * Time: 17:49
 */

namespace App\Libs\Repo\Filters;


class HasFilter extends Filter {
    public $field;
    public $value;
    public $column;

    public function __construct($field, $value, $column = 'nId') {
        $this->field = $field;
        $this->value = array_filter(explode(',', $value));
        $this->column = $column;
    }
}