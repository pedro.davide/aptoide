<?php

namespace App\Libs\Repo\Filters;

class RangeFilter extends Filter
{
    public $field;
    public $min_value;
    public $max_value;

    public function __construct($field, $min_value, $max_value) {
        $this->field = $field;
        $this->min_value = $min_value;
        $this->max_value = $max_value;
    }

}