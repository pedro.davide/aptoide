<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 11:27
 */

namespace App\Libs\Repo;


use App\Libs\Http\Pagination;

/**
 * Class EloquentRepoResultDeferrer
 *
 * @package App\Libs\Repo
 */
trait EloquentRepoResultDeferrer {
    /**
     * @param $query
     *
     * @return EloquentRepoItem
     */
    public function promiseItem($query, $allowEmpty = false) {
        return new EloquentRepoItem($query, $allowEmpty);
    }

    /**
     * @param      $query
     * @param null $pagination Pagination
     *
     * @return EloquentRepoItems
     */
    public function promiseItems($query, Pagination $pagination = null) {
        return new EloquentRepoItems($query, $pagination);
    }

    /**
     * @param $item
     * @param $errors
     *
     * @return RepoActionResult
     */
    public function promiseResult($item, $errors = null) {
        return new RepoActionResult($item, $errors);
    }
}