<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 20/08/15
 * Time: 19:02
 */

namespace App\Libs\Repo;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Class SiEloquentBuilder
 *
 * @package App\Libs\Repo
 */
class EloquentBuilder extends Builder {
    /**
     * @param $resource
     * @param $column
     *
     * @return EloquentBuilder
     */
    public function selectIfAllowed($resource, $column) {
        return parent::selectIfAllowed($resource, $column);
    }

    public function selectAll($arr) {
        return parent::selectAll($arr);
    }

    /**
     * @param        $filterValue
     * @param        $column
     * @param null   $operator
     * @param string $boolean
     *
     * @return EloquentBuilder
     */
    public function filterBy($filterValue, $column, $operator = '=', $boolean = 'and') {
        return parent::filterBy($filterValue, $column, $operator, $boolean);
    }

    public function addFilters(array $filters = [], $table = null) {
        return parent::addFilters($filters,$table);
    }

    public function hydrated($parent, $relation, $childs, $additional = ['nId', 'txName']) {
        return parent::with([$relation => function (Relation $query) use (&$parent, $relation, $childs, $additional) {
            $query->addSelect($query->getRelated()
                ->getHydrated());
            if ($query instanceof HasMany) {
                $query->addSelect($query->getForeignKey());
            }
            if ($childs) {
                $query->hydrated($query, reset($childs), array_slice($childs, 1), $additional);
            }
        }]);
    }

    public function withHydrated($relation, $additional = ['nId', 'txName']) {
        if (strpos($relation, '.') !== false) {
            $rels = explode('.', $relation);
            $this->hydrated(null, reset($rels), array_slice($rels, 1), $additional);
        } else {
            $this->hydrated(null, $relation, [], $additional);
        };
        return $this;
    }

    public function onlyAllowed($column, $klass, $user) {
        return parent::onlyAllowed($column, $klass, $user);
    }

    public function onlyCustomer($column, $user) {
        return parent::onlyCustomer($column, $user);
    }

    public function onlySupplier($column, $user,$campaigns = false) {
        return parent::onlySupplier($column, $user,$campaigns);
    }
}