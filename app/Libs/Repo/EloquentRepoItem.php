<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 11:17
 */

namespace App\Libs\Repo;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class EloquentRepoItem
 *
 * @package App\Libs\Repo
 */
class EloquentRepoItem implements DeferredRepoResult {
    /**
     * @var Object
     */
    public $item;
    /**
     * @var Builder
     */
    public $query;

    public $defaultValue;

    public $allowEmpty;

    /**
     * @param $query
     */
    public function __construct($query, $allowEmpty = false) {
        $this->query = $query;
        $this->allowEmpty = $allowEmpty;
    }

    /**
     *
     */
    public function resolve() {
        $this->item = $this->query->first();
        if(!$this->item && $this->defaultValue){
            $this->item = $this->defaultValue;
        }
        if (!$this->item && !$this->allowEmpty) {
            throw new NotFoundHttpException();
        }
    }

    public function orDefault($value){
        $this->defaultValue = $value;
        return $this;
    }

    public function getResult() {
        if (!$this->item) {
            $this->resolve();
        }
        return $this->item;
    }

}