<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 17/10/2016
 * Time: 15:53
 */

namespace App\Libs\Repo;


interface ValidatableModel
{
    const GENERIC_RULES = 'generic';
    const REQUIRED = 'required';
}