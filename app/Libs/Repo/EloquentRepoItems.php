<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 11:19
 */

namespace App\Libs\Repo;


use App\Libs\Http\Pagination;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EloquentRepoItems
 *
 * @package App\Libs\Repo
 */
class EloquentRepoItems implements DeferredRepoResult {
    /**
     * @var array
     */
    public $items;
    /**
     * @var int
     */
    public $total;
    /**
     * @var \Illuminate\Database\Query\Builder
     */
    public $query;
    /**
     * @var Pagination
     */
    public $pagination;

    private $original;

    /**
     * @param Builder    $query
     * @param Pagination $pagination
     */
    public function __construct($query, Pagination $pagination = null) {
        $this->original = $query;
        $this->query = $query instanceof Builder ? $query->getQuery() : $query;
        $this->pagination = $pagination;
    }

    /**
     *
     */
    public function resolve() {
        $this->total = $this->query->getCountForPagination();

        if ($this->pagination) {
            $this->original
                ->skip(($this->pagination->page - 1) * $this->pagination->perPage)
                ->take($this->pagination->perPage)
                ->orderBy($this->pagination->order,$this->pagination->direction);
        }

        $this->items = $this->original->get();
    }

    public function getResult() {
        if (!$this->items) {
            $this->resolve();
        }
        return $this->items;
    }

    public function hydrateIdAndName() {
        $table = $this->query->from;
        $this->original->setEagerLoads([]);
        $this->query->select($table . ".id", $table . ".name");
        return $this;
    }

    public function hydrateName() {
        $table = $this->query->from;
        $this->original->setEagerLoads([]);
        $this->query->select($table . ".name as nId", $table . ".name");
        return $this;
    }
}