<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 20/08/15
 * Time: 19:02
 */

namespace App\Libs\Repo;


use App\Libs\Repo\Filters\DateRangeFilter;
use App\Libs\Repo\Filters\DiscountFilter;
use App\Libs\Repo\Filters\InSetFilter;
use App\Libs\Repo\Filters\RangeFilter;
use App\Libs\Repo\Filters\TagFilter;
use App\Libs\Repo\Filters\WhereInFilter;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\Processors\Processor;
use Illuminate\Support\Facades\DB;

/**
 * Class SiQueryBuilder
 *
 * @package App\Libs\Repo
 */
class QueryBuilder extends Builder {


    /**
     * @param ConnectionInterface $connection
     * @param Grammar             $grammar
     * @param Processor           $processor
     */
    public function __construct(ConnectionInterface $connection,
                                Grammar $grammar,
                                Processor $processor) {
        parent::__construct($connection, $grammar, $processor);
    }


    public function selectAll($arr) {
        foreach ($arr as $col) {
            $this->addSelect($col);
        }
        return $this;
    }

    /**
     * @param        $filterValue
     * @param        $column
     * @param null   $operator
     * @param string $boolean
     *
     * @return $this
     */
    public function filterBy($filterValue, $column, $operator = '=', $boolean = 'and') {
        if ($filterValue || $filterValue == '0') {
            parent::where($column, $operator, $operator == 'LIKE' ? DB::raw("'%$filterValue%'") : $filterValue, $boolean);
        }
        return $this;
    }

    /**
     * @param        $filterValue
     * @param        $column
     * @param null   $operator
     * @param string $boolean
     *
     * @return $this
     */
    public function addFilters(array $filters = [], $table = null) {
        if($table){
            $filters = array_map(function($filter)use($table){
                $newFilter = clone $filter;
                $newFilter->field = str_replace('PARENT_TABLE',$table, $newFilter->field);
                return $newFilter;
            },$filters);
        }
        foreach ($filters as $filter) {
            if ($filter instanceof DateRangeFilter) {
                if ($filter->hasValue()) {
                    parent::whereBetween($filter->field, [$filter->valueFrom . ' 00:00:00', $filter->valueTo . ' 23:59:59']);
                }
            } elseif ($filter instanceof InSetFilter) {
                if ((!empty($filter->value)) && (sizeof($filter->value) > 0)) {
                    if ($filter->value) {
                        parent::whereIn($filter->field, $filter->value);
                    }
                }
            } elseif ($filter instanceof WhereInFilter) {
                if ((!empty($filter->value)) && (sizeof($filter->value) > 0)) {
                    parent::whereIn($filter->field, $filter->value);
                }
            }
            elseif ($filter instanceof RangeFilter) {
                if (!empty($filter->min_value) && (!empty($filter->max_value))) {
                    parent::where($filter->field,'>=', $filter->min_value)->where($filter->field, '<=', $filter->max_value);
                }
            }
            elseif ($filter instanceof DiscountFilter) {
                if ((!empty($filter->value)) && (sizeof($filter->value) > 0)) {
                    parent::where(DB::raw("((({$filter->old} - {$filter->new}) * 100) / {$filter->old})"), '>=', $filter->value);
                }
            }
            else if($filter instanceof TagFilter) {
                if ((!empty($filter->value)) && (sizeof($filter->value) > 0)) {
                    parent::whereIn($filter->field, $filter->value);
                }
            }

            else {
                self::filterBy($filter->value, $filter->field, $filter->operator);
            }

        }
        return $this;
    }

}