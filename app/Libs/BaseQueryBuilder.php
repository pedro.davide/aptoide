<?php

namespace App\Libs;

use App\Models\BriefingWatcher;
use App\Models\ProjectUser;
use App\Models\TaskWatcher;
use App\Models\TeamUser;
use App\Models\TicketWatcher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BaseQueryBuilder extends \Illuminate\Database\Query\Builder
{
    public function addFilters(array $filters = []) {
        foreach ($filters as $filter) {
            $filter->applyToQuery($this);
        }
        return $this;
    }
}