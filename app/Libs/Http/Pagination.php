<?php
/**
 * Created by PhpStorm.
 * User: pavel.egorov
 * Date: 19/08/15
 * Time: 10:38
 */

namespace App\Libs\Http;


/**
 * Class Pagination
 *
 * @package App\Libs\Http
 */
class Pagination {
    /**
     * @var int
     */
    public $page;
    /**
     * @var int
     */
    public $perPage;

    /**
     * @var string
     */
    public $order;

    /**
     * @var string
     */
    public $direction;
    /**
     * @param int $page
     * @param int $perPage
     * @param string $order
     * @param string $direction
     */
    public function __construct($page = 1, $perPage = 30, $order = '-id', $direction = 'desc') {
        $this->page = intval($page);
        $this->perPage = intval($perPage);
        $this->order = str_replace('-', '', $order);
        $this->direction = $direction;
    }
}