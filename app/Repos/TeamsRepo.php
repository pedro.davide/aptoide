<?php

namespace App\Repos;

use App\Libs\Http\Pagination;
use App\Libs\Repo\EloquentRepoResultDeferrer;
use App\Models\Country;
use App\Models\Season;
use App\Models\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TeamsRepo
{
    use EloquentRepoResultDeferrer;

    public function getAll($filters = [], Pagination $pagination = null) {
        $query = Team::query()
            ->orderBy('id', 'desc')
            ->addFilters($filters);

        return $this->promiseItems($query, $pagination);
    }

    public function countAll($filters = []) {
        return Team::query()
            ->addFilters($filters)
            ->count();
    }

    public function get($id){
        $query = Team::query()
            ->where('id', $id);

        return $this->promiseItem($query);
    }

    public function save($attributes){
        $team = new Team();
        $team->fill($attributes);

        if ($team->isValid()){
            $team->save();
        }

        return $this->promiseResult($team, $team->getErrors()->count() > 0 ? $team->getErrors() : []);
    }

    public function update($id, $attributes){
        $team = Team::findOrFail($id);
        $team->fill($attributes);

        if ($team->isValid()){
            $team->save();
        }

        return $this->promiseResult($team, $team->getErrors()->count() > 0 ? $team->getErrors() : []);
    }

    public function delete($id) {
        $team = Team::findOrFail($id);
        $team->delete();

        return $this->promiseResult($team, []);
    }

    public function bulkDelete($items) {
        $result = DB::transaction(function()use($items) {
            foreach($items as $item) {
                $item = Team::findOrFail($item['id']);
                $item->delete();
            }

            return true;
        });

        return $this->promiseResult(null,$result ? [] : ['transaction' => $result]);
    }
}