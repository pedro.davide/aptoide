<?php

namespace App\Repos;

use App\Libs\Http\Pagination;
use App\Libs\Repo\EloquentRepoResultDeferrer;
use App\Models\Country;
use App\Models\Power;
use App\Models\Season;
use App\Models\Superhero;
use App\Models\SuperheroPower;
use App\Models\SuperheroTeam;
use App\Models\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SuperherosRepo
{
    use EloquentRepoResultDeferrer;

    public function getAll($filters = [], Pagination $pagination = null) {
        $query = Superhero::query()
            ->with(['Powers', 'Teams'])
            ->orderBy('id', 'desc')
            ->addFilters($filters);

        return $this->promiseItems($query, $pagination);
    }

    public function get($id){
        $query = Superhero::query()
            ->with(['Powers', 'Teams'])
            ->where('id', $id);

        return $this->promiseItem($query);
    }

    public function save($attributes){
        $superhero = new Superhero();
        $superhero->fill($attributes);

        if ($superhero->isValid()){
            $superhero->save();

            if (!empty($attributes['teams'])) {
                foreach(collect($attributes['teams']) as $item) {
                    $superhero_team = SuperheroTeam::firstOrNew(['superhero_id' => $superhero->id, 'team_id' => $item['id']]);

                    if ($superhero_team->isValid()) {
                        $superhero_team->save();
                    }
                }
            }

            if (!empty($attributes['powers'])) {
                foreach(collect($attributes['powers']) as $item) {
                    $superhero_power = SuperheroPower::firstOrNew(['superhero_id' => $superhero->id, 'power_id' => $item['id']]);

                    if ($superhero_power->isValid()) {
                        $superhero_power->save();
                    }
                }
            }
        }

        return $this->promiseResult($superhero, $superhero->getErrors()->count() > 0 ? $superhero->getErrors() : []);
    }

    public function update($id, $attributes){
        $superhero = Superhero::findOrFail($id);
        $superhero->fill($attributes);

        if ($superhero->isValid()){
            $superhero->save();

            if (!empty($attributes['teams'])) {
                foreach(collect($attributes['teams']) as $item) {
                    $superhero_team = SuperheroTeam::firstOrNew(['superhero_id' => $superhero->id, 'team_id' => $item['id']]);

                    if ($superhero_team->isValid()) {
                        $superhero_team->save();
                    }
                }
            }

            if (!empty($attributes['powers'])) {
                foreach(collect($attributes['powers']) as $item) {
                    $superhero_power = SuperheroPower::firstOrNew(['superhero_id' => $superhero->id, 'power_id' => $item['id']]);

                    if ($superhero_power->isValid()) {
                        $superhero_power->save();
                    }
                }
            }
        }

        return $this->promiseResult($superhero, $superhero->getErrors()->count() > 0 ? $superhero->getErrors() : []);
    }

    public function delete($id){
        $superhero = Superhero::findOrFail($id);
        $superhero->delete();

        return $this->promiseResult($superhero, []);
    }

    public function bulkDelete($items){
        $result = DB::transaction(function()use($items) {
            foreach($items as $item) {
                $item = Superhero::findOrFail($item['id']);
                $item->delete();
            }

            return true;
        });

        return $this->promiseResult(null,$result ? [] : ['transaction' => $result]);
    }
}