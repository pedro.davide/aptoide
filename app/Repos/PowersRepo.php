<?php

namespace App\Repos;

use App\Libs\Http\Pagination;
use App\Libs\Repo\EloquentRepoResultDeferrer;
use App\Models\Country;
use App\Models\Power;
use App\Models\Season;
use App\Models\Team;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PowersRepo
{
    use EloquentRepoResultDeferrer;

    public function getAll($filters = [], Pagination $pagination = null) {
        $query = Power::query()
            ->orderBy('id', 'desc')
            ->addFilters($filters);

        return $this->promiseItems($query, $pagination);
    }

    public function countAll($filters = []) {
        return Power::query()
            ->addFilters($filters)
            ->count();
    }

    public function get($id) {
        $query = Power::query()
            ->where('id', $id);

        return $this->promiseItem($query);
    }

    public function save($attributes){
        $power = new Power();
        $power->fill($attributes);

        if ($power->isValid()){
            $power->save();
        }

        return $this->promiseResult($power, $power->getErrors()->count() > 0 ? $power->getErrors() : []);
    }

    public function update($id, $attributes){
        $power = Power::findOrFail($id);
        $power->fill($attributes);

        if ($power->isValid()){
            $power->save();
        }

        return $this->promiseResult($power, $power->getErrors()->count() > 0 ? $power->getErrors() : []);
    }

    public function delete($id){
        $power = Power::findOrFail($id);
        $power->delete();

        return $this->promiseResult($power, []);
    }

    public function bulkDelete($items){
        $result = DB::transaction(function()use($items) {
            foreach($items as $item) {
                $item = Power::findOrFail($item['id']);
                $item->delete();
            }

            return true;
        });

        return $this->promiseResult(null,$result ? [] : ['transaction' => $result]);
    }
}