<?php
/**
 * Created by PhpStorm.
 * User: pedrodavide
 * Date: 27/07/18
 * Time: 21:18
 */

namespace App\Repos;


class Repo
{
    public function name() {
        return lcfirst(str_replace('App\\Repos\\', '', get_called_class()));
    }

    public function returnDates($fromdate, $todate) {
        $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
        $todate = \DateTime::createFromFormat('Y-m-d', $todate);
        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }
}