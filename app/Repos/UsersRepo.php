<?php

namespace App\Repos;

use App\Libs\Http\Pagination;
use App\Libs\Repo\EloquentRepoResultDeferrer;
use App\Models\InfluencerCategory;
use App\Models\Message;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersRepo
{
    use EloquentRepoResultDeferrer;

    public function getAll($filters = [], Pagination $pagination = null) {

        $query = User::query()
            ->addFilters($filters);

        return $this->promiseItems($query, $pagination);
    }


    public function get($id){
        $user = User::find($id);

        return $user;
    }

    public function save($attributes){
        $user = new User($attributes);

        if ($user->isValid()){
            if(!empty($attributes['password'])) {
                if ($attributes['password'] === $attributes['password_confirmation']) {
                    $user->setPassword($attributes['password']);
                }
            }
            $user->save();
        }

        return $this->promiseResult($user, $user->getErrors()->count() > 0 ? $user->getErrors() : []);
    }

    public function update($id, $attributes) {
        $user = User::findOrFail($id);
        $user->fill($attributes);

        if ($user->isValid()){
            if(!empty($attributes['password'])) {
                if ($attributes['password'] === $attributes['password_confirmation']) {
                    $user->setPassword($attributes['password']);
                }
            }
            $user->save();
        }

        return $this->promiseResult($user, $user->getErrors()->count() > 0 ? $user->getErrors() : []);
    }

    public function delete($id){
        $user = User::findOrFail($id);
        $user->delete();

        return $this->promiseResult($user, []);
    }

    public function bulkDelete($items) {
        $result = DB::transaction(function()use($items) {
            foreach($items as $item) {
                $item = Team::findOrFail($item['id']);
                $item->delete();
            }

            return true;
        });

        return $this->promiseResult(null,$result ? [] : ['transaction' => $result]);
    }
}