@extends('dashboard.layouts.app')

@section('content')

    <div ng-controller="coreSettingsCtrl" ng-class="{'container': app.setting.boxed, 'ie': isIE,'smart': isSmart}">
        <div class="app" ui-view></div>

        <script src="/bundles/vendor.js"></script>
        <script src="/bundles/app.js"></script>

        <script>(function () {
                angular.element(document).ready(function() {
                    angular.module('app').value('USER', {
                        name: '{{ Auth::user()->name }}',
                        id: '{{ Auth::user()->id }}',
                        email: '{{ Auth::user()->email }}',
                        csrf_field: '{{ csrf_token() }}',
                        logout_route: '{{ route('logout') }}'
                    });
                    angular.bootstrap(document, ['app']);
                });
            })(window);
        </script>
    </div>
@endsection