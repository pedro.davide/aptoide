<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Aptoide</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('/css/glyphicons/glyphicons.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/ionic/ionicons.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/material-design-icons/material-design-icons.css') }}" type="text/css" />
        <!-- ionic here -->
        <link rel="stylesheet" href="{{ asset('/css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
        <!-- build:css ../assets/styles/app.min.css -->
        <link rel="stylesheet" href="{{ asset('/css/styles/app.css') }}" type="text/css" />
        <!-- endbuild -->
        <link rel="stylesheet" href="{{ asset('/css/styles/font.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/styles/custom.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/ng-gaDateRangePicker/ga.theme.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/angular-loading-bar/loading-bar.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/ui-select/dist/select.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css/ng-tags-input/ng-tags-input.css') }}">
        {{--
        <link rel="stylesheet" href="{{ asset('/css/styles/bootstrap-additions.min.css') }}">
        --}}
    </head>
    <body>
        @yield('content')
    </body>
</html>