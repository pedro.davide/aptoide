<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('powers', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('teams', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('superheros', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('nickname');

            $table->dateTime('first_appearing_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('superheros_powers', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('power_id')->unsigned();
            $table->foreign('power_id')->references('id')->on('powers');

            $table->integer('superhero_id')->unsigned();
            $table->foreign('superhero_id')->references('id')->on('superheros');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('superheros_teams', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');

            $table->integer('superhero_id')->unsigned();
            $table->foreign('superhero_id')->references('id')->on('superheros');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('superheros_teams', function($table) {
            $table->dropForeign('superheros_teams_team_id_foreign');
            $table->dropForeign('superheros_teams_superhero_id_foreign');
        });

        Schema::drop('superheros_teams');

        Schema::table('superheros_powers', function($table) {
            $table->dropForeign('superheros_powers_power_id_foreign');
            $table->dropForeign('superheros_powers_superhero_id_foreign');
        });

        Schema::drop('superheros_powers');

        Schema::drop('superheros');

        Schema::drop('teams');

        Schema::drop('powers');
    }
}
