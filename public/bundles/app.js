webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var app = angular.module('app', [
	    'angular-loading-bar',
	    'ncy-angular-breadcrumb',
	    'ui.bootstrap.showErrors',
	    'ui.bootstrap',
	    'ngSanitize',
	    'gaDatePickerRange',
	    'ui.select',
	    'bw.paging',
	    'yaru22.angular-timeago',
	    'ngTagsInput',
	    'app.core',
	    'app.dashboard',
	    'app.users',
	    'app.teams',
	    'app.powers',
	    'app.superheros'
	]);

	__webpack_require__(1);
	__webpack_require__(25);
	__webpack_require__(29);
	__webpack_require__(33);
	__webpack_require__(37);
	__webpack_require__(41);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';


	var appCore = angular.module('app.core', [
	    'ngAnimate',
	    'ui.router',
	    'ngResource'
	]);
	 
	__webpack_require__(2)(appCore);
	__webpack_require__(3)(appCore);
	__webpack_require__(4)(appCore);
	__webpack_require__(5)(appCore);
	__webpack_require__(6)(appCore);
	__webpack_require__(7)(appCore);
	__webpack_require__(8)(appCore);
	__webpack_require__(9)(appCore);
	//require('moduleDir/core/services/ui-load')(appCore);

	__webpack_require__(10)(appCore);

	__webpack_require__(11)(appCore);
	__webpack_require__(12)(appCore);
	__webpack_require__(13)(appCore);
	__webpack_require__(14)(appCore);
	//require('moduleDir/core/directives/ui-jp')(appCore);

	/** filters */
	__webpack_require__(15)(appCore);
	__webpack_require__(16)(appCore);
	__webpack_require__(17)(appCore);
	__webpack_require__(18)(appCore);
	__webpack_require__(19)(appCore);
	__webpack_require__(20)(appCore);
	__webpack_require__(21)(appCore);
	__webpack_require__(22)(appCore);
	__webpack_require__(23)(appCore);

	__webpack_require__(24)(appCore);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	/**
	 * Core run
	 * @module: app.core
	 */
	module.exports = function(module) {
	    var _ = window._;
	    var DOMParser = window.DOMParser;
	    var ActiveXObject = window.ActiveXObject;

	    module.run(['$rootScope', '$state', '$stateParams','notify','qs','checkboxes', function($rootScope, $state, $stateParams,notify, qs, checkboxes) {
	        $rootScope.$state = $state;
	        $rootScope.$stateParams = $stateParams;
	        
	        $rootScope.errorHandler = function (message, form) {
	            return function (result) {
	                notify.error(message);

	                if (result.data.errors) {
	                    angular.forEach(result.data.errors, function (errors, key) {
	                        angular.forEach(errors, function (e) {
	                            form[key].$dirty = true;
	                            form[key].$setValidity(e, false);
	                        }, this);
	                    }, this);
	                }
	            };
	        };

	        $rootScope.qs = qs;

	        $rootScope.selected = [];
	        $rootScope.checkboxes = checkboxes($rootScope.selected);

	        $rootScope.fixNameAndId = function(arr, type) {
	            if (!type) {
	                type = 'name';
	            }

	            return _.map(arr, function(item) {
	                return { id: item.id, txName: item[type] };
	            });
	        };

	        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
	            notify.close();
	        });
	    }])
	    ;
	};


/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Core routes
	 * @module: app.core
	 */
	var moment = window.moment;
	module.exports = function(module) {
	    module.config([
	        '$locationProvider',
	        '$urlRouterProvider',
	        '$stateProvider',
	        '$controllerProvider',
	        '$compileProvider',
	        '$filterProvider',
	        '$httpProvider',
	        '$provide',
	        function($locationProvider, $urlRouterProvider, $stateProvider, $controllerProvider, $compileProvider, $filterProvider,$httpProvider, $provide) {
	            /** store a reference to various provider functions */
	            module.controller = $controllerProvider.register;
	            module.directive = $compileProvider.directive;
	            module.filter = $filterProvider.register;
	            module.factory = $provide.factory;
	            module.provider = $provide.provider;
	            module.service = $provide.service;
	            module.constant = $provide.constant;
	            module.value = $provide.value;

	            /** default route */
	            $urlRouterProvider.otherwise('/superheros/list');

	            /** parent route */
	            $stateProvider.state('default', {
	                abstract: true,
	                url: '',
	                templateUrl: 'modules/core/views/layouts/default.html'
	            });
	        }
	    ])
	    .config(function($breadcrumbProvider) {
	        $breadcrumbProvider.setOptions({
	            templateUrl: '/modules/core/views/partials/breadcrumb.html'
	        });
	    })
	    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	        cfpLoadingBarProvider.includeSpinner = false;
	    }])

	    ;
	};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Core setting controller
	 * @module: app.core
	 */
	module.exports = function(module) {
	    module.controller('coreSettingsCtrl', ['$scope', '$rootScope','$window','USER', function($scope, $rootScope, $window, USER) {
	        $scope.user = USER;

	        $scope.isIE = function() {
	            return !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./);
	        };

	        $scope.isSmart = function() {
	            // Adapted from http://www.detectmobilebrowsers.com
	            var ua = $window.navigator.userAgent || $window.navigator.vendor || $window.opera;
	            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
	            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
	        };

	        $scope.app = {
	            name: 'Flatkit',
	            version: '1.1.3',
	            // for chart colors
	            color: {
	                'primary':      '#0cc2aa',
	                'accent':       '#a88add',
	                'warn':         '#fcc100',
	                'info':         '#6887ff',
	                'success':      '#6cc788',
	                'warning':      '#f77a99',
	                'danger':       '#f44455',
	                'white':        '#ffffff',
	                'light':        '#f1f2f3',
	                'dark':         '#2e3e4e',
	                'black':        '#2a2b3c'
	            },
	            setting: {
	                theme: {
	                    primary: 'primary',
	                    accent: 'accent',
	                    warn: 'warn'
	                },
	                folded: false,
	                boxed: false,
	                container: false,
	                themeID: 1,
	                bg: ''
	            },
	            feedback : {
	                html2canvasURL : 'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js',
	                ajaxURL : 'feedback',
	                postHTML : false
	            }
	        };

	        $scope.sections = [
	            {
	                divider: true,
	                name: 'Main'
	            },/*
	            {
	                divider: false,
	                name: 'Dashboard',
	                icon: 'dashboard',
	                state: 'default.home',
	                route: 'default.home'
	            },*/
	            {
	                divider: false,
	                name: 'Superheros',
	                icon: 'android',
	                state: 'default.superheros.list',
	                route: 'default.superheros'
	            },
	            {
	                divider: false,
	                name: 'Teams',
	                icon: 'assignment_ind',
	                state: 'default.teams.list',
	                route: 'default.teams'
	            },
	            {
	                divider: false,
	                name: 'Powers',
	                icon: 'flash_on',
	                state: 'default.powers.list',
	                route: 'default.powers'
	            },
	            {
	                divider: true,
	                name: 'Manage'
	            },
	            {
	                divider: false,
	                name: 'Users',
	                icon: 'people',
	                state: 'default.users.list',
	                route: 'default.users'
	            }
	        ];
	    }]);
	};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Core setting controller
	 * @module: app.core
	 */
	module.exports = function (module) {
	    module.controller('PaginationController', ['$scope','qs', function ($scope,qs) {
	        $scope.pageChanged = function(page){
	            qs.setValue('page', page);
	        };

	        $scope.setPerpage = function(ppage){
	            qs.setValues({
	                limit : ppage,
	                page: 1
	            });
	        };
	    }]);
	};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.factory('notify', ['$rootScope', function ($rootScope) {

	        $rootScope.notification = null;

	        var error = function (message) {
	            $rootScope.notification = { icon: 'fa fa-ban', message: message, type: 'danger' };
	        };

	        var warning = function (message) {
	            $rootScope.notification = { icon: 'fa fa-bell', message: message, type: 'warning' };
	        };

	        var success = function (message) {
	            $rootScope.notification = { icon: 'fa fa-check', message: message, type: 'mint' };
	        };

	        var close = function () {
	            $rootScope.notification = null;
	        };

	        var has = function () {
	            return $rootScope.notification !== null;
	        };

	        var notification = function () {
	            return $rootScope.notification;
	        };

	        return {
	            error: error,
	            warning: warning,
	            success: success,
	            close: close,
	            has: has,
	            notification : notification
	        };
	    }]);
	};

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.factory('ld',['$q',function($q) {
	        return function(){
	            var args = Array.prototype.slice.call(arguments);
	            var $scope = args.pop();
	            $scope.loading = true;
	            $q.all(args).finally(function () {
	                $scope.loading = false;
	                $scope.loaded = true;
	            });
	        };
	    }]);
	};

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.factory('qs',['$location','$state','$rootScope', function($location,$state,$rootScope) {
	        $rootScope.queryString = $rootScope.queryString ? $rootScope.queryString : {};

	        var pack = function(params){
	            var querystring = [];
	            for(var name in params){
	                querystring.push(name+'='+encodeURIComponent(params[name]));
	            }
	            return querystring.join('&');
	        };

	        var unpack = function(string){
	            if(typeof string !== 'string' ){
	                return {};
	            }
	            var querystringObj = {};
	            var params = string.split('&');
	            for(var idx in params){
	                var kv = params[idx].split('=');
	                querystringObj[kv[0]] = decodeURIComponent(kv[1]);
	            }
	            return querystringObj;
	        };

	        return {
	            pack : pack,
	            unpack : unpack,
	            setValue : function(name,value, notify){
	                if (!notify) {
	                    notify = true;
	                }
	                $rootScope.queryString[name] = value;
	                $state.go($state.current.name,{options : pack($rootScope.queryString)}, { notify: notify });
	            },
	            removeValue : function(name){
	                delete $rootScope.queryString[name];
	                $state.go($state.current.name,{options : pack($rootScope.queryString)});
	            },
	            setValues : function(values,replace,notify){
	                if (!notify) {
	                    notify = true;
	                }
	                if(replace){
	                    $rootScope.queryString = {};
	                }
	                for(var idx in values){
	                    $rootScope.queryString[idx] = values[idx];
	                }
	                return $state.go($state.current.name,{options : pack($rootScope.queryString)},{notify: notify});
	            },
	            getValue : function(name,defaultValue){
	                $rootScope.queryString = unpack($location.$$search.options);
	                return $rootScope.queryString[name] ? $rootScope.queryString[name] : defaultValue;
	            },
	            getAll : function(){
	                $rootScope.queryString = unpack($location.$$search.options);

	                return $rootScope.queryString ? $rootScope.queryString : {};
	            }
	        };
	    }]);
	};

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    module.factory('checkboxes',['$q',function($q) {
	        return function(selected){

	            var toggle = function (value) {
	                var idx = selected.indexOf(value);
	                if (idx > -1) {
	                    selected.splice(idx, 1);
	                } else {
	                    selected.push(value);
	                }

	            };

	            var isChecked = function (item) {
	                var idx = selected.indexOf(item);
	                if (idx > -1) {
	                    return true;
	                } else {
	                    return false;
	                }
	            };

	            var isCheckedAll = function (items) {
	                if (items && selected.length === items.length) {
	                    return true;
	                } else {
	                    return false;
	                }
	            };

	            var toggleAll = function (value) {
	                if (selected.length === value.length) {
	                    _.remove(selected);
	                } else {
	                    _.each(value, function (elm) {
	                        if(!_.includes(selected,elm)){
	                            selected.push(elm);
	                        }
	                    });
	                }
	            };

	            var getSelected = function () {
	                return _.map(selected,function(item){return item.id;});
	            };

	            var reset = function () {
	                _.remove(selected,function(){return true;});
	            };

	            return {
	                toggle : toggle,
	                isChecked : isChecked,
	                isCheckedAll : isCheckedAll,
	                toggleAll : toggleAll,
	                getSelected : getSelected,
	                reset : reset
	            };
	        };
	    }]);
	};

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.value('pagination', { perPages: [30, 50, 100] });
	};

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	'use strict';
	module.exports = function (module) {
	    module.directive('loadAware', ['$timeout','qs','$window',function ($timeout,qs,$window) {
	        return {
	            restrict: 'E',
	            transclude : true,
	            templateUrl: 'modules/core/views/partials/loadAware.html'
	        };
	    }]);
	};

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	'use strict';
	module.exports = function (module) {
	    module.directive('notificationsBar', ['$timeout','$rootScope','notify',function ($timeout,$rootScope,notify) {
	        return {
	            restrict: 'EA',
	            templateUrl: '/modules/core/views/partials/notifications.html',
	            link: function(scope) {
	                scope.hasNotification = function(){
	                    return notify.has();
	                };

	                scope.message = function(){
	                    return notify.notification();
	                };

	                scope.close = function (index) {
	                    notify.close();
	                };
	            }
	        };
	    }]);
	};

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	'use strict';
	module.exports = function (module) {
	    var _ = window._;

	    module.directive('permission', ['$timeout','$rootScope','USER', function ($timeout,$rootScope,USER) {
	        return {
	            scope: {
	                resource: '@',
	                action: '@',
	                section: '='
	            },
	            replace: false,
	            restrict: 'EA',
	            link: function($scope, element, attrs) {
	                if ((!$scope.section) || (!$scope.section.divider)) {
	                    if (USER.is_admin == '1') {
	                        element.show();
	                        return false;
	                    }
	                    else {
	                        var permission = _.find(USER.permissions, { action: $scope.action, resource: $scope.resource });

	                        if ((permission) && (_.find(USER.sites, { site_id: permission.site_id, role_id: permission.role_id }))) {
	                            element.show();
	                        }
	                        else {
	                            element.remove();
	                        }
	                    }
	                }
	            }
	        };
	    }]);
	};

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	'use strict';
	module.exports = function (module) {
	    module.directive('uiNav', ['$timeout', function ($timeout) {
	        return {
	            scope: {
	                data: '='
	            },
	            restrict: 'AC',
	            link: function(scope, el, attr) {
	                scope.$watch('scope.data', function() {
	                    $timeout(function() {
	                        el.find('a').bind('click', function(e) {
	                            var li = angular.element(this).parent();
	                            var active = li.parent()[0].querySelectorAll('.active');
	                            li.toggleClass('active');
	                            angular.element(active).removeClass('active');
	                        });
	                    }, 1);
	                });
	            }
	        };
	    }]);
	};

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filtersBar', ['$timeout', 'qs', '$rootScope', '$window', function ($timeout, qs, $rootScope, $window) {
	        var _ = window._;
	        return {
	            restrict: 'EA',
	            scope: {
	                availableFilters: '='
	            },
	            templateUrl: '/modules/core/views/filters/layout.html',
	            link: function ($scope, iElm, iAttrs) {
	                $scope.filters = qs.getAll();
	                $scope.editing = false;
	                $scope.active = 0;
	                $scope.query = '';

	                var KeyCodes = {
	                    BACKSPACE: 8,
	                    TABKEY: 9,
	                    RETURNKEY: 13,
	                    ESCAPE: 27,
	                    SPACEBAR: 32,
	                    LEFTARROW: 37,
	                    UPARROW: 38,
	                    RIGHTARROW: 39,
	                    DOWNARROW: 40
	                };
	                $window.onclick = function (event) {
	                    $rootScope.$broadcast('closeDD', event);
	                };

	                $scope.$on('closeDD', function (event) {
	                    $scope.close();
	                });

	                $scope.$watch('availableFilters', function (newVal, oldVal) {
	                    if (newVal) {
	                        $timeout(function () {
	                            $scope.used = _.filter($scope.availableFilters, function (item) {
	                                if (item.defaultValue !== undefined) {
	                                    return true;
	                                }
	                                return _.some($scope.filters, function (i, key) {
	                                    if (item.type === 'select') {
	                                        return item.txName === key;
	                                    }
	                                    if (item.type === 'multiselect') {
	                                        console.log(item.txName, key);
	                                        return item.txName === key;
	                                    }
	                                    if (item.type === 'text') {
	                                        return item.txName === key;
	                                    }
	                                    if (item.type === 'daterange') {
	                                        return (item.txName + '_start' === key || item.txName + '_end' === key);
	                                    }
	                                    if (item.type === 'calendardate') {
	                                        return (item.txName + '_start' === key || item.txName + '_end' === key || item.txName + '_view' === key || item.txName + '_currentDay' === key);
	                                    }
	                                });
	                            });

	                            $scope.available = _.filter($scope.availableFilters, function (item) {
	                                return !_.some($scope.used, function (i) {
	                                        return item.txName === i.txName;
	                                    }) && (item.type !== 'text' && item.payload && item.payload.length >= 1 || item.type === 'text' || item.type === 'calendardate');
	                            });

	                        });
	                    }
	                });


	                $scope.close = function () {
	                    $scope.editing = false;
	                    angular.element($window).unbind('keydown', $scope.keydown);
	                    $timeout(function () {
	                        $scope.$apply();
	                    });

	                };

	                $scope.activateNext = function () {
	                    if ($scope.available.length > $scope.active + 1) {
	                        ++$scope.active;
	                    }
	                    $timeout(function () {
	                        $scope.$apply();
	                    });
	                };

	                $scope.activatePrev = function () {
	                    if ($scope.active > 0) {
	                        --$scope.active;
	                    } else {
	                        $scope.active = 0;
	                    }
	                    $timeout(function () {
	                        $scope.$apply();
	                    });
	                };

	                $scope.keydown = function (e) {
	                    var handledKeys = [KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY];
	                    if (handledKeys.indexOf(e.which) === -1) {
	                        return;
	                    }

	                    if (e.which === KeyCodes.RETURNKEY) {
	                        e.preventDefault();
	                        $scope.addFilter($scope.available[$scope.active], e);
	                        $scope.close();
	                    }

	                    if (e.which === KeyCodes.DOWNARROW) {
	                        e.preventDefault();
	                        $scope.activateNext();
	                    }
	                    if (e.which === KeyCodes.UPARROW) {
	                        e.preventDefault();
	                        $scope.activatePrev();
	                    }
	                };

	                $scope.addFilter = function (item, e) {
	                    $rootScope.$broadcast('closeDD', e);
	                    e.stopPropagation();
	                    $timeout(function () {
	                        item.editing = true;
	                        $scope.used.push(item);
	                        $scope.available = _.filter($scope.available, function (elm) {
	                            return elm.txName !== item.txName;
	                        });
	                        //$scope.openEdit(e);
	                    });
	                };

	                $scope.apply = function (reload) {
	                    reload = reload || false;

	                    var values = _.chain($scope.used).keyBy('txName').transform(function (result, value, key) {
	                        if (value.type === 'select') {
	                            result[key] = value.value.id;
	                        }
	                        if (value.type === 'multiselect') {
	                            result[key] = _.map(value.value, 'id').join(',');
	                        }
	                        if (value.type === 'text') {
	                            result[key] = value.value;
	                        }
	                        if (value.type === 'daterange') {
	                            result[key + '_end'] = value.value.end;
	                            result[key + '_start'] = value.value.start;
	                        }
	                        if (value.type === 'calendardate') {
	                            result[key + '_end'] = value.value.end;
	                            result[key + '_start'] = value.value.start;
	                            result[key + '_view'] = value.value.view;
	                            result[key + '_currentDay'] = value.value.currentDay;
	                        }
	                    }).value();
	                    qs.setValues(values, true, reload);
	                };

	                $scope.clear = function () {
	                    $scope.available = _.filter($scope.availableFilters, function (item) {
	                        return item;
	                    });
	                    $scope.used = [];
	                    qs.setValues([], true);
	                };

	                $scope.openEdit = function (e) {
	                    if (e) {
	                        e.stopPropagation();
	                    }
	                    $rootScope.$broadcast('closeDD', e);
	                    $timeout(function () {
	                        $scope.editing = true;
	                        angular.element($window).bind('keydown', $scope.keydown);
	                    }, 50);
	                };


	                $scope.remove = function (item) {
	                    $scope.available.push(item);
	                    $scope.used = _.filter($scope.used, function (elm) {
	                        return elm.txName !== item.txName;
	                    });
	                    if (angular.isDefined(item.reset)) {
	                        item.reset();
	                    }
	                };

	                $scope.editPrevious = function (item) {
	                    var idx = _.findIndex($scope.used, function (elm) {
	                        return elm.txName === item.txName;
	                    });

	                    var next = $scope.used[--idx];

	                    if (next) {
	                        item.editing = false;
	                        $timeout(function () {
	                            next.editing = true;
	                            next.focus();
	                            $scope.$apply();
	                        });

	                    }
	                };

	                $scope.editNext = function (item) {
	                    var idx = _.findIndex($scope.used, function (elm) {
	                        return elm.txName === item.txName;
	                    });

	                    var next = $scope.used[++idx];

	                    if (next) {
	                        item.editing = false;
	                        $timeout(function () {
	                            next.editing = true;
	                            next.focus();
	                            $scope.$apply();
	                        });
	                    } else {
	                        item.editing = false;
	                    }
	                };

	            }
	        };
	    }]);
	};

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filtersList', ['qs', '$timeout', function (qs, $timeout) {
	        var _ = window._;
	        return {
	            restrict: 'E',
	            templateUrl: '/modules/core/views/filters/list.html',
	            scope: {
	                availableFilters: '='
	            },
	            link: function ($scope, iElm, iAttrs) {
	                $scope.filters = qs.getAll();

	                $scope.$watch('availableFilters', function (value) {
	                    if (value) {
	                        $scope.list = _.filter(_.map(value, function (item) {
	                            if (item.type === 'daterange' || item.type === 'calendardate') {
	                                if ($scope.filters.hasOwnProperty(item.txName + '_end')) {
	                                    return {
	                                        display: item.display,
	                                        value: [$scope.filters[item.txName + '_start'] + ' to ' + $scope.filters[item.txName + '_end']]
	                                    };
	                                }
	                            }
	                            if (item.type === 'select' || item.type === 'multiselect') {
	                                if ($scope.filters.hasOwnProperty(item.txName)) {
	                                    var itemValue = $scope.filters[item.txName];

	                                    // find value in payload
	                                    if (item.hasOwnProperty('payload')) {
	                                        var txNameArray = [];
	                                        var filtersArray = ($scope.filters[item.txName]).split(',');
	                                        _.forEach(filtersArray, function (filter) {
	                                            txNameArray.push(_.find(item.payload, {id: parseInt(filter)}));
	                                        });
	                                        itemValue = _.map(txNameArray, 'txName');
	                                    }

	                                    return {display: item.display, value: itemValue};
	                                }
	                            }
	                            if (item.type === 'text') {
	                                if ($scope.filters.hasOwnProperty(item.txName)) {
	                                    return {display: item.display, value: [$scope.filters[item.txName]]};
	                                }
	                            }

	                            return false;
	                        }));
	                    }
	                });
	            }
	        };
	    }]);
	};

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterName', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
	        return {
	            restrict: 'A',
	            replace: true,
	            transcluded: true,
	            scope: {
	                filterName: '='
	            },
	            templateUrl: '/modules/core/views/filters/name.html',
	            link: function ($scope, iElm, iAttrs) {
	                var KeyCodes = {
	                    BACKSPACE: 8,
	                    TABKEY: 9,
	                    RETURNKEY: 13,
	                    ESCAPE: 27,
	                    SPACEBAR: 32,
	                    LEFTARROW: 37,
	                    UPARROW: 38,
	                    RIGHTARROW: 39,
	                    DOWNARROW: 40
	                };
	                $scope.$watch('filterName', function (newval, oldval) {
	                    if (newval) {
	                        $scope.filterName.value = qs.getValue(newval.txName);
	                    }
	                });


	                $scope.edit = function (val, e) {
	                    e.stopPropagation();
	                    $scope.filterName.editing = val;

	                    if (!val && !$scope.filterName.value) {
	                        $scope.$parent.remove($scope.filterName);
	                    }
	                };

	                $scope.keydown = function (e) {
	                    var handledKeys = [KeyCodes.BACKSPACE, KeyCodes.TABKEY, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
	                    if (handledKeys.indexOf(e.which) === -1) {
	                        return;
	                    }
	                    if (e.which === KeyCodes.BACKSPACE && !$scope.filterName.value) {
	                        e.preventDefault();
	                        $scope.$parent.editPrevious($scope.filterName);
	                        $scope.$parent.remove($scope.filterName);
	                    }
	                    if (e.which === KeyCodes.TABKEY) {
	                        console.log('EDIT NEXT');
	                        $scope.filterName.editing = false;
	                        $scope.$parent.editNext($scope.filterName);
	                    }
	                    if (e.which === KeyCodes.RETURNKEY) {
	                        $scope.$parent.apply();
	                    }
	                    console.log('Pressed', e.which);
	                };
	            }
	        };
	    }]);
	};

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterSelect', ['$timeout', 'qs', '$window', '$rootScope', function ($timeout, qs, $window, $rootScope) {
	        var _ = window._;
	        return {
	            restrict: 'A',
	            scope: {
	                filterSelect: '='
	            },
	            templateUrl: '/modules/core/views/filters/select.html',
	            controller: function ($scope) {


	            },
	            link: function ($scope, iElm, iAttrs) {
	                var input = iElm.find('input');
	                var prevHandler;
	                $scope.active = -1;
	                $scope.query = null;
	                $scope.currentActive = null;
	                $scope.startIdx = 0;
	                $scope.items = _.filter($scope.filterSelect.payload, function (item) {
	                    if (!$scope.query) {
	                        return true;
	                    }
	                    return item.txName.toLowerCase().indexOf($scope.query.toLowerCase()) > -1;
	                });
	                $scope.endIdx = $scope.filterSelect.payload.length;

	                $scope.$on('closeDD', function (event) {
	                    $scope.close();
	                });
	                $scope.$watch('query', _.debounce(function (newval, oldval) {
	                    if (newval !== oldval) {
	                        $scope.$apply(function () {
	                            $scope.items = _.filter($scope.filterSelect.payload, function (item) {
	                                if (!newval) {
	                                    return true;
	                                }
	                                return item.txName.toLowerCase().indexOf(newval.toLowerCase()) > -1;
	                            });
	                            $scope.endIdx = $scope.items.length;
	                            $scope.active = -1;
	                        });
	                    }
	                }, 200));
	                $scope.$watch('filterSelect', function (newval, oldval) {
	                    if (newval) {
	                        $scope.filterSelect.focus = function () {
	                            $timeout(function () {
	                                input[0].focus();
	                            });
	                        };
	                        $scope.filterSelect.value = _.find($scope.filterSelect.payload, function (item) {
	                            if (!isNaN(item.id)) {
	                                return parseInt(item.id) === parseInt(qs.getValue($scope.filterSelect.txName));
	                            }
	                            return item.id === qs.getValue($scope.filterSelect.txName);
	                        });
	                        if ($scope.filterSelect.editing) {
	                            $scope.filterSelect.focus();
	                        }
	                        // set default if no value defined
	                        if (!$scope.filterSelect.value && !qs.getValue($scope.filterSelect.txName)) {
	                            if (_.isPlainObject($scope.filterSelect.defaultValue) && _.has($scope.filterSelect.defaultValue, 'id') && _.has($scope.filterSelect.defaultValue, 'txName')) {
	                                $scope.filterSelect.value = $scope.filterSelect.defaultValue;
	                            }
	                            else {
	                                $scope.filterSelect.value = {
	                                    id: $scope.filterSelect.defaultValue,
	                                    txName: $scope.filterSelect.defaultValue
	                                };
	                            }
	                        }
	                        $timeout(function () {
	                            $scope.$apply();
	                        });
	                    }
	                });
	                var KeyCodes = {
	                    BACKSPACE: 8,
	                    TABKEY: 9,
	                    RETURNKEY: 13,
	                    ESCAPE: 27,
	                    SPACEBAR: 32,
	                    LEFTARROW: 37,
	                    UPARROW: 38,
	                    RIGHTARROW: 39,
	                    DOWNARROW: 40
	                };

	                $scope.close = function () {
	                    $scope.filterSelect.editing = false;
	                    $scope.query = '';

	                    if ($scope.filterSelect.value.id === undefined) {
	                        $scope.$parent.remove($scope.filterSelect);
	                    }
	                };

	                $scope.edit = function (val, e) {
	                    $rootScope.$broadcast('closeDD', e);
	                    if (val) {
	                        $scope.filterSelect.focus();
	                    }
	                    $scope.filterSelect.editing = val;

	                    $scope.$broadcast('vsRepeatTrigger', {
	                        scrollIndex: $scope.active,
	                        scrollIndexPosition: 'inview#auto'
	                    });
	                    e.stopPropagation();
	                };

	                $scope.select = function (filter, e) {
	                    e.stopPropagation();
	                    $scope.filterSelect.value = filter;
	                    $scope.close();
	                    $scope.$parent.editNext($scope.filterSelect);
	                };

	                $scope.setActive = function (item) {
	                    if (item) {
	                        $scope.currentActive = item;
	                    }
	                };


	                $scope.activateNext = function () {
	                    if ($scope.endIdx > $scope.active + 1) {
	                        ++$scope.active;
	                    }
	                };

	                $scope.activatePrev = function () {
	                    if ($scope.active > 0) {
	                        --$scope.active;
	                    } else {
	                        $scope.active = -1;
	                    }
	                };

	                $scope.isActive = function (idx) {
	                    return idx === $scope.active ? 'active' : '';
	                };

	                $scope.keydown = function (e) {
	                    var handledKeys = [KeyCodes.TABKEY, KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
	                    if (handledKeys.indexOf(e.which) === -1) {
	                        return;
	                    }
	                    if (e.which === KeyCodes.RETURNKEY) {
	                        e.preventDefault();
	                        if ($scope.active >= 0) {
	                            $scope.filterSelect.value = $scope.items[$scope.active];
	                            $scope.$parent.editNext($scope.filterSelect);
	                        }
	                        $scope.close();
	                    }
	                    if (e.which === KeyCodes.TABKEY && $scope.filterSelect.value) {
	                        e.preventDefault();
	                        if ($scope.active >= 0) {
	                            $scope.filterSelect.value = $scope.filterSelect.payload[$scope.active];
	                        }
	                        $scope.close();
	                        $scope.$parent.editNext($scope.filterSelect);
	                    }
	                    if (e.which === KeyCodes.DOWNARROW) {
	                        e.preventDefault();
	                        $scope.activateNext();
	                    }
	                    if (e.which === KeyCodes.UPARROW) {
	                        e.preventDefault();
	                        $scope.activatePrev();
	                    }
	                    $scope.$broadcast('vsRepeatTrigger', {
	                        scrollIndex: $scope.active,
	                        scrollIndexPosition: 'inview#auto'
	                    });
	                };
	            }
	        };
	    }]);
	};

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterMultiSelect', ['$timeout', 'qs', '$window', '$rootScope', function ($timeout, qs, $window, $rootScope) {
	        var _ = window._;
	        return {
	            restrict: 'A',
	            scope: {
	                filterMultiSelect: '='
	            },
	            templateUrl: '/modules/core/views/filters/multiselect.html',
	            controller: function ($scope) {


	            },
	            link: function ($scope, iElm, iAttrs) {
	                var input = iElm.find('input');
	                var prevHandler;
	                $scope.max = 19;
	                $scope.active = -1;
	                $scope.query = null;
	                $scope.currentActive = null;
	                $scope.startIdx = 0;
	                $scope.filterMultiSelect.value = [];
	                $scope.original = _.cloneDeep($scope.filterMultiSelect.payload);

	                $scope.items = _.sortBy($scope.filterMultiSelect.payload, function (el) {
	                    return el.name.toLowerCase();
	                });

	                $scope.endIdx = $scope.filterMultiSelect.payload.length;

	                var sync = function () {
	                    $timeout(function () {
	                        $scope.items = _.sortBy($scope.filterMultiSelect.payload, function (el) {
	                            return el.name.toLowerCase();
	                        });
	                        $scope.$apply();
	                        $scope.$broadcast('vsRepeatTrigger');
	                    });
	                };

	                var add = function (item) {
	                    $scope.filterMultiSelect.value.push(item);
	                    _.remove($scope.filterMultiSelect.payload, function (el) {
	                        return el.id === item.id;
	                    });
	                    $scope.query = '';
	                    sync();
	                };

	                var del = function (item) {
	                    _.remove($scope.filterMultiSelect.value, item, 'id');
	                    $scope.filterMultiSelect.payload.push(item);
	                    sync();

	                };

	                $scope.filterMultiSelect.reset = function () {
	                    $scope.filterMultiSelect.payload = $scope.original;
	                    $scope.filterMultiSelect.value = [];
	                    sync();
	                };


	                $scope.$on('closeDD', function (event) {
	                    $scope.close();
	                });
	                $scope.$watch('query', _.debounce(function (newval, oldval) {
	                    if (newval !== oldval) {
	                        console.log('QUERY', newval, $scope.filterMultiSelect.editing);
	                        $scope.$apply(function () {
	                            $scope.items = _.filter($scope.filterMultiSelect.payload, function (item) {
	                                if (!newval) {
	                                    return true;
	                                }
	                                return item.name.toLowerCase().indexOf(newval.toLowerCase()) > -1;
	                            });
	                            $scope.endIdx = $scope.items.length;
	                            $scope.active = -1;
	                            console.log('QUERY2', $scope.items, $scope.filterMultiSelect.editing);
	                        });
	                    }
	                }, 200));
	                $scope.$watch('filterMultiSelect', function (newval, oldval) {
	                    if (newval) {
	                        $scope.filterMultiSelect.focus = function () {
	                            $timeout(function () {
	                                input[0].focus();
	                            });
	                        };
	                        var val = _.filter($scope.filterMultiSelect.payload, function (item) {
	                            var qsvalue = qs.getValue($scope.filterMultiSelect.txName);
	                            return qsvalue && _.includes(qsvalue.split(','), '' + item.id);
	                        });
	                        if (val) {
	                            $scope.filterMultiSelect.value = val;
	                        }

	                        if ($scope.filterMultiSelect.editing) {
	                            $scope.filterMultiSelect.focus();
	                        }
	                        // set default if no value defined
	                        if (!$scope.filterMultiSelect.value.length && !qs.getValue($scope.filterMultiSelect.txName)) {
	                            $scope.filterMultiSelect.value = _.filter($scope.filterMultiSelect.payload, function (item) {
	                                var qsvalue = $scope.filterMultiSelect.defaultValue;
	                                return qsvalue && _.includes(qsvalue.split(','), '' + item.id);
	                            });
	                        }
	                        _.remove($scope.filterMultiSelect.payload, function (item) {
	                            return _.includes($scope.filterMultiSelect.value, item);
	                        });
	                        sync();
	                        $timeout(function () {
	                            $scope.$apply();
	                        });
	                    }
	                });
	                var KeyCodes = {
	                    BACKSPACE: 8,
	                    TABKEY: 9,
	                    RETURNKEY: 13,
	                    ESCAPE: 27,
	                    SPACEBAR: 32,
	                    LEFTARROW: 37,
	                    UPARROW: 38,
	                    RIGHTARROW: 39,
	                    DOWNARROW: 40
	                };

	                $scope.close = function () {
	                    $scope.filterMultiSelect.editing = false;
	                    $scope.query = '';

	                    if ($scope.filterMultiSelect.value.length === 0) {
	                        $scope.$parent.remove($scope.filterMultiSelect);
	                    }
	                };


	                $scope.isSelected = function (filter) {
	                    return _.includes($scope.filterMultiSelect.value, filter);
	                };
	                $scope.removeSelected = function (filter) {
	                    del(filter);
	                };
	                $scope.edit = function (val, e) {
	                    if (val) {
	                        $scope.filterMultiSelect.focus();
	                    }
	                    $scope.filterMultiSelect.editing = val;
	                    $scope.$broadcast('vsRepeatTrigger', {scrollIndex: 0, scrollIndexPosition: 'inview#auto'});
	                    e.stopPropagation();
	                };

	                $scope.select = function (filter, e) {
	                    e.stopPropagation();
	                    add(filter);
	                };

	                $scope.setActive = function (item) {
	                    if (item) {
	                        $scope.currentActive = item;
	                    }
	                };


	                $scope.activateNext = function () {
	                    if ($scope.endIdx > $scope.active + 1) {
	                        ++$scope.active;
	                    }
	                    $scope.$broadcast('vsRepeatTrigger', {
	                        scrollIndex: $scope.active,
	                        scrollIndexPosition: 'inview#auto'
	                    });
	                };

	                $scope.activatePrev = function () {
	                    if ($scope.active > 0) {
	                        --$scope.active;
	                    } else {
	                        $scope.active = -1;
	                    }
	                    $scope.$broadcast('vsRepeatTrigger', {
	                        scrollIndex: $scope.active - 1,
	                        scrollIndexPosition: 'inview#top'
	                    });
	                };

	                $scope.isActive = function (idx) {
	                    return idx === $scope.active ? 'active' : '';
	                };

	                $scope.keydown = function (e) {
	                    var handledKeys = [KeyCodes.TABKEY, KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
	                    if (handledKeys.indexOf(e.which) === -1) {
	                        return;
	                    }
	                    if (e.which === KeyCodes.RETURNKEY) {
	                        e.preventDefault();
	                        if ($scope.active >= 0) {
	                            add($scope.items[$scope.active]);
	                        }
	                    }
	                    if (e.which === KeyCodes.TABKEY && $scope.filterMultiSelect.value) {
	                        e.preventDefault();
	                        if ($scope.active >= 0) {
	                            add($scope.filterMultiSelect.payload[$scope.active]);
	                        }
	                        $scope.close();
	                        $scope.$parent.editNext($scope.filterMultiSelect);
	                    }
	                    if (e.which === KeyCodes.DOWNARROW) {
	                        e.preventDefault();
	                        $scope.activateNext();
	                    }
	                    if (e.which === KeyCodes.UPARROW) {
	                        e.preventDefault();
	                        $scope.activatePrev();
	                    }
	                };
	            }
	        };
	    }]);
	};

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterDaterange', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
	        return {
	            restrict: 'A',
	            replace: true,
	            transcluded: true,
	            scope: {
	                filterDaterange: '='
	            },
	            templateUrl: '/modules/core/views/filters/daterange.html',
	            controller: function ($scope) {

	                window.moment.locale('en');
	                var now = window.moment();
	                //$scope.myDates={ start:now.format( 'YYYY-MM-DD' ) , end:now.format( 'YYYY-MM-DD' ) };
	                //$scope.filterDaterange.value={ start:now.format( 'YYYY-MM-DD' ) , end:now.format( 'YYYY-MM-DD' ) };

	                if (qs.getValue($scope.filterDaterange.txName + '_start') && qs.getValue($scope.filterDaterange.txName + '_end')) {
	                    $scope.filterDaterange.value = {
	                        start: window.moment(qs.getValue($scope.filterDaterange.txName + '_start')).format('YYYY-MM-DD'),
	                        end: window.moment(qs.getValue($scope.filterDaterange.txName + '_end')).format('YYYY-MM-DD')
	                    };
	                } else {
	                    $scope.filterDaterange.value = {start: now.format('YYYY-MM-DD'), end: now.format('YYYY-MM-DD')};
	                }

	            },
	            link: function ($scope, iElm, iAttrs) {
	                $scope.editing = false;
	                var KeyCodes = {
	                    BACKSPACE: 8,
	                    TABKEY: 9,
	                    RETURNKEY: 13,
	                    ESCAPE: 27,
	                    SPACEBAR: 32,
	                    LEFTARROW: 37,
	                    UPARROW: 38,
	                    RIGHTARROW: 39,
	                    DOWNARROW: 40
	                };


	                $scope.focus = function () {
	                    $scope.editing = true;
	                };
	                $scope.setValue = function (result) {
	                    $scope.filterDaterange.value = result;
	                    //$scope.$parent.editNext($scope.filterDaterange);
	                };

	            }
	        };
	    }]);
	};

/***/ }),
/* 21 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterAutoResize', [
	            function () {
	                return {
	                    restrict: 'A',
	                    scope: {
	                        model: '=ngModel'
	                    },
	                    link: function ($scope, $element, $attrs) {
	                        var container = angular.element('<div style="position: fixed; top: -9999px; left: 0px;"></div>');
	                        var shadow = angular.element('<span style="white-space:pre;"></span>');

	                        var maxWidth = $element.css('maxWidth') === 'none' ? $element.parent().innerWidth() : $element.css('maxWidth');
	                        $element.css('maxWidth', maxWidth);

	                        angular.forEach([
	                            'fontSize', 'fontFamily', 'fontWeight', 'fontStyle',
	                            'letterSpacing', 'textTransform', 'wordSpacing', 'textIndent',
	                            'boxSizing', 'borderLeftWidth', 'borderRightWidth', 'borderLeftStyle', 'borderRightStyle',
	                            'paddingLeft', 'paddingRight', 'marginLeft', 'marginRight'
	                        ], function (css) {
	                            shadow.css(css, $element.css(css));
	                        });

	                        angular.element('body').append(container.append(shadow));

	                        function resize() {
	                            shadow.text($element.val() || $element.attr('placeholder'));
	                            $element.css('width', shadow.outerWidth() + ($element.val().length === 0 ? 75 : 0));
	                        }

	                        resize();

	                        if ($scope.model) {
	                            $scope.$watch('model', function () {
	                                resize();
	                            });
	                        } else {
	                            $element.on('keypress keyup keydown focus input propertychange change', function () {
	                                resize();
	                            });
	                        }
	                    }
	                };
	            }]
	    );
	};

/***/ }),
/* 22 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.directive('filterFocus', ['$timeout', '$parse',
	        function ($timeout, $parse) {
	            return {
	                restrict: 'A',
	                link: function ($scope, $element, $attrs) {
	                    var model = $parse($attrs.filterFocus);
	                    $scope.$watch(model, function (value) {
	                        if (value === true) {
	                            $timeout(function () {
	                                $element[0].focus();
	                            });
	                        }
	                    });
	                    $element.bind('blur', function () {
	                        $scope.$apply(model.assign($scope, false));
	                    });
	                }
	            };
	        }
	    ]);
	};

/***/ }),
/* 23 */
/***/ (function(module, exports) {

	'use strict';

	var moment = window.moment;

	module.exports = function (module) {
	    module.directive('filterCalendarDate', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
	        return {
	            restrict: 'A',
	            replace: true,
	            transcluded: true,
	            scope: {
	                filterCalendarDate: '='
	            },
	            templateUrl: '/modules/core/views/filters/calendarDate.html',
	            link: function ($scope, iElm, iAttrs) {
	                $scope.currentDate = $scope.$parent.$parent.$parent.$parent.calendarDay;

	                $scope.filterCalendarDate.value = $scope.filterCalendarDate.defaultValue;

	                $scope.filterCalendarDate.value.start = qs.getValue($scope.filterCalendarDate.txName + '_start', $scope.filterCalendarDate.defaultValue.start);
	                $scope.filterCalendarDate.value.end = qs.getValue($scope.filterCalendarDate.txName + '_end', $scope.filterCalendarDate.defaultValue.end);
	                $scope.filterCalendarDate.value.view = qs.getValue($scope.filterCalendarDate.txName + '_view', $scope.filterCalendarDate.defaultValue.view);
	                $scope.filterCalendarDate.value.currentDay = qs.getValue($scope.filterCalendarDate.txName + '_currentDay', $scope.filterCalendarDate.defaultValue.currentDay);

	                $scope.nav = function (direction) {
	                    var objDate = {dtEvent_view: $scope.filterCalendarDate.value.view};
	                    var now;
	                    if (direction === 'left') {
	                        now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString()).subtract(1, $scope.filterCalendarDate.value.view + 's');
	                    }
	                    else if (direction === 'right') {
	                        now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString()).add(1, $scope.filterCalendarDate.value.view + 's');
	                    }
	                    else {
	                        now = moment(new Date().toString());
	                    }
	                    var dtEvent_start, dtEvent_end = now;

	                    objDate.dtEvent_currentDay = now.format('YYYY-MM-DD');

	                    if ($scope.filterCalendarDate.value.view === 'month') {
	                        dtEvent_start = now.startOf('month');
	                    }
	                    else if ($scope.filterCalendarDate.value.view === 'week') {
	                        dtEvent_start = now.startOf('week');
	                    }
	                    else {
	                        dtEvent_start = now;
	                    }

	                    objDate.dtEvent_start = dtEvent_start.format('YYYY-MM-DD');

	                    if ($scope.filterCalendarDate.value.view === 'month') {
	                        dtEvent_end = now.endOf('month');
	                    }
	                    else if ($scope.filterCalendarDate.value.view === 'week') {
	                        dtEvent_end = now.endOf('week');
	                    }
	                    else {
	                        dtEvent_end = now;
	                    }

	                    objDate.dtEvent_end = dtEvent_end.format('YYYY-MM-DD');

	                    $scope.filterCalendarDate.value = objDate;

	                    qs.setValues(objDate);
	                };

	                $scope.day = function () {
	                    var objDate = {dtEvent_view: 'day'};
	                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

	                    objDate.dtEvent_start = now.format('YYYY-MM-DD');
	                    objDate.dtEvent_end = now.format('YYYY-MM-DD');

	                    $scope.filterCalendarDate.value = objDate;

	                    qs.setValues(objDate);
	                };

	                $scope.week = function () {
	                    var objDate = {dtEvent_view: 'week'};
	                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

	                    objDate.dtEvent_start = now.startOf('week').format('YYYY-MM-DD');
	                    objDate.dtEvent_end = now.endOf('week').format('YYYY-MM-DD');

	                    $scope.filterCalendarDate.value = objDate;

	                    qs.setValues(objDate);
	                };

	                $scope.month = function () {
	                    var objDate = {dtEvent_view: 'month'};
	                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

	                    objDate.dtEvent_start = now.startOf('month').format('YYYY-MM-DD');
	                    objDate.dtEvent_end = now.endOf('month').format('YYYY-MM-DD');

	                    $scope.filterCalendarDate.value = objDate;

	                    qs.setValues(objDate);
	                };

	                $scope.getCurrentView = function (view) {
	                    return view === $scope.filterCalendarDate.value.view;
	                };
	            }
	        };
	    }]);
	};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Filters
	 * @module: app.core
	 * @desc: Switch case in template
	 */
	module.exports = function (module) {
	    module.filter('highlight',['$sce',function ($sce) {
	        return function(text, phrase) {
	            if (phrase) {
	                text = text.replace(new RegExp('('+phrase+')', 'gi'),'<span class="highlighted">$1</span>');
	            }

	            return $sce.trustAsHtml(text);
	        };
	    }]);
	};

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var mod = angular.module('app.dashboard',[]);

	__webpack_require__(26)(mod);
	__webpack_require__(27)(mod);
	__webpack_require__(28)(mod);

/***/ }),
/* 26 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.config(['$stateProvider',function ($stateProvider) {

		    $stateProvider.state('default.home', {
			    url: '/dashboard?options',
			    controller : 'default.home',
			    templateUrl: 'modules/dashboard/views/index.html',
			    data: {
				    title: 'Dashboard'
			    },
	            ncyBreadcrumb: {
	                label: 'Home'
	            }
		    });
	    }]);
	};

/***/ }),
/* 27 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    return module.factory('Dashboard', ['$resource',function ($resource) {
	        return $resource('/dashboard/:orderId',{orderId:'@orderId'},{
	            query : {
	                isArray:false
	            },
	            update :{
	                method : 'PUT'
	            },
	            stats: {
	                method: 'GET',
	                url: '/dashboard/stats'
	            },
	            cart: {
	                method: 'GET',
	                url: '/dashboard/cart'
	            },

	        });
	    }]);
	};



/***/ }),
/* 28 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    var moment = window.moment;

	    module.controller('default.home', [ '$scope','ld','Dashboard','$timeout','qs','$sce', function ( $scope,ld, Dashboard,$timeout,qs,$sce) {

	    }]);
	};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var mod = angular.module('app.users',[]);

	__webpack_require__(30)(mod);
	__webpack_require__(31)(mod);
	__webpack_require__(32)(mod);

/***/ }),
/* 30 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	  module.config(['$stateProvider',function ($stateProvider) {

	      $stateProvider.state('default.users', {
	          url: '/users',
	          template: '<ui-view></ui-view>',
	          abstract: true,
	          ncyBreadcrumb: {skip : true},
	          data: {
	              title: 'Users'
	          }
	      })
	      .state('default.users.list', {
	          url: '/list?options',
	          controller: 'default.users',
	          templateUrl: 'modules/users/views/index.html',
	          ncyBreadcrumb: {
	              parent:'default.home',
	              label: 'Users'
	          }
	      })
	      .state('default.users.edit', {
	          url: '/:userId/edit',
	          controller: 'users.edit',
	          templateUrl: 'modules/users/views/edit.html',
	          abstract: true,
	          ncyBreadcrumb: {
	              parent:'default.users.list',
	              label: '{{ item.name }}'
	          }
	      })
	      .state('default.users.edit.details', {
	          url: '/details',
	          controller: 'users.edit.details',
	          templateUrl: 'modules/users/views/details.html',
	          ncyBreadcrumb: {
	              label: '{{ item.name }}',
	              parent: 'default.users.edit'
	          }
	      })
	      .state('default.users.edit.roles', {
	          url: '/roles',
	          controller: 'users.edit.roles',
	          templateUrl: 'modules/users/views/roles.html',
	          ncyBreadcrumb: {
	              label: '{{ item.name }}',
	              parent: 'default.users.edit'
	          }
	      })
	      .state('default.users.new', {
	          url: '/new',
	          controller: 'users.edit.new',
	          templateUrl: 'modules/users/views/details.html',
	          ncyBreadcrumb: {
	              parent:'default.users.list',
	              label: 'New user'
	          }
	      })
	      .state('default.users.edit.delete', {
	          url: '/delete',
	          templateUrl: 'modules/users/views/delete.html',
	          controller: 'users.edit.delete',
	          ncyBreadcrumb: {
	              label: '{{ item.name }}',
	              parent: 'default.users.edit'
	          }
	      })
	      .state('default.users.edit.impersonate', {
	          controller: 'users.impersonate',
	          url: '/impersonate/:userId',
	          ncyBreadcrumb: {
	              skip: true
	          }
	      })
	      .state('default.users.edit.unimpersonate', {
	          controller: 'users.unimpersonate',
	          url: '/unimpersonate',
	          ncyBreadcrumb: {
	              skip: true
	          }
	      })
	      ;
	    }
	  ]);
	};

/***/ }),
/* 31 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    var moment = window.moment;

	    module.controller('default.users', ['$scope', 'Users', 'qs','ld','pagination','$timeout','$window','checkboxes','$state','notify', function ($scope, Users, qs,ld, pagination, $timeout,$window,checkboxes,$state,notify) {
	        $scope.items = [];
	        $scope.meta = {};
	        $scope.pagination = {};
	        $scope.loading = true;

	        $scope.bulkDelete = function (items, $event) {
	            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
	                ld(Users.bulkDelete({ items: items }).$promise.then(function (result) {
	                    checkboxes($scope.selected).reset();
	                    return $state.reload().then(function () {
	                        notify.success('Deleted successfully');
	                    });
	                }),$scope);
	            }
	        };

	        ld(Users.query(qs.getAll()).$promise.then(function (result) {
	            $scope.meta = result.meta;
	            $scope.items = result.items;
	            $scope.pagination = result.pagination;

	            $scope.loading = false;

	            $scope.filters = [
	                {
	                    txName: 'created_at',
	                    type: 'daterange',
	                    display: 'Created',
	                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
	                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
	                },
	                { txName: 'name', type: 'text', display: 'Name' },
	                { txName: 'email', type: 'text', display: 'E-mail' },
	                { txName: 'gender', type: 'select', display: 'Gender', payload: [{id: 'male', txName: 'Male'}, {id: 'female', txName: 'Female'}] }
	            ];
	        }),$scope);
	    }])
	    .controller('users.edit', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','Upload','notify','$state','USER', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams,Upload,notify,$state, USER) {
	        $scope.item = {};
	        $scope.meta = {};
	        $scope.loading = true;
	        $scope.progress = 0;
	        $scope.user = USER;

	        $scope.sections = [
	            {
	                divider: true,
	                name: 'Main',
	                showWhenNew: true
	            },
	            {
	                divider: false,
	                name: 'Details',
	                icon: 'edit',
	                state: 'default.users.edit.details',
	                showWhenNew: true,
	                resource: 'USERS',
	                action: 'UPDATE'
	            },
	            {
	                divider: false,
	                name: 'Roles',
	                icon: 'lock_open',
	                state: 'default.users.edit.roles',
	                showToAdmin: true,
	                resource: 'USERS',
	                action: 'UPDATE'
	            },
	            {
	                divider: true,
	                name: 'Options',
	                showWhenNew: false
	            },
	            {
	                divider: false,
	                name: 'Impersonate',
	                icon: 'accessibility',
	                state: 'default.users.edit.impersonate',
	                showWhenNew: false,
	                resource: 'USERS',
	                action: 'IMPERSONATE'
	            },
	            {
	                divider: false,
	                name: 'Delete',
	                icon: 'delete',
	                state: 'default.users.edit.delete',
	                showWhenNew: false,
	                resource: 'USERS',
	                action: 'DELETE'
	            }
	        ];

	        ld(Users.get({ userId: $stateParams.userId }).$promise.then(function (result) {
	            $scope.item = result.item;
	            $scope.item.newsletter = $scope.item.newsletter ? true : false;

	            $scope.meta = result.meta;

	            $scope.$watch('avatar', function (newValue, oldValue) {
	                $scope.upload($scope.avatar);
	            });

	            $scope.upload = function (file) {
	                if (file) {
	                    $scope.uploading = true;
	                    $scope.uploaded = false;

	                    Upload.upload({
	                        url: '/users/' + $scope.item.id + '/upload',
	                        file: file
	                    }).progress(function (evt) {
	                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                        $scope.progress = progressPercentage;

	                    }).success(function (data, status, headers, config) {
	                        $scope.progress = 0;
	                        $scope.uploading = false;
	                        $scope.uploaded = true;

	                        if (data.status === 'OK') {
	                            $scope.error = false;

	                            $scope.item.avatar = data.path;

	                            return $state.reload().then(function () {
	                                notify.success('Save successfully');
	                            });
	                        }
	                        else {
	                            $scope.error = data.error;
	                            notify.error('Try a different image');
	                        }
	                    }).error(function (data, status, headers, config) {
	                        notify.error('General error. Try a different image');
	                    });
	                }
	            };
	        }),$scope);
	    }])
	    .controller('users.edit.details', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','$state','notify','Upload', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams, $state, notify, Upload) {
	        $scope.newItem = false;

	        $scope.save = function(form) {
	            $scope.$broadcast('show-errors-check-validity');

	            if (form.$valid) {
	                ld(Users.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    return $state.reload().then(function () {
	                        $scope.item = result.item;
	                        notify.success('Save successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to update user', form)), $scope);
	            }
	        };
	    }])
	    .controller('users.edit.roles', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','$state','notify','USER', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams, $state, notify, USER) {
	        $scope.items = [];
	        $scope.user = USER;
	        $scope.meta = {};

	        ld(Users.get_roles({ userId: $stateParams.userId }).$promise.then(function (result) {
	            $scope.items = _.map(result.items, function(item) { item.active = item.active == 1; return item; });
	            $scope.meta = result.meta;

	        }), $scope);

	        $scope.save = function(form) {
	            $scope.$broadcast('show-errors-check-validity');

	            if (form.$valid) {
	                ld(Users.save_roles(angular.extend({ userId: $stateParams.userId }, { items: $scope.items })).$promise.then(function (result) {
	                    return $state.reload().then(function () {
	                        notify.success('Save successfully');
	                    });

	                }).catch($scope.errorHandler('Failed to update roles', form)), $scope);
	            }
	        };
	    }])
	    .controller('users.edit.new', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','notify','$state', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams,notify,$state) {
	        $scope.item = {};
	        $scope.meta = {};
	        $scope.newItem = true;

	        ld(Users.create().$promise.then(function (result) {
	            $scope.meta = result.meta;
	        }), $scope);

	        $scope.save = function(form){
	            $scope.$broadcast('show-errors-check-validity');
	            if (form.$valid) {
	                ld(Users.save($scope.item).$promise.then(function (result) {
	                    $state.go('.list', {}, {reload: true}).then(function () {
	                        notify.success('Saved successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to create user',form)), $scope);
	            }
	        };
	    }])
	    .controller('users.impersonate', ['$scope', 'Users', 'ld','notify','$stateParams','$state','$window', function ($scope, Users, ld,notify, $stateParams, $state, $window) {
	        ld(Users.impersonate({ userId: $statePar^ams.userId }).$promise.then(function(result){
	            notify.success('Impersonated successfully');
	            $window.location.href = '/';
	        }).catch(function (result) {
	            notify.error('Failed to impersonate user');
	        }),$scope);
	    }])
	    .controller('users.unimpersonate', ['$scope', 'Users', 'ld','notify','$state', function ($scope, Users, ld,notify,$state) {
	        ld(Users.unimpersonate().$promise.then(function(result){
	            return $state.go('default.home',{},{reload: true}).then(function () {
	                notify.success('Switched back successfully');
	            });
	        }).catch(function (result) {
	            notify.error('Failed to switch back');
	        }),$scope);
	    }])
	    .controller('users.edit.delete', ['$scope', 'Users', 'ld','notify','$state','$stateParams', function ($scope, Users, ld,notify,$state, $stateParams) {
	        $scope.delete = function() {
	            ld(Users.delete($stateParams).$promise.then(function (result) {
	                $state.go('^.^.list', {reload: true}).then(function () {
	                    notify.success('Deleted successfully');
	                });
	            }).catch($scope.errorHandler('Failed to delete user', null)), $scope);
	        };
	    }])
	    ;
	};

/***/ }),
/* 32 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    return module.factory('Users', ['$resource',function ($resource) {
	        return $resource('/users/:userId', { userId: '@userId' },{
	            query : {
	                isArray:false
	            }, 
	            update :{
	                method : 'PUT'
	            },
	            create : {
	                url : '/users/create',
	                method : 'GET'
	            },
	            bulkDelete : {
	                url : '/users/bulk/delete',
	                method : 'POST'
	            }
	        });
	    }]);
	};



/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var mod = angular.module('app.teams',[]);

	__webpack_require__(34)(mod);
	__webpack_require__(35)(mod);
	__webpack_require__(36)(mod);

/***/ }),
/* 34 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.config(['$stateProvider', function ($stateProvider) {
	        
	        $stateProvider.state('default.teams', {
	            url: '/teams',
	            template: '<ui-view></ui-view>',
	            abstract: true,
	            ncyBreadcrumb: {skip : true},
	            data: {
	                title: 'Teams'
	            }
	        })
	        .state('default.teams.list', {
	            url: '/list?options',
	            controller: 'default.teams',
	            templateUrl: 'modules/teams/views/index.html',
	            ncyBreadcrumb: {
	                parent: 'default.home',
	                label: 'Teams'
	            }
	        })
	        .state('default.teams.edit', {
	            url: '/:categoryId/edit',
	            controller: 'default.teams.edit',
	            templateUrl: 'modules/teams/views/edit.html',
	            abstract: true,
	            ncyBreadcrumb: {
	                parent:'default.teams.list',
	                label: '{{ item.name }}'
	            }
	        })
	        .state('default.teams.edit.details', {
	            url: '/details',
	            controller: 'default.teams.edit.details',
	            templateUrl: 'modules/teams/views/details.html',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.teams.list'
	            }
	        })
	        .state('default.teams.new', {
	            url: '/new',
	            controller: 'default.teams.edit.new',
	            templateUrl: 'modules/teams/views/details.html',
	            ncyBreadcrumb: {
	                parent:'default.teams.list',
	                label: 'New team'
	            }
	        })
	        .state('default.teams.edit.delete', {
	            url: '/delete',
	            templateUrl: 'modules/teams/views/delete.html',
	            controller: 'default.teams.edit.delete',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.teams.edit.details'
	            }
	        })
	        ;
	    }
	    ]);
	};

/***/ }),
/* 35 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    var moment = window.moment;

	    module.controller('default.teams', ['$scope', 'Teams', 'qs','notify','ld','$timeout','$window','checkboxes','$state','$stateParams', function ($scope, Teams, qs,notify,ld,$timeout,$window,checkboxes,$state,$stateParams) {
	        $scope.items = [];
	        $scope.meta = {};
	        $scope.pagination = {};
	        $scope.loading = true;

	        $scope.bulkDelete = function (items, $event) {
	            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
	                ld(Teams.bulkDelete({ items: items }).$promise.then(function (result) {
	                    checkboxes($scope.selected).reset();
	                    return $state.reload().then(function () {
	                        notify.success('Deleted successfully');
	                    });
	                }),$scope);
	            }
	        };

	        ld(Teams.query(angular.extend($stateParams, qs.getAll())).$promise.then(function (result) {
	            $scope.meta = result.meta;
	            $scope.items = result.items;
	            $scope.pagination = result.pagination;

	            $scope.loading = false;

	            $scope.filters = [
	                    /*
	                {
	                    txName: 'created_at',
	                    type: 'daterange',
	                    display: 'Created',
	                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
	                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
	                },
	                */
	                { txName: 'name', type: 'text', display: 'Name' }
	            ];
	        }),$scope);
	    }])
	    .controller('default.teams.edit', ['$scope', 'Teams', 'ld','notify','$stateParams', function ($scope, Teams, ld, notify,$stateParams) {
	        $scope.item = {};
	        $scope.meta = {};
	        $scope.loading = true;

	        $scope.sections = [
	            {
	                divider: true,
	                name: 'Main',
	                showWhenNew: true
	            },
	            {
	                divider: false,
	                name: 'Details',
	                icon: 'edit',
	                state: 'default.teams.edit.details',
	                showWhenNew: true
	            },
	            {
	                divider: true,
	                name: 'Options',
	                showWhenNew: false
	            },
	            {
	                divider: false,
	                name: 'Delete',

	                icon: 'delete',
	                state: 'default.teams.edit.delete',
	                showWhenNew: false
	            }
	        ];

	        ld(Teams.get($stateParams).$promise.then(function (result) {
	            $scope.item = result.item;
	            $scope.meta = result.meta;
	        }),$scope);
	    }])
	    .controller('default.teams.edit.new', ['$scope', 'Teams', 'ld','notify','$state','$stateParams', function ($scope, Teams, ld, notify,$state,$stateParams) {
	        $scope.item = {};
	        $scope.progress = 0;

	        ld(Teams.create().$promise.then(function (result) {
	            $scope.meta = result.meta;
	        }), $scope);

	        $scope.save = function(form){
	            $scope.$broadcast('show-errors-check-validity');
	            if (form.$valid) {
	                ld(Teams.save(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    $state.go('^.list', {}, {reload: true}).then(function () {
	                        notify.success('Saved successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to create team',form)), $scope);
	            }
	        };
	    }]).controller('default.teams.edit.delete', ['$scope', 'Teams', 'ld','notify','$stateParams','$state', function ($scope, Teams, ld, notify, $stateParams,$state) {
	        $scope.delete = function() {
	            ld(Teams.delete($stateParams).$promise.then(function (result) {
	                $state.go('^.^.list', {reload: true}).then(function () {
	                    notify.success('Deleted successfully');
	                });
	            }).catch($scope.errorHandler('Failed to delete team', null)), $scope);
	        };
	    }]).controller('default.teams.edit.details', ['$scope', 'Teams', 'ld','notify','$stateParams','$state', function ($scope, Teams, ld, notify, $stateParams, $state) {

	        $scope.save = function(form) {
	            $scope.$broadcast('show-errors-check-validity');

	            if (form.$valid) {
	                ld(Teams.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    return $state.reload().then(function () {
	                        $scope.item = result.item;
	                        notify.success('Save successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to update team', form)), $scope);
	            }
	        };
	    }])
	    ;
	};

/***/ }),
/* 36 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    return module.factory('Teams', ['$resource',function ($resource) {
	        return $resource('/api/teams/:teamId', { teamId: '@teamId' }, {
	            query : {
	                isArray:false
	            },
	            update :{
	                method : 'PUT'
	            },
	            create : {
	                url : '/api/teams/create',
	                method : 'GET'
	            },
	            bulkDelete : {
	                url : '/api/teams/bulk/delete',
	                method : 'POST'
	            }
	        });
	    }]);
	};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var mod = angular.module('app.powers',[]);

	__webpack_require__(38)(mod);
	__webpack_require__(39)(mod);
	__webpack_require__(40)(mod);

/***/ }),
/* 38 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.config(['$stateProvider', function ($stateProvider) {
	        
	        $stateProvider.state('default.powers', {
	            url: '/powers',
	            template: '<ui-view></ui-view>',
	            abstract: true,
	            ncyBreadcrumb: {skip : true},
	            data: {
	                title: 'Powers'
	            }
	        })
	        .state('default.powers.list', {
	            url: '/list?options',
	            controller: 'default.powers',
	            templateUrl: 'modules/powers/views/index.html',
	            ncyBreadcrumb: {
	                parent: 'default.home',
	                label: 'Powers'
	            }
	        })
	        .state('default.powers.edit', {
	            url: '/:powerId/edit',
	            controller: 'default.powers.edit',
	            templateUrl: 'modules/powers/views/edit.html',
	            abstract: true,
	            ncyBreadcrumb: {
	                parent:'default.powers.list',
	                label: '{{ item.name }}'
	            }
	        })
	        .state('default.powers.edit.details', {
	            url: '/details',
	            controller: 'default.powers.edit.details',
	            templateUrl: 'modules/powers/views/details.html',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.powers.list'
	            }
	        })
	        .state('default.powers.new', {
	            url: '/new',
	            controller: 'default.powers.edit.new',
	            templateUrl: 'modules/powers/views/details.html',
	            ncyBreadcrumb: {
	                parent:'default.powers.list',
	                label: 'New power'
	            }
	        })
	        .state('default.powers.edit.delete', {
	            url: '/delete',
	            templateUrl: 'modules/powers/views/delete.html',
	            controller: 'default.powers.edit.delete',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.powers.edit.details'
	            }
	        })
	        ;
	    }
	    ]);
	};

/***/ }),
/* 39 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    var moment = window.moment;

	    module.controller('default.powers', ['$scope', 'Powers', 'qs','notify','ld','$timeout','$window','checkboxes','$state','$stateParams', function ($scope, Powers, qs,notify,ld,$timeout,$window,checkboxes,$state,$stateParams) {
	        $scope.items = [];
	        $scope.meta = {};
	        $scope.pagination = {};
	        $scope.loading = true;

	        $scope.bulkDelete = function (items, $event) {
	            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
	                ld(Powers.bulkDelete({ items: items }).$promise.then(function (result) {
	                    checkboxes($scope.selected).reset();
	                    return $state.reload().then(function () {
	                        notify.success('Deleted successfully');
	                    });
	                }),$scope);
	            }
	        };

	        ld(Powers.query(angular.extend($stateParams, qs.getAll())).$promise.then(function (result) {
	            $scope.meta = result.meta;
	            $scope.items = result.items;
	            $scope.pagination = result.pagination;

	            $scope.loading = false;

	            $scope.filters = [
	                    /*
	                {
	                    txName: 'created_at',
	                    type: 'daterange',
	                    display: 'Created',
	                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
	                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
	                },
	                */
	                { txName: 'name', type: 'text', display: 'Name' }
	            ];
	        }),$scope);
	    }])
	    .controller('default.powers.edit', ['$scope', 'Powers', 'ld','notify','$stateParams', function ($scope, Powers, ld, notify,$stateParams) {
	        $scope.item = {};
	        $scope.meta = {};
	        $scope.loading = true;

	        $scope.sections = [
	            {
	                divider: true,
	                name: 'Main',
	                showWhenNew: true
	            },
	            {
	                divider: false,
	                name: 'Details',
	                icon: 'edit',
	                state: 'default.powers.edit.details',
	                showWhenNew: true
	            },
	            {
	                divider: true,
	                name: 'Options',
	                showWhenNew: false
	            },
	            {
	                divider: false,
	                name: 'Delete',

	                icon: 'delete',
	                state: 'default.powers.edit.delete',
	                showWhenNew: false
	            }
	        ];

	        ld(Powers.get($stateParams).$promise.then(function (result) {
	            $scope.item = result.item;
	            $scope.meta = result.meta;
	        }),$scope);
	    }])
	    .controller('default.powers.edit.new', ['$scope', 'Powers', 'ld','notify','$state','$stateParams', function ($scope, Powers, ld, notify,$state,$stateParams) {
	        $scope.item = {};
	        $scope.progress = 0;

	        ld(Powers.create().$promise.then(function (result) {
	            $scope.meta = result.meta;
	        }), $scope);

	        $scope.save = function(form){
	            $scope.$broadcast('show-errors-check-validity');
	            if (form.$valid) {
	                ld(Powers.save(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    $state.go('^.list', {}, {reload: true}).then(function () {
	                        notify.success('Saved successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to create power',form)), $scope);
	            }
	        };
	    }]).controller('default.powers.edit.delete', ['$scope', 'Powers', 'ld','notify','$stateParams','$state', function ($scope, Powers, ld, notify, $stateParams,$state) {
	        $scope.delete = function() {
	            ld(Powers.delete($stateParams).$promise.then(function (result) {
	                $state.go('^.^.list', {reload: true}).then(function () {
	                    notify.success('Deleted successfully');
	                });
	            }).catch($scope.errorHandler('Failed to delete power', null)), $scope);
	        };
	    }]).controller('default.powers.edit.details', ['$scope', 'Powers', 'ld','notify','$stateParams','$state', function ($scope, Powers, ld, notify, $stateParams, $state) {

	        $scope.save = function(form) {
	            $scope.$broadcast('show-errors-check-validity');

	            if (form.$valid) {
	                ld(Powers.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    return $state.reload().then(function () {
	                        $scope.item = result.item;
	                        notify.success('Save successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to update power', form)), $scope);
	            }
	        };
	    }])
	    ;
	};

/***/ }),
/* 40 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    return module.factory('Powers', ['$resource',function ($resource) {
	        return $resource('/api/powers/:powerId', { powerId: '@powerId' }, {
	            query : {
	                isArray:false
	            },
	            update :{
	                method : 'PUT'
	            },
	            create : {
	                url : '/api/powers/create',
	                method : 'GET'
	            },
	            bulkDelete : {
	                url : '/api/powers/bulk/delete',
	                method : 'POST'
	            }
	        });
	    }]);
	};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var mod = angular.module('app.superheros',[]);

	__webpack_require__(42)(mod);
	__webpack_require__(43)(mod);
	__webpack_require__(44)(mod);

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    module.config(['$stateProvider', function ($stateProvider) {
	        
	        $stateProvider.state('default.superheros', {
	            url: '/superheros',
	            template: '<ui-view></ui-view>',
	            abstract: true,
	            ncyBreadcrumb: {skip : true},
	            data: {
	                title: 'Superheros'
	            }
	        })
	        .state('default.superheros.list', {
	            url: '/list?options',
	            controller: 'default.superheros',
	            templateUrl: 'modules/superheros/views/index.html',
	            ncyBreadcrumb: {
	                parent: 'default.home',
	                label: 'Superheros'
	            }
	        })
	        .state('default.superheros.edit', {
	            url: '/:superheroId/edit',
	            controller: 'default.superheros.edit',
	            templateUrl: 'modules/superheros/views/edit.html',
	            abstract: true,
	            ncyBreadcrumb: {
	                parent:'default.superheros.list',
	                label: '{{ item.name }}'
	            }
	        })
	        .state('default.superheros.edit.metatags', {
	            url: '/metatags',
	            controller: 'default.superheros.edit.metatags',
	            templateUrl: 'modules/superheros/views/metatags.html',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.superheros.list'
	            }
	        })
	        .state('default.superheros.edit.details', {
	            url: '/details',
	            controller: 'default.superheros.edit.details',
	            templateUrl: 'modules/superheros/views/details.html',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.superheros.list'
	            }
	        })
	        .state('default.superheros.new', {
	            url: '/new',
	            controller: 'default.superheros.edit.new',
	            templateUrl: 'modules/superheros/views/details.html',
	            ncyBreadcrumb: {
	                parent:'default.superheros.list',
	                label: 'New superhero'
	            }
	        })
	        .state('default.superheros.edit.delete', {
	            url: '/delete',
	            templateUrl: 'modules/superheros/views/delete.html',
	            controller: 'default.superheros.edit.delete',
	            ncyBreadcrumb: {
	                label: '{{ item.name }}',
	                parent: 'default.superheros.edit.details'
	            }
	        })
	        ;
	    }
	    ]);
	};

/***/ }),
/* 43 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    var _ = window._;
	    var moment = window.moment;

	    module.controller('default.superheros', ['$scope', 'Superheros', 'qs','notify','ld','$timeout','$window','checkboxes','$state','$stateParams', function ($scope, Superheros, qs,notify,ld,$timeout,$window,checkboxes,$state,$stateParams) {
	        $scope.items = [];
	        $scope.meta = {};
	        $scope.pagination = {};
	        $scope.loading = true;

	        $scope.bulkDelete = function (items, $event) {
	            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
	                ld(Superheros.bulkDelete({ items: items }).$promise.then(function (result) {
	                    checkboxes($scope.selected).reset();
	                    return $state.reload().then(function () {
	                        notify.success('Deleted successfully');
	                    });
	                }),$scope);
	            }
	        };

	        ld(Superheros.query(angular.extend($stateParams, qs.getAll())).$promise.then(function (result) {
	            $scope.meta = result.meta;
	            $scope.items = result.items;
	            $scope.pagination = result.pagination;

	            $scope.loading = false;

	            $scope.filters = [
	                    /*
	                {
	                    txName: 'created_at',
	                    type: 'daterange',
	                    display: 'Created',
	                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
	                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
	                },
	                */
	                { txName: 'name', type: 'text', display: 'Name' }
	            ];
	        }),$scope);
	    }])
	    .controller('default.superheros.edit', ['$scope', 'Superheros', 'ld','notify','$stateParams', function ($scope, Superheros, ld, notify,$stateParams) {
	        $scope.item = {};
	        $scope.meta = {};
	        $scope.loading = true;

	        $scope.sections = [
	            {
	                divider: true,
	                name: 'Main',
	                showWhenNew: true
	            },
	            {
	                divider: false,
	                name: 'Details',
	                icon: 'edit',
	                state: 'default.superheros.edit.details',
	                showWhenNew: true
	            },
	            {
	                divider: true,
	                name: 'Options',
	                showWhenNew: false
	            },
	            {
	                divider: false,
	                name: 'Delete',

	                icon: 'delete',
	                state: 'default.superheros.edit.delete',
	                showWhenNew: false
	            }
	        ];

	        ld(Superheros.get($stateParams).$promise.then(function (result) {
	            $scope.item = result.item;
	            $scope.meta = result.meta;
	        }),$scope);
	    }])
	    .controller('default.superheros.edit.new', ['$scope', 'Superheros', 'ld','notify','$state','$stateParams', function ($scope, Superheros, ld, notify,$state,$stateParams) {
	        $scope.item = {};

	        $scope.loadTeams = function(query) {
	            var ids = _.map($scope.item.teams, function(item) { return item.id; });

	            return _.filter($scope.meta.collections.teams, function(item) { return !_.includes(ids, item.id); });
	        };

	        $scope.loadPowers = function(query) {
	            var ids = _.map($scope.item.powers, function(item) { return item.id; });

	            return _.filter($scope.meta.collections.powers, function(item) { return !_.includes(ids, item.id); });
	        };

	        ld(Superheros.create().$promise.then(function (result) {
	            $scope.meta = result.meta;
	        }), $scope);

	        $scope.save = function(form){
	            $scope.$broadcast('show-errors-check-validity');
	            if (form.$valid) {
	                ld(Superheros.save(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    $state.go('^.list', {}, {reload: true}).then(function () {
	                        notify.success('Saved successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to create superhero',form)), $scope);
	            }
	        };
	    }]).controller('default.superheros.edit.delete', ['$scope', 'Superheros', 'ld','notify','$stateParams','$state', function ($scope, Superheros, ld, notify, $stateParams,$state) {
	        $scope.delete = function() {
	            ld(Superheros.delete($stateParams).$promise.then(function (result) {
	                $state.go('^.^.list', {reload: true}).then(function () {
	                    notify.success('Deleted successfully');
	                });
	            }).catch($scope.errorHandler('Failed to delete superhero', null)), $scope);
	        };
	    }]).controller('default.superheros.edit.details', ['$scope', 'Superheros', 'ld','notify','$stateParams','$state', function ($scope, Superheros, ld, notify, $stateParams, $state) {
	        $scope.loadTeams = function(query) {
	            var ids = _.map($scope.item.teams, function(item) { return item.id; });

	            return _.filter($scope.meta.collections.teams, function(item) { return !_.includes(ids, item.id); });
	        };

	        $scope.loadPowers = function(query) {
	            /*
	            return $resource('/api/powers').query().$promise.then(function (result) {
	                console.log(result);
	            });
	            */
	            var ids = _.map($scope.item.powers, function(item) { return item.id; });

	            return _.filter($scope.meta.collections.powers, function(item) { return !_.includes(ids, item.id); });
	        };

	        $scope.save = function(form) {
	            $scope.$broadcast('show-errors-check-validity');

	            if (form.$valid) {
	                ld(Superheros.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
	                    return $state.reload().then(function () {
	                        $scope.item = result.item;
	                        notify.success('Save successfully');
	                    });
	                }).catch($scope.errorHandler('Failed to update superhero', form)), $scope);
	            }
	        };
	    }])
	    ;
	};

/***/ }),
/* 44 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function (module) {
	    return module.factory('Superheros', ['$resource',function ($resource) {
	        return $resource('/api/superheros/:superheroId', { superheroId: '@superheroId' }, {
	            query : {
	                isArray:false
	            }, 
	            update :{
	                method : 'PUT'
	            },
	            create : {
	                url : '/api/superheros/create',
	                method : 'GET'
	            },
	            bulkDelete : {
	                url : '/api/powers/bulk/delete',
	                method : 'POST'
	            }
	        });
	    }]);
	};



/***/ })
]);