'use strict';

module.exports = function (module) {
    return module.factory('Superheros', ['$resource',function ($resource) {
        return $resource('/api/superheros/:superheroId', { superheroId: '@superheroId' }, {
            query : {
                isArray:false
            }, 
            update :{
                method : 'PUT'
            },
            create : {
                url : '/api/superheros/create',
                method : 'GET'
            },
            bulkDelete : {
                url : '/api/powers/bulk/delete',
                method : 'POST'
            }
        });
    }]);
};

