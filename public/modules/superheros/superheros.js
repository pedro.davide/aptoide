'use strict';

var mod = angular.module('app.superheros',[]);

require('moduleDir/superheros/configs/routes')(mod);
require('moduleDir/superheros/controllers/superheros')(mod);
require('moduleDir/superheros/services/superheros')(mod);