'use strict';

module.exports = function (module) {
    module.config(['$stateProvider', function ($stateProvider) {
        
        $stateProvider.state('default.superheros', {
            url: '/superheros',
            template: '<ui-view></ui-view>',
            abstract: true,
            ncyBreadcrumb: {skip : true},
            data: {
                title: 'Superheros'
            }
        })
        .state('default.superheros.list', {
            url: '/list?options',
            controller: 'default.superheros',
            templateUrl: 'modules/superheros/views/index.html',
            ncyBreadcrumb: {
                parent: 'default.home',
                label: 'Superheros'
            }
        })
        .state('default.superheros.edit', {
            url: '/:superheroId/edit',
            controller: 'default.superheros.edit',
            templateUrl: 'modules/superheros/views/edit.html',
            abstract: true,
            ncyBreadcrumb: {
                parent:'default.superheros.list',
                label: '{{ item.name }}'
            }
        })
        .state('default.superheros.edit.metatags', {
            url: '/metatags',
            controller: 'default.superheros.edit.metatags',
            templateUrl: 'modules/superheros/views/metatags.html',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.superheros.list'
            }
        })
        .state('default.superheros.edit.details', {
            url: '/details',
            controller: 'default.superheros.edit.details',
            templateUrl: 'modules/superheros/views/details.html',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.superheros.list'
            }
        })
        .state('default.superheros.new', {
            url: '/new',
            controller: 'default.superheros.edit.new',
            templateUrl: 'modules/superheros/views/details.html',
            ncyBreadcrumb: {
                parent:'default.superheros.list',
                label: 'New superhero'
            }
        })
        .state('default.superheros.edit.delete', {
            url: '/delete',
            templateUrl: 'modules/superheros/views/delete.html',
            controller: 'default.superheros.edit.delete',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.superheros.edit.details'
            }
        })
        ;
    }
    ]);
};