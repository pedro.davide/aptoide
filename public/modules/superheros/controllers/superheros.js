'use strict';

module.exports = function (module) {
    var _ = window._;
    var moment = window.moment;

    module.controller('default.superheros', ['$scope', 'Superheros', 'qs','notify','ld','$timeout','$window','checkboxes','$state','$stateParams', function ($scope, Superheros, qs,notify,ld,$timeout,$window,checkboxes,$state,$stateParams) {
        $scope.items = [];
        $scope.meta = {};
        $scope.pagination = {};
        $scope.loading = true;

        $scope.bulkDelete = function (items, $event) {
            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
                ld(Superheros.bulkDelete({ items: items }).$promise.then(function (result) {
                    checkboxes($scope.selected).reset();
                    return $state.reload().then(function () {
                        notify.success('Deleted successfully');
                    });
                }),$scope);
            }
        };

        ld(Superheros.query(angular.extend($stateParams, qs.getAll())).$promise.then(function (result) {
            $scope.meta = result.meta;
            $scope.items = result.items;
            $scope.pagination = result.pagination;

            $scope.loading = false;

            $scope.filters = [
                    /*
                {
                    txName: 'created_at',
                    type: 'daterange',
                    display: 'Created',
                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
                },
                */
                { txName: 'name', type: 'text', display: 'Name' }
            ];
        }),$scope);
    }])
    .controller('default.superheros.edit', ['$scope', 'Superheros', 'ld','notify','$stateParams', function ($scope, Superheros, ld, notify,$stateParams) {
        $scope.item = {};
        $scope.meta = {};
        $scope.loading = true;

        $scope.sections = [
            {
                divider: true,
                name: 'Main',
                showWhenNew: true
            },
            {
                divider: false,
                name: 'Details',
                icon: 'edit',
                state: 'default.superheros.edit.details',
                showWhenNew: true
            },
            {
                divider: true,
                name: 'Options',
                showWhenNew: false
            },
            {
                divider: false,
                name: 'Delete',

                icon: 'delete',
                state: 'default.superheros.edit.delete',
                showWhenNew: false
            }
        ];

        ld(Superheros.get($stateParams).$promise.then(function (result) {
            $scope.item = result.item;
            $scope.meta = result.meta;
        }),$scope);
    }])
    .controller('default.superheros.edit.new', ['$scope', 'Superheros', 'ld','notify','$state','$stateParams', function ($scope, Superheros, ld, notify,$state,$stateParams) {
        $scope.item = {};

        $scope.loadTeams = function(query) {
            var ids = _.map($scope.item.teams, function(item) { return item.id; });

            return _.filter($scope.meta.collections.teams, function(item) { return !_.includes(ids, item.id); });
        };

        $scope.loadPowers = function(query) {
            var ids = _.map($scope.item.powers, function(item) { return item.id; });

            return _.filter($scope.meta.collections.powers, function(item) { return !_.includes(ids, item.id); });
        };

        ld(Superheros.create().$promise.then(function (result) {
            $scope.meta = result.meta;
        }), $scope);

        $scope.save = function(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid) {
                ld(Superheros.save(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
                    $state.go('^.list', {}, {reload: true}).then(function () {
                        notify.success('Saved successfully');
                    });
                }).catch($scope.errorHandler('Failed to create superhero',form)), $scope);
            }
        };
    }]).controller('default.superheros.edit.delete', ['$scope', 'Superheros', 'ld','notify','$stateParams','$state', function ($scope, Superheros, ld, notify, $stateParams,$state) {
        $scope.delete = function() {
            ld(Superheros.delete($stateParams).$promise.then(function (result) {
                $state.go('^.^.list', {reload: true}).then(function () {
                    notify.success('Deleted successfully');
                });
            }).catch($scope.errorHandler('Failed to delete superhero', null)), $scope);
        };
    }]).controller('default.superheros.edit.details', ['$scope', 'Superheros', 'ld','notify','$stateParams','$state', function ($scope, Superheros, ld, notify, $stateParams, $state) {
        $scope.loadTeams = function(query) {
            var ids = _.map($scope.item.teams, function(item) { return item.id; });

            return _.filter($scope.meta.collections.teams, function(item) { return !_.includes(ids, item.id); });
        };

        $scope.loadPowers = function(query) {
            /*
            return $resource('/api/powers').query().$promise.then(function (result) {
                console.log(result);
            });
            */
            var ids = _.map($scope.item.powers, function(item) { return item.id; });

            return _.filter($scope.meta.collections.powers, function(item) { return !_.includes(ids, item.id); });
        };

        $scope.save = function(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (form.$valid) {
                ld(Superheros.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
                    return $state.reload().then(function () {
                        $scope.item = result.item;
                        notify.success('Save successfully');
                    });
                }).catch($scope.errorHandler('Failed to update superhero', form)), $scope);
            }
        };
    }])
    ;
};