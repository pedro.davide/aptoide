'use strict';

module.exports = function (module) {
    return module.factory('Users', ['$resource',function ($resource) {
        return $resource('/users/:userId', { userId: '@userId' },{
            query : {
                isArray:false
            }, 
            update :{
                method : 'PUT'
            },
            create : {
                url : '/users/create',
                method : 'GET'
            },
            bulkDelete : {
                url : '/users/bulk/delete',
                method : 'POST'
            }
        });
    }]);
};

