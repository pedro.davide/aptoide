'use strict';

var mod = angular.module('app.users',[]);

require('moduleDir/users/configs/routes')(mod);
require('moduleDir/users/controllers/users')(mod);
require('moduleDir/users/services/users')(mod);