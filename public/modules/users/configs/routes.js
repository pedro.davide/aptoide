'use strict';

module.exports = function (module) {
  module.config(['$stateProvider',function ($stateProvider) {

      $stateProvider.state('default.users', {
          url: '/users',
          template: '<ui-view></ui-view>',
          abstract: true,
          ncyBreadcrumb: {skip : true},
          data: {
              title: 'Users'
          }
      })
      .state('default.users.list', {
          url: '/list?options',
          controller: 'default.users',
          templateUrl: 'modules/users/views/index.html',
          ncyBreadcrumb: {
              parent:'default.home',
              label: 'Users'
          }
      })
      .state('default.users.edit', {
          url: '/:userId/edit',
          controller: 'users.edit',
          templateUrl: 'modules/users/views/edit.html',
          abstract: true,
          ncyBreadcrumb: {
              parent:'default.users.list',
              label: '{{ item.name }}'
          }
      })
      .state('default.users.edit.details', {
          url: '/details',
          controller: 'users.edit.details',
          templateUrl: 'modules/users/views/details.html',
          ncyBreadcrumb: {
              label: '{{ item.name }}',
              parent: 'default.users.edit'
          }
      })
      .state('default.users.edit.roles', {
          url: '/roles',
          controller: 'users.edit.roles',
          templateUrl: 'modules/users/views/roles.html',
          ncyBreadcrumb: {
              label: '{{ item.name }}',
              parent: 'default.users.edit'
          }
      })
      .state('default.users.new', {
          url: '/new',
          controller: 'users.edit.new',
          templateUrl: 'modules/users/views/details.html',
          ncyBreadcrumb: {
              parent:'default.users.list',
              label: 'New user'
          }
      })
      .state('default.users.edit.delete', {
          url: '/delete',
          templateUrl: 'modules/users/views/delete.html',
          controller: 'users.edit.delete',
          ncyBreadcrumb: {
              label: '{{ item.name }}',
              parent: 'default.users.edit'
          }
      })
      .state('default.users.edit.impersonate', {
          controller: 'users.impersonate',
          url: '/impersonate/:userId',
          ncyBreadcrumb: {
              skip: true
          }
      })
      .state('default.users.edit.unimpersonate', {
          controller: 'users.unimpersonate',
          url: '/unimpersonate',
          ncyBreadcrumb: {
              skip: true
          }
      })
      ;
    }
  ]);
};