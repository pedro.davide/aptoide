'use strict';

module.exports = function (module) {
    var _ = window._;
    var moment = window.moment;

    module.controller('default.users', ['$scope', 'Users', 'qs','ld','pagination','$timeout','$window','checkboxes','$state','notify', function ($scope, Users, qs,ld, pagination, $timeout,$window,checkboxes,$state,notify) {
        $scope.items = [];
        $scope.meta = {};
        $scope.pagination = {};
        $scope.loading = true;

        $scope.bulkDelete = function (items, $event) {
            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
                ld(Users.bulkDelete({ items: items }).$promise.then(function (result) {
                    checkboxes($scope.selected).reset();
                    return $state.reload().then(function () {
                        notify.success('Deleted successfully');
                    });
                }),$scope);
            }
        };

        ld(Users.query(qs.getAll()).$promise.then(function (result) {
            $scope.meta = result.meta;
            $scope.items = result.items;
            $scope.pagination = result.pagination;

            $scope.loading = false;

            $scope.filters = [
                {
                    txName: 'created_at',
                    type: 'daterange',
                    display: 'Created',
                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
                },
                { txName: 'name', type: 'text', display: 'Name' },
                { txName: 'email', type: 'text', display: 'E-mail' },
                { txName: 'gender', type: 'select', display: 'Gender', payload: [{id: 'male', txName: 'Male'}, {id: 'female', txName: 'Female'}] }
            ];
        }),$scope);
    }])
    .controller('users.edit', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','Upload','notify','$state','USER', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams,Upload,notify,$state, USER) {
        $scope.item = {};
        $scope.meta = {};
        $scope.loading = true;
        $scope.progress = 0;
        $scope.user = USER;

        $scope.sections = [
            {
                divider: true,
                name: 'Main',
                showWhenNew: true
            },
            {
                divider: false,
                name: 'Details',
                icon: 'edit',
                state: 'default.users.edit.details',
                showWhenNew: true,
                resource: 'USERS',
                action: 'UPDATE'
            },
            {
                divider: false,
                name: 'Roles',
                icon: 'lock_open',
                state: 'default.users.edit.roles',
                showToAdmin: true,
                resource: 'USERS',
                action: 'UPDATE'
            },
            {
                divider: true,
                name: 'Options',
                showWhenNew: false
            },
            {
                divider: false,
                name: 'Impersonate',
                icon: 'accessibility',
                state: 'default.users.edit.impersonate',
                showWhenNew: false,
                resource: 'USERS',
                action: 'IMPERSONATE'
            },
            {
                divider: false,
                name: 'Delete',
                icon: 'delete',
                state: 'default.users.edit.delete',
                showWhenNew: false,
                resource: 'USERS',
                action: 'DELETE'
            }
        ];

        ld(Users.get({ userId: $stateParams.userId }).$promise.then(function (result) {
            $scope.item = result.item;
            $scope.item.newsletter = $scope.item.newsletter ? true : false;

            $scope.meta = result.meta;

            $scope.$watch('avatar', function (newValue, oldValue) {
                $scope.upload($scope.avatar);
            });

            $scope.upload = function (file) {
                if (file) {
                    $scope.uploading = true;
                    $scope.uploaded = false;

                    Upload.upload({
                        url: '/users/' + $scope.item.id + '/upload',
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.progress = progressPercentage;

                    }).success(function (data, status, headers, config) {
                        $scope.progress = 0;
                        $scope.uploading = false;
                        $scope.uploaded = true;

                        if (data.status === 'OK') {
                            $scope.error = false;

                            $scope.item.avatar = data.path;

                            return $state.reload().then(function () {
                                notify.success('Save successfully');
                            });
                        }
                        else {
                            $scope.error = data.error;
                            notify.error('Try a different image');
                        }
                    }).error(function (data, status, headers, config) {
                        notify.error('General error. Try a different image');
                    });
                }
            };
        }),$scope);
    }])
    .controller('users.edit.details', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','$state','notify','Upload', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams, $state, notify, Upload) {
        $scope.newItem = false;

        $scope.save = function(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (form.$valid) {
                ld(Users.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
                    return $state.reload().then(function () {
                        $scope.item = result.item;
                        notify.success('Save successfully');
                    });
                }).catch($scope.errorHandler('Failed to update user', form)), $scope);
            }
        };
    }])
    .controller('users.edit.roles', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','$state','notify','USER', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams, $state, notify, USER) {
        $scope.items = [];
        $scope.user = USER;
        $scope.meta = {};

        ld(Users.get_roles({ userId: $stateParams.userId }).$promise.then(function (result) {
            $scope.items = _.map(result.items, function(item) { item.active = item.active == 1; return item; });
            $scope.meta = result.meta;

        }), $scope);

        $scope.save = function(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (form.$valid) {
                ld(Users.save_roles(angular.extend({ userId: $stateParams.userId }, { items: $scope.items })).$promise.then(function (result) {
                    return $state.reload().then(function () {
                        notify.success('Save successfully');
                    });

                }).catch($scope.errorHandler('Failed to update roles', form)), $scope);
            }
        };
    }])
    .controller('users.edit.new', ['$scope', 'Users', 'qs', 'ld', 'pagination','$timeout', '$stateParams','notify','$state', function ($scope, Users, qs, ld, pagination, $timeout, $stateParams,notify,$state) {
        $scope.item = {};
        $scope.meta = {};
        $scope.newItem = true;

        ld(Users.create().$promise.then(function (result) {
            $scope.meta = result.meta;
        }), $scope);

        $scope.save = function(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid) {
                ld(Users.save($scope.item).$promise.then(function (result) {
                    $state.go('.list', {}, {reload: true}).then(function () {
                        notify.success('Saved successfully');
                    });
                }).catch($scope.errorHandler('Failed to create user',form)), $scope);
            }
        };
    }])
    .controller('users.impersonate', ['$scope', 'Users', 'ld','notify','$stateParams','$state','$window', function ($scope, Users, ld,notify, $stateParams, $state, $window) {
        ld(Users.impersonate({ userId: $statePar^ams.userId }).$promise.then(function(result){
            notify.success('Impersonated successfully');
            $window.location.href = '/';
        }).catch(function (result) {
            notify.error('Failed to impersonate user');
        }),$scope);
    }])
    .controller('users.unimpersonate', ['$scope', 'Users', 'ld','notify','$state', function ($scope, Users, ld,notify,$state) {
        ld(Users.unimpersonate().$promise.then(function(result){
            return $state.go('default.home',{},{reload: true}).then(function () {
                notify.success('Switched back successfully');
            });
        }).catch(function (result) {
            notify.error('Failed to switch back');
        }),$scope);
    }])
    .controller('users.edit.delete', ['$scope', 'Users', 'ld','notify','$state','$stateParams', function ($scope, Users, ld,notify,$state, $stateParams) {
        $scope.delete = function() {
            ld(Users.delete($stateParams).$promise.then(function (result) {
                $state.go('^.^.list', {reload: true}).then(function () {
                    notify.success('Deleted successfully');
                });
            }).catch($scope.errorHandler('Failed to delete user', null)), $scope);
        };
    }])
    ;
};