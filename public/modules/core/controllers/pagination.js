'use strict';

/**
 * Core setting controller
 * @module: app.core
 */
module.exports = function (module) {
    module.controller('PaginationController', ['$scope','qs', function ($scope,qs) {
        $scope.pageChanged = function(page){
            qs.setValue('page', page);
        };

        $scope.setPerpage = function(ppage){
            qs.setValues({
                limit : ppage,
                page: 1
            });
        };
    }]);
};