'use strict';

/**
 * Core setting controller
 * @module: app.core
 */
module.exports = function(module) {
    module.controller('coreSettingsCtrl', ['$scope', '$rootScope','$window','USER', function($scope, $rootScope, $window, USER) {
        $scope.user = USER;

        $scope.isIE = function() {
            return !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./);
        };

        $scope.isSmart = function() {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window.navigator.userAgent || $window.navigator.vendor || $window.opera;
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        };

        $scope.app = {
            name: 'Flatkit',
            version: '1.1.3',
            // for chart colors
            color: {
                'primary':      '#0cc2aa',
                'accent':       '#a88add',
                'warn':         '#fcc100',
                'info':         '#6887ff',
                'success':      '#6cc788',
                'warning':      '#f77a99',
                'danger':       '#f44455',
                'white':        '#ffffff',
                'light':        '#f1f2f3',
                'dark':         '#2e3e4e',
                'black':        '#2a2b3c'
            },
            setting: {
                theme: {
                    primary: 'primary',
                    accent: 'accent',
                    warn: 'warn'
                },
                folded: false,
                boxed: false,
                container: false,
                themeID: 1,
                bg: ''
            },
            feedback : {
                html2canvasURL : 'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js',
                ajaxURL : 'feedback',
                postHTML : false
            }
        };

        $scope.sections = [
            {
                divider: true,
                name: 'Main'
            },/*
            {
                divider: false,
                name: 'Dashboard',
                icon: 'dashboard',
                state: 'default.home',
                route: 'default.home'
            },*/
            {
                divider: false,
                name: 'Superheros',
                icon: 'android',
                state: 'default.superheros.list',
                route: 'default.superheros'
            },
            {
                divider: false,
                name: 'Teams',
                icon: 'assignment_ind',
                state: 'default.teams.list',
                route: 'default.teams'
            },
            {
                divider: false,
                name: 'Powers',
                icon: 'flash_on',
                state: 'default.powers.list',
                route: 'default.powers'
            },
            {
                divider: true,
                name: 'Manage'
            },
            {
                divider: false,
                name: 'Users',
                icon: 'people',
                state: 'default.users.list',
                route: 'default.users'
            }
        ];
    }]);
};
