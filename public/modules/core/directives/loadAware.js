'use strict';
module.exports = function (module) {
    module.directive('loadAware', ['$timeout','qs','$window',function ($timeout,qs,$window) {
        return {
            restrict: 'E',
            transclude : true,
            templateUrl: 'modules/core/views/partials/loadAware.html'
        };
    }]);
};