'use strict';
module.exports = function (module) {
    var _ = window._;

    module.directive('permission', ['$timeout','$rootScope','USER', function ($timeout,$rootScope,USER) {
        return {
            scope: {
                resource: '@',
                action: '@',
                section: '='
            },
            replace: false,
            restrict: 'EA',
            link: function($scope, element, attrs) {
                if ((!$scope.section) || (!$scope.section.divider)) {
                    if (USER.is_admin == '1') {
                        element.show();
                        return false;
                    }
                    else {
                        var permission = _.find(USER.permissions, { action: $scope.action, resource: $scope.resource });

                        if ((permission) && (_.find(USER.sites, { site_id: permission.site_id, role_id: permission.role_id }))) {
                            element.show();
                        }
                        else {
                            element.remove();
                        }
                    }
                }
            }
        };
    }]);
};