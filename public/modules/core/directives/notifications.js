'use strict';
module.exports = function (module) {
    module.directive('notificationsBar', ['$timeout','$rootScope','notify',function ($timeout,$rootScope,notify) {
        return {
            restrict: 'EA',
            templateUrl: '/modules/core/views/partials/notifications.html',
            link: function(scope) {
                scope.hasNotification = function(){
                    return notify.has();
                };

                scope.message = function(){
                    return notify.notification();
                };

                scope.close = function (index) {
                    notify.close();
                };
            }
        };
    }]);
};