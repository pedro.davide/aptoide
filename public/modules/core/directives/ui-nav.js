'use strict';
module.exports = function (module) {
    module.directive('uiNav', ['$timeout', function ($timeout) {
        return {
            scope: {
                data: '='
            },
            restrict: 'AC',
            link: function(scope, el, attr) {
                scope.$watch('scope.data', function() {
                    $timeout(function() {
                        el.find('a').bind('click', function(e) {
                            var li = angular.element(this).parent();
                            var active = li.parent()[0].querySelectorAll('.active');
                            li.toggleClass('active');
                            angular.element(active).removeClass('active');
                        });
                    }, 1);
                });
            }
        };
    }]);
};