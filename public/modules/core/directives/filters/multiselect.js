'use strict';

module.exports = function (module) {
    module.directive('filterMultiSelect', ['$timeout', 'qs', '$window', '$rootScope', function ($timeout, qs, $window, $rootScope) {
        var _ = window._;
        return {
            restrict: 'A',
            scope: {
                filterMultiSelect: '='
            },
            templateUrl: '/modules/core/views/filters/multiselect.html',
            controller: function ($scope) {


            },
            link: function ($scope, iElm, iAttrs) {
                var input = iElm.find('input');
                var prevHandler;
                $scope.max = 19;
                $scope.active = -1;
                $scope.query = null;
                $scope.currentActive = null;
                $scope.startIdx = 0;
                $scope.filterMultiSelect.value = [];
                $scope.original = _.cloneDeep($scope.filterMultiSelect.payload);

                $scope.items = _.sortBy($scope.filterMultiSelect.payload, function (el) {
                    return el.name.toLowerCase();
                });

                $scope.endIdx = $scope.filterMultiSelect.payload.length;

                var sync = function () {
                    $timeout(function () {
                        $scope.items = _.sortBy($scope.filterMultiSelect.payload, function (el) {
                            return el.name.toLowerCase();
                        });
                        $scope.$apply();
                        $scope.$broadcast('vsRepeatTrigger');
                    });
                };

                var add = function (item) {
                    $scope.filterMultiSelect.value.push(item);
                    _.remove($scope.filterMultiSelect.payload, function (el) {
                        return el.id === item.id;
                    });
                    $scope.query = '';
                    sync();
                };

                var del = function (item) {
                    _.remove($scope.filterMultiSelect.value, item, 'id');
                    $scope.filterMultiSelect.payload.push(item);
                    sync();

                };

                $scope.filterMultiSelect.reset = function () {
                    $scope.filterMultiSelect.payload = $scope.original;
                    $scope.filterMultiSelect.value = [];
                    sync();
                };


                $scope.$on('closeDD', function (event) {
                    $scope.close();
                });
                $scope.$watch('query', _.debounce(function (newval, oldval) {
                    if (newval !== oldval) {
                        console.log('QUERY', newval, $scope.filterMultiSelect.editing);
                        $scope.$apply(function () {
                            $scope.items = _.filter($scope.filterMultiSelect.payload, function (item) {
                                if (!newval) {
                                    return true;
                                }
                                return item.name.toLowerCase().indexOf(newval.toLowerCase()) > -1;
                            });
                            $scope.endIdx = $scope.items.length;
                            $scope.active = -1;
                            console.log('QUERY2', $scope.items, $scope.filterMultiSelect.editing);
                        });
                    }
                }, 200));
                $scope.$watch('filterMultiSelect', function (newval, oldval) {
                    if (newval) {
                        $scope.filterMultiSelect.focus = function () {
                            $timeout(function () {
                                input[0].focus();
                            });
                        };
                        var val = _.filter($scope.filterMultiSelect.payload, function (item) {
                            var qsvalue = qs.getValue($scope.filterMultiSelect.txName);
                            return qsvalue && _.includes(qsvalue.split(','), '' + item.id);
                        });
                        if (val) {
                            $scope.filterMultiSelect.value = val;
                        }

                        if ($scope.filterMultiSelect.editing) {
                            $scope.filterMultiSelect.focus();
                        }
                        // set default if no value defined
                        if (!$scope.filterMultiSelect.value.length && !qs.getValue($scope.filterMultiSelect.txName)) {
                            $scope.filterMultiSelect.value = _.filter($scope.filterMultiSelect.payload, function (item) {
                                var qsvalue = $scope.filterMultiSelect.defaultValue;
                                return qsvalue && _.includes(qsvalue.split(','), '' + item.id);
                            });
                        }
                        _.remove($scope.filterMultiSelect.payload, function (item) {
                            return _.includes($scope.filterMultiSelect.value, item);
                        });
                        sync();
                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                });
                var KeyCodes = {
                    BACKSPACE: 8,
                    TABKEY: 9,
                    RETURNKEY: 13,
                    ESCAPE: 27,
                    SPACEBAR: 32,
                    LEFTARROW: 37,
                    UPARROW: 38,
                    RIGHTARROW: 39,
                    DOWNARROW: 40
                };

                $scope.close = function () {
                    $scope.filterMultiSelect.editing = false;
                    $scope.query = '';

                    if ($scope.filterMultiSelect.value.length === 0) {
                        $scope.$parent.remove($scope.filterMultiSelect);
                    }
                };


                $scope.isSelected = function (filter) {
                    return _.includes($scope.filterMultiSelect.value, filter);
                };
                $scope.removeSelected = function (filter) {
                    del(filter);
                };
                $scope.edit = function (val, e) {
                    if (val) {
                        $scope.filterMultiSelect.focus();
                    }
                    $scope.filterMultiSelect.editing = val;
                    $scope.$broadcast('vsRepeatTrigger', {scrollIndex: 0, scrollIndexPosition: 'inview#auto'});
                    e.stopPropagation();
                };

                $scope.select = function (filter, e) {
                    e.stopPropagation();
                    add(filter);
                };

                $scope.setActive = function (item) {
                    if (item) {
                        $scope.currentActive = item;
                    }
                };


                $scope.activateNext = function () {
                    if ($scope.endIdx > $scope.active + 1) {
                        ++$scope.active;
                    }
                    $scope.$broadcast('vsRepeatTrigger', {
                        scrollIndex: $scope.active,
                        scrollIndexPosition: 'inview#auto'
                    });
                };

                $scope.activatePrev = function () {
                    if ($scope.active > 0) {
                        --$scope.active;
                    } else {
                        $scope.active = -1;
                    }
                    $scope.$broadcast('vsRepeatTrigger', {
                        scrollIndex: $scope.active - 1,
                        scrollIndexPosition: 'inview#top'
                    });
                };

                $scope.isActive = function (idx) {
                    return idx === $scope.active ? 'active' : '';
                };

                $scope.keydown = function (e) {
                    var handledKeys = [KeyCodes.TABKEY, KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
                    if (handledKeys.indexOf(e.which) === -1) {
                        return;
                    }
                    if (e.which === KeyCodes.RETURNKEY) {
                        e.preventDefault();
                        if ($scope.active >= 0) {
                            add($scope.items[$scope.active]);
                        }
                    }
                    if (e.which === KeyCodes.TABKEY && $scope.filterMultiSelect.value) {
                        e.preventDefault();
                        if ($scope.active >= 0) {
                            add($scope.filterMultiSelect.payload[$scope.active]);
                        }
                        $scope.close();
                        $scope.$parent.editNext($scope.filterMultiSelect);
                    }
                    if (e.which === KeyCodes.DOWNARROW) {
                        e.preventDefault();
                        $scope.activateNext();
                    }
                    if (e.which === KeyCodes.UPARROW) {
                        e.preventDefault();
                        $scope.activatePrev();
                    }
                };
            }
        };
    }]);
};