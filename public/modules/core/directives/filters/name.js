'use strict';

module.exports = function (module) {
    module.directive('filterName', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
        return {
            restrict: 'A',
            replace: true,
            transcluded: true,
            scope: {
                filterName: '='
            },
            templateUrl: '/modules/core/views/filters/name.html',
            link: function ($scope, iElm, iAttrs) {
                var KeyCodes = {
                    BACKSPACE: 8,
                    TABKEY: 9,
                    RETURNKEY: 13,
                    ESCAPE: 27,
                    SPACEBAR: 32,
                    LEFTARROW: 37,
                    UPARROW: 38,
                    RIGHTARROW: 39,
                    DOWNARROW: 40
                };
                $scope.$watch('filterName', function (newval, oldval) {
                    if (newval) {
                        $scope.filterName.value = qs.getValue(newval.txName);
                    }
                });


                $scope.edit = function (val, e) {
                    e.stopPropagation();
                    $scope.filterName.editing = val;

                    if (!val && !$scope.filterName.value) {
                        $scope.$parent.remove($scope.filterName);
                    }
                };

                $scope.keydown = function (e) {
                    var handledKeys = [KeyCodes.BACKSPACE, KeyCodes.TABKEY, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
                    if (handledKeys.indexOf(e.which) === -1) {
                        return;
                    }
                    if (e.which === KeyCodes.BACKSPACE && !$scope.filterName.value) {
                        e.preventDefault();
                        $scope.$parent.editPrevious($scope.filterName);
                        $scope.$parent.remove($scope.filterName);
                    }
                    if (e.which === KeyCodes.TABKEY) {
                        console.log('EDIT NEXT');
                        $scope.filterName.editing = false;
                        $scope.$parent.editNext($scope.filterName);
                    }
                    if (e.which === KeyCodes.RETURNKEY) {
                        $scope.$parent.apply();
                    }
                    console.log('Pressed', e.which);
                };
            }
        };
    }]);
};