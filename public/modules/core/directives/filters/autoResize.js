'use strict';

module.exports = function (module) {
    module.directive('filterAutoResize', [
            function () {
                return {
                    restrict: 'A',
                    scope: {
                        model: '=ngModel'
                    },
                    link: function ($scope, $element, $attrs) {
                        var container = angular.element('<div style="position: fixed; top: -9999px; left: 0px;"></div>');
                        var shadow = angular.element('<span style="white-space:pre;"></span>');

                        var maxWidth = $element.css('maxWidth') === 'none' ? $element.parent().innerWidth() : $element.css('maxWidth');
                        $element.css('maxWidth', maxWidth);

                        angular.forEach([
                            'fontSize', 'fontFamily', 'fontWeight', 'fontStyle',
                            'letterSpacing', 'textTransform', 'wordSpacing', 'textIndent',
                            'boxSizing', 'borderLeftWidth', 'borderRightWidth', 'borderLeftStyle', 'borderRightStyle',
                            'paddingLeft', 'paddingRight', 'marginLeft', 'marginRight'
                        ], function (css) {
                            shadow.css(css, $element.css(css));
                        });

                        angular.element('body').append(container.append(shadow));

                        function resize() {
                            shadow.text($element.val() || $element.attr('placeholder'));
                            $element.css('width', shadow.outerWidth() + ($element.val().length === 0 ? 75 : 0));
                        }

                        resize();

                        if ($scope.model) {
                            $scope.$watch('model', function () {
                                resize();
                            });
                        } else {
                            $element.on('keypress keyup keydown focus input propertychange change', function () {
                                resize();
                            });
                        }
                    }
                };
            }]
    );
};