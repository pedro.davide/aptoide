'use strict';

module.exports = function (module) {
    module.directive('filtersBar', ['$timeout', 'qs', '$rootScope', '$window', function ($timeout, qs, $rootScope, $window) {
        var _ = window._;
        return {
            restrict: 'EA',
            scope: {
                availableFilters: '='
            },
            templateUrl: '/modules/core/views/filters/layout.html',
            link: function ($scope, iElm, iAttrs) {
                $scope.filters = qs.getAll();
                $scope.editing = false;
                $scope.active = 0;
                $scope.query = '';

                var KeyCodes = {
                    BACKSPACE: 8,
                    TABKEY: 9,
                    RETURNKEY: 13,
                    ESCAPE: 27,
                    SPACEBAR: 32,
                    LEFTARROW: 37,
                    UPARROW: 38,
                    RIGHTARROW: 39,
                    DOWNARROW: 40
                };
                $window.onclick = function (event) {
                    $rootScope.$broadcast('closeDD', event);
                };

                $scope.$on('closeDD', function (event) {
                    $scope.close();
                });

                $scope.$watch('availableFilters', function (newVal, oldVal) {
                    if (newVal) {
                        $timeout(function () {
                            $scope.used = _.filter($scope.availableFilters, function (item) {
                                if (item.defaultValue !== undefined) {
                                    return true;
                                }
                                return _.some($scope.filters, function (i, key) {
                                    if (item.type === 'select') {
                                        return item.txName === key;
                                    }
                                    if (item.type === 'multiselect') {
                                        console.log(item.txName, key);
                                        return item.txName === key;
                                    }
                                    if (item.type === 'text') {
                                        return item.txName === key;
                                    }
                                    if (item.type === 'daterange') {
                                        return (item.txName + '_start' === key || item.txName + '_end' === key);
                                    }
                                    if (item.type === 'calendardate') {
                                        return (item.txName + '_start' === key || item.txName + '_end' === key || item.txName + '_view' === key || item.txName + '_currentDay' === key);
                                    }
                                });
                            });

                            $scope.available = _.filter($scope.availableFilters, function (item) {
                                return !_.some($scope.used, function (i) {
                                        return item.txName === i.txName;
                                    }) && (item.type !== 'text' && item.payload && item.payload.length >= 1 || item.type === 'text' || item.type === 'calendardate');
                            });

                        });
                    }
                });


                $scope.close = function () {
                    $scope.editing = false;
                    angular.element($window).unbind('keydown', $scope.keydown);
                    $timeout(function () {
                        $scope.$apply();
                    });

                };

                $scope.activateNext = function () {
                    if ($scope.available.length > $scope.active + 1) {
                        ++$scope.active;
                    }
                    $timeout(function () {
                        $scope.$apply();
                    });
                };

                $scope.activatePrev = function () {
                    if ($scope.active > 0) {
                        --$scope.active;
                    } else {
                        $scope.active = 0;
                    }
                    $timeout(function () {
                        $scope.$apply();
                    });
                };

                $scope.keydown = function (e) {
                    var handledKeys = [KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY];
                    if (handledKeys.indexOf(e.which) === -1) {
                        return;
                    }

                    if (e.which === KeyCodes.RETURNKEY) {
                        e.preventDefault();
                        $scope.addFilter($scope.available[$scope.active], e);
                        $scope.close();
                    }

                    if (e.which === KeyCodes.DOWNARROW) {
                        e.preventDefault();
                        $scope.activateNext();
                    }
                    if (e.which === KeyCodes.UPARROW) {
                        e.preventDefault();
                        $scope.activatePrev();
                    }
                };

                $scope.addFilter = function (item, e) {
                    $rootScope.$broadcast('closeDD', e);
                    e.stopPropagation();
                    $timeout(function () {
                        item.editing = true;
                        $scope.used.push(item);
                        $scope.available = _.filter($scope.available, function (elm) {
                            return elm.txName !== item.txName;
                        });
                        //$scope.openEdit(e);
                    });
                };

                $scope.apply = function (reload) {
                    reload = reload || false;

                    var values = _.chain($scope.used).keyBy('txName').transform(function (result, value, key) {
                        if (value.type === 'select') {
                            result[key] = value.value.id;
                        }
                        if (value.type === 'multiselect') {
                            result[key] = _.map(value.value, 'id').join(',');
                        }
                        if (value.type === 'text') {
                            result[key] = value.value;
                        }
                        if (value.type === 'daterange') {
                            result[key + '_end'] = value.value.end;
                            result[key + '_start'] = value.value.start;
                        }
                        if (value.type === 'calendardate') {
                            result[key + '_end'] = value.value.end;
                            result[key + '_start'] = value.value.start;
                            result[key + '_view'] = value.value.view;
                            result[key + '_currentDay'] = value.value.currentDay;
                        }
                    }).value();
                    qs.setValues(values, true, reload);
                };

                $scope.clear = function () {
                    $scope.available = _.filter($scope.availableFilters, function (item) {
                        return item;
                    });
                    $scope.used = [];
                    qs.setValues([], true);
                };

                $scope.openEdit = function (e) {
                    if (e) {
                        e.stopPropagation();
                    }
                    $rootScope.$broadcast('closeDD', e);
                    $timeout(function () {
                        $scope.editing = true;
                        angular.element($window).bind('keydown', $scope.keydown);
                    }, 50);
                };


                $scope.remove = function (item) {
                    $scope.available.push(item);
                    $scope.used = _.filter($scope.used, function (elm) {
                        return elm.txName !== item.txName;
                    });
                    if (angular.isDefined(item.reset)) {
                        item.reset();
                    }
                };

                $scope.editPrevious = function (item) {
                    var idx = _.findIndex($scope.used, function (elm) {
                        return elm.txName === item.txName;
                    });

                    var next = $scope.used[--idx];

                    if (next) {
                        item.editing = false;
                        $timeout(function () {
                            next.editing = true;
                            next.focus();
                            $scope.$apply();
                        });

                    }
                };

                $scope.editNext = function (item) {
                    var idx = _.findIndex($scope.used, function (elm) {
                        return elm.txName === item.txName;
                    });

                    var next = $scope.used[++idx];

                    if (next) {
                        item.editing = false;
                        $timeout(function () {
                            next.editing = true;
                            next.focus();
                            $scope.$apply();
                        });
                    } else {
                        item.editing = false;
                    }
                };

            }
        };
    }]);
};