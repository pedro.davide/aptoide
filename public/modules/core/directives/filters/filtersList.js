'use strict';

module.exports = function (module) {
    module.directive('filtersList', ['qs', '$timeout', function (qs, $timeout) {
        var _ = window._;
        return {
            restrict: 'E',
            templateUrl: '/modules/core/views/filters/list.html',
            scope: {
                availableFilters: '='
            },
            link: function ($scope, iElm, iAttrs) {
                $scope.filters = qs.getAll();

                $scope.$watch('availableFilters', function (value) {
                    if (value) {
                        $scope.list = _.filter(_.map(value, function (item) {
                            if (item.type === 'daterange' || item.type === 'calendardate') {
                                if ($scope.filters.hasOwnProperty(item.txName + '_end')) {
                                    return {
                                        display: item.display,
                                        value: [$scope.filters[item.txName + '_start'] + ' to ' + $scope.filters[item.txName + '_end']]
                                    };
                                }
                            }
                            if (item.type === 'select' || item.type === 'multiselect') {
                                if ($scope.filters.hasOwnProperty(item.txName)) {
                                    var itemValue = $scope.filters[item.txName];

                                    // find value in payload
                                    if (item.hasOwnProperty('payload')) {
                                        var txNameArray = [];
                                        var filtersArray = ($scope.filters[item.txName]).split(',');
                                        _.forEach(filtersArray, function (filter) {
                                            txNameArray.push(_.find(item.payload, {id: parseInt(filter)}));
                                        });
                                        itemValue = _.map(txNameArray, 'txName');
                                    }

                                    return {display: item.display, value: itemValue};
                                }
                            }
                            if (item.type === 'text') {
                                if ($scope.filters.hasOwnProperty(item.txName)) {
                                    return {display: item.display, value: [$scope.filters[item.txName]]};
                                }
                            }

                            return false;
                        }));
                    }
                });
            }
        };
    }]);
};