'use strict';

module.exports = function (module) {
    module.directive('filterDaterange', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
        return {
            restrict: 'A',
            replace: true,
            transcluded: true,
            scope: {
                filterDaterange: '='
            },
            templateUrl: '/modules/core/views/filters/daterange.html',
            controller: function ($scope) {

                window.moment.locale('en');
                var now = window.moment();
                //$scope.myDates={ start:now.format( 'YYYY-MM-DD' ) , end:now.format( 'YYYY-MM-DD' ) };
                //$scope.filterDaterange.value={ start:now.format( 'YYYY-MM-DD' ) , end:now.format( 'YYYY-MM-DD' ) };

                if (qs.getValue($scope.filterDaterange.txName + '_start') && qs.getValue($scope.filterDaterange.txName + '_end')) {
                    $scope.filterDaterange.value = {
                        start: window.moment(qs.getValue($scope.filterDaterange.txName + '_start')).format('YYYY-MM-DD'),
                        end: window.moment(qs.getValue($scope.filterDaterange.txName + '_end')).format('YYYY-MM-DD')
                    };
                } else {
                    $scope.filterDaterange.value = {start: now.format('YYYY-MM-DD'), end: now.format('YYYY-MM-DD')};
                }

            },
            link: function ($scope, iElm, iAttrs) {
                $scope.editing = false;
                var KeyCodes = {
                    BACKSPACE: 8,
                    TABKEY: 9,
                    RETURNKEY: 13,
                    ESCAPE: 27,
                    SPACEBAR: 32,
                    LEFTARROW: 37,
                    UPARROW: 38,
                    RIGHTARROW: 39,
                    DOWNARROW: 40
                };


                $scope.focus = function () {
                    $scope.editing = true;
                };
                $scope.setValue = function (result) {
                    $scope.filterDaterange.value = result;
                    //$scope.$parent.editNext($scope.filterDaterange);
                };

            }
        };
    }]);
};