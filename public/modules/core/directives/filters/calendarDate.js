'use strict';

var moment = window.moment;

module.exports = function (module) {
    module.directive('filterCalendarDate', ['$timeout', 'qs', '$window', function ($timeout, qs, $window) {
        return {
            restrict: 'A',
            replace: true,
            transcluded: true,
            scope: {
                filterCalendarDate: '='
            },
            templateUrl: '/modules/core/views/filters/calendarDate.html',
            link: function ($scope, iElm, iAttrs) {
                $scope.currentDate = $scope.$parent.$parent.$parent.$parent.calendarDay;

                $scope.filterCalendarDate.value = $scope.filterCalendarDate.defaultValue;

                $scope.filterCalendarDate.value.start = qs.getValue($scope.filterCalendarDate.txName + '_start', $scope.filterCalendarDate.defaultValue.start);
                $scope.filterCalendarDate.value.end = qs.getValue($scope.filterCalendarDate.txName + '_end', $scope.filterCalendarDate.defaultValue.end);
                $scope.filterCalendarDate.value.view = qs.getValue($scope.filterCalendarDate.txName + '_view', $scope.filterCalendarDate.defaultValue.view);
                $scope.filterCalendarDate.value.currentDay = qs.getValue($scope.filterCalendarDate.txName + '_currentDay', $scope.filterCalendarDate.defaultValue.currentDay);

                $scope.nav = function (direction) {
                    var objDate = {dtEvent_view: $scope.filterCalendarDate.value.view};
                    var now;
                    if (direction === 'left') {
                        now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString()).subtract(1, $scope.filterCalendarDate.value.view + 's');
                    }
                    else if (direction === 'right') {
                        now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString()).add(1, $scope.filterCalendarDate.value.view + 's');
                    }
                    else {
                        now = moment(new Date().toString());
                    }
                    var dtEvent_start, dtEvent_end = now;

                    objDate.dtEvent_currentDay = now.format('YYYY-MM-DD');

                    if ($scope.filterCalendarDate.value.view === 'month') {
                        dtEvent_start = now.startOf('month');
                    }
                    else if ($scope.filterCalendarDate.value.view === 'week') {
                        dtEvent_start = now.startOf('week');
                    }
                    else {
                        dtEvent_start = now;
                    }

                    objDate.dtEvent_start = dtEvent_start.format('YYYY-MM-DD');

                    if ($scope.filterCalendarDate.value.view === 'month') {
                        dtEvent_end = now.endOf('month');
                    }
                    else if ($scope.filterCalendarDate.value.view === 'week') {
                        dtEvent_end = now.endOf('week');
                    }
                    else {
                        dtEvent_end = now;
                    }

                    objDate.dtEvent_end = dtEvent_end.format('YYYY-MM-DD');

                    $scope.filterCalendarDate.value = objDate;

                    qs.setValues(objDate);
                };

                $scope.day = function () {
                    var objDate = {dtEvent_view: 'day'};
                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

                    objDate.dtEvent_start = now.format('YYYY-MM-DD');
                    objDate.dtEvent_end = now.format('YYYY-MM-DD');

                    $scope.filterCalendarDate.value = objDate;

                    qs.setValues(objDate);
                };

                $scope.week = function () {
                    var objDate = {dtEvent_view: 'week'};
                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

                    objDate.dtEvent_start = now.startOf('week').format('YYYY-MM-DD');
                    objDate.dtEvent_end = now.endOf('week').format('YYYY-MM-DD');

                    $scope.filterCalendarDate.value = objDate;

                    qs.setValues(objDate);
                };

                $scope.month = function () {
                    var objDate = {dtEvent_view: 'month'};
                    var now = moment(new Date($scope.filterCalendarDate.value.currentDay).toString());

                    objDate.dtEvent_start = now.startOf('month').format('YYYY-MM-DD');
                    objDate.dtEvent_end = now.endOf('month').format('YYYY-MM-DD');

                    $scope.filterCalendarDate.value = objDate;

                    qs.setValues(objDate);
                };

                $scope.getCurrentView = function (view) {
                    return view === $scope.filterCalendarDate.value.view;
                };
            }
        };
    }]);
};
