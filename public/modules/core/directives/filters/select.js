'use strict';

module.exports = function (module) {
    module.directive('filterSelect', ['$timeout', 'qs', '$window', '$rootScope', function ($timeout, qs, $window, $rootScope) {
        var _ = window._;
        return {
            restrict: 'A',
            scope: {
                filterSelect: '='
            },
            templateUrl: '/modules/core/views/filters/select.html',
            controller: function ($scope) {


            },
            link: function ($scope, iElm, iAttrs) {
                var input = iElm.find('input');
                var prevHandler;
                $scope.active = -1;
                $scope.query = null;
                $scope.currentActive = null;
                $scope.startIdx = 0;
                $scope.items = _.filter($scope.filterSelect.payload, function (item) {
                    if (!$scope.query) {
                        return true;
                    }
                    return item.txName.toLowerCase().indexOf($scope.query.toLowerCase()) > -1;
                });
                $scope.endIdx = $scope.filterSelect.payload.length;

                $scope.$on('closeDD', function (event) {
                    $scope.close();
                });
                $scope.$watch('query', _.debounce(function (newval, oldval) {
                    if (newval !== oldval) {
                        $scope.$apply(function () {
                            $scope.items = _.filter($scope.filterSelect.payload, function (item) {
                                if (!newval) {
                                    return true;
                                }
                                return item.txName.toLowerCase().indexOf(newval.toLowerCase()) > -1;
                            });
                            $scope.endIdx = $scope.items.length;
                            $scope.active = -1;
                        });
                    }
                }, 200));
                $scope.$watch('filterSelect', function (newval, oldval) {
                    if (newval) {
                        $scope.filterSelect.focus = function () {
                            $timeout(function () {
                                input[0].focus();
                            });
                        };
                        $scope.filterSelect.value = _.find($scope.filterSelect.payload, function (item) {
                            if (!isNaN(item.id)) {
                                return parseInt(item.id) === parseInt(qs.getValue($scope.filterSelect.txName));
                            }
                            return item.id === qs.getValue($scope.filterSelect.txName);
                        });
                        if ($scope.filterSelect.editing) {
                            $scope.filterSelect.focus();
                        }
                        // set default if no value defined
                        if (!$scope.filterSelect.value && !qs.getValue($scope.filterSelect.txName)) {
                            if (_.isPlainObject($scope.filterSelect.defaultValue) && _.has($scope.filterSelect.defaultValue, 'id') && _.has($scope.filterSelect.defaultValue, 'txName')) {
                                $scope.filterSelect.value = $scope.filterSelect.defaultValue;
                            }
                            else {
                                $scope.filterSelect.value = {
                                    id: $scope.filterSelect.defaultValue,
                                    txName: $scope.filterSelect.defaultValue
                                };
                            }
                        }
                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                });
                var KeyCodes = {
                    BACKSPACE: 8,
                    TABKEY: 9,
                    RETURNKEY: 13,
                    ESCAPE: 27,
                    SPACEBAR: 32,
                    LEFTARROW: 37,
                    UPARROW: 38,
                    RIGHTARROW: 39,
                    DOWNARROW: 40
                };

                $scope.close = function () {
                    $scope.filterSelect.editing = false;
                    $scope.query = '';

                    if ($scope.filterSelect.value.id === undefined) {
                        $scope.$parent.remove($scope.filterSelect);
                    }
                };

                $scope.edit = function (val, e) {
                    $rootScope.$broadcast('closeDD', e);
                    if (val) {
                        $scope.filterSelect.focus();
                    }
                    $scope.filterSelect.editing = val;

                    $scope.$broadcast('vsRepeatTrigger', {
                        scrollIndex: $scope.active,
                        scrollIndexPosition: 'inview#auto'
                    });
                    e.stopPropagation();
                };

                $scope.select = function (filter, e) {
                    e.stopPropagation();
                    $scope.filterSelect.value = filter;
                    $scope.close();
                    $scope.$parent.editNext($scope.filterSelect);
                };

                $scope.setActive = function (item) {
                    if (item) {
                        $scope.currentActive = item;
                    }
                };


                $scope.activateNext = function () {
                    if ($scope.endIdx > $scope.active + 1) {
                        ++$scope.active;
                    }
                };

                $scope.activatePrev = function () {
                    if ($scope.active > 0) {
                        --$scope.active;
                    } else {
                        $scope.active = -1;
                    }
                };

                $scope.isActive = function (idx) {
                    return idx === $scope.active ? 'active' : '';
                };

                $scope.keydown = function (e) {
                    var handledKeys = [KeyCodes.TABKEY, KeyCodes.DOWNARROW, KeyCodes.UPARROW, KeyCodes.RETURNKEY, KeyCodes.LEFTARROW, KeyCodes.RIGHTARROW];
                    if (handledKeys.indexOf(e.which) === -1) {
                        return;
                    }
                    if (e.which === KeyCodes.RETURNKEY) {
                        e.preventDefault();
                        if ($scope.active >= 0) {
                            $scope.filterSelect.value = $scope.items[$scope.active];
                            $scope.$parent.editNext($scope.filterSelect);
                        }
                        $scope.close();
                    }
                    if (e.which === KeyCodes.TABKEY && $scope.filterSelect.value) {
                        e.preventDefault();
                        if ($scope.active >= 0) {
                            $scope.filterSelect.value = $scope.filterSelect.payload[$scope.active];
                        }
                        $scope.close();
                        $scope.$parent.editNext($scope.filterSelect);
                    }
                    if (e.which === KeyCodes.DOWNARROW) {
                        e.preventDefault();
                        $scope.activateNext();
                    }
                    if (e.which === KeyCodes.UPARROW) {
                        e.preventDefault();
                        $scope.activatePrev();
                    }
                    $scope.$broadcast('vsRepeatTrigger', {
                        scrollIndex: $scope.active,
                        scrollIndexPosition: 'inview#auto'
                    });
                };
            }
        };
    }]);
};