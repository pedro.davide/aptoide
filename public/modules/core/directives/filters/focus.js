'use strict';

module.exports = function (module) {
    module.directive('filterFocus', ['$timeout', '$parse',
        function ($timeout, $parse) {
            return {
                restrict: 'A',
                link: function ($scope, $element, $attrs) {
                    var model = $parse($attrs.filterFocus);
                    $scope.$watch(model, function (value) {
                        if (value === true) {
                            $timeout(function () {
                                $element[0].focus();
                            });
                        }
                    });
                    $element.bind('blur', function () {
                        $scope.$apply(model.assign($scope, false));
                    });
                }
            };
        }
    ]);
};