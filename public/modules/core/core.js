'use strict';


var appCore = angular.module('app.core', [
    'ngAnimate',
    'ui.router',
    'ngResource'
]);
 
require('moduleDir/core/configs/run')(appCore);
require('moduleDir/core/configs/routes')(appCore);
require('moduleDir/core/controllers/appsetting')(appCore);
require('moduleDir/core/controllers/pagination')(appCore);
require('moduleDir/core/services/notifications')(appCore);
require('moduleDir/core/services/ld')(appCore);
require('moduleDir/core/services/querystring')(appCore);
require('moduleDir/core/services/checkboxes')(appCore);
//require('moduleDir/core/services/ui-load')(appCore);

require('moduleDir/core/values/pagination')(appCore);

require('moduleDir/core/directives/loadAware')(appCore);
require('moduleDir/core/directives/notifications')(appCore);
require('moduleDir/core/directives/permissions')(appCore);
require('moduleDir/core/directives/ui-nav')(appCore);
//require('moduleDir/core/directives/ui-jp')(appCore);

/** filters */
require('moduleDir/core/directives/filters/filtersBar')(appCore);
require('moduleDir/core/directives/filters/filtersList')(appCore);
require('moduleDir/core/directives/filters/name')(appCore);
require('moduleDir/core/directives/filters/select')(appCore);
require('moduleDir/core/directives/filters/multiselect')(appCore);
require('moduleDir/core/directives/filters/daterange')(appCore);
require('moduleDir/core/directives/filters/autoResize')(appCore);
require('moduleDir/core/directives/filters/focus')(appCore);
require('moduleDir/core/directives/filters/calendarDate')(appCore);

require('moduleDir/core/filters/highlight')(appCore);