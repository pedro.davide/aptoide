'use strict';

/**
 * Filters
 * @module: app.core
 * @desc: Date to ISO
 */
module.exports = function (module) {
    var moment = window.moment;
    module.filter('trust', ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        };
    }]);
};