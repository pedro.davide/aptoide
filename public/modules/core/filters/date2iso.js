'use strict';

/**
 * Filters
 * @module: app.core
 * @desc: Date to ISO
 */
module.exports = function (module) {
    var moment = window.moment;
    module.filter('date2iso', function () {
        return function(input) {
            if(!input){
                return input;
            }
            input = moment(input, 'YYYY-MM-DD').format();
            return input;
        };
    });
};