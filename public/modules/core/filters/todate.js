'use strict';

/**
 * Filters
 * @module: app.core
 * @desc: Date to ISO
 */
module.exports = function (module) {
    var moment = window.moment;
    module.filter('todate', function () {
        return function(input) {
            if(!input){
                return input;
            }
            input = new Date(input);
            return input;
        };
    });
};