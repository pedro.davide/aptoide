'use strict';

module.exports = function (module) {
    module.value('pagination', { perPages: [30, 50, 100] });
};