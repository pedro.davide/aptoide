'use strict';

module.exports = function (module) {
    module.factory('auth', ['$q', '$rootScope', 'user', function ($q, $rootScope, user) {
        return {
            'response': function (response) {
                if (response.data.result === 'OK') {
                    user.avatar = response.data.meta.user.avatar;
                    user.txName = response.data.meta.user.txName;
                    if (response.data.meta.user.impersonated) {
                        user.impersonated = response.data.meta.user.impersonated;
                    } else {
                        user.impersonated = false;
                    }
                }
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    window.location.href = '/login';
                }
                return $q.reject(rejection);
            }
        };
    }]);
};