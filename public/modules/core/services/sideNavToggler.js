'use strict';

module.exports = function (module) {
    module.factory('sideNavToggler',['$mdSidenav',function($mdSidenav) {
        var buildToggler = function (navID) {
            return function() {
                $mdSidenav(navID).toggle();
            };
        };

        var buildCloser = function (navID) {
            return function() {
                $mdSidenav(navID).close();
            };
        };


        return {
            opener : buildToggler,
            closer : buildCloser
        };
    }]);
};