'use strict';

module.exports = function (module) {
    module.factory('qs',['$location','$state','$rootScope', function($location,$state,$rootScope) {
        $rootScope.queryString = $rootScope.queryString ? $rootScope.queryString : {};

        var pack = function(params){
            var querystring = [];
            for(var name in params){
                querystring.push(name+'='+encodeURIComponent(params[name]));
            }
            return querystring.join('&');
        };

        var unpack = function(string){
            if(typeof string !== 'string' ){
                return {};
            }
            var querystringObj = {};
            var params = string.split('&');
            for(var idx in params){
                var kv = params[idx].split('=');
                querystringObj[kv[0]] = decodeURIComponent(kv[1]);
            }
            return querystringObj;
        };

        return {
            pack : pack,
            unpack : unpack,
            setValue : function(name,value, notify){
                if (!notify) {
                    notify = true;
                }
                $rootScope.queryString[name] = value;
                $state.go($state.current.name,{options : pack($rootScope.queryString)}, { notify: notify });
            },
            removeValue : function(name){
                delete $rootScope.queryString[name];
                $state.go($state.current.name,{options : pack($rootScope.queryString)});
            },
            setValues : function(values,replace,notify){
                if (!notify) {
                    notify = true;
                }
                if(replace){
                    $rootScope.queryString = {};
                }
                for(var idx in values){
                    $rootScope.queryString[idx] = values[idx];
                }
                return $state.go($state.current.name,{options : pack($rootScope.queryString)},{notify: notify});
            },
            getValue : function(name,defaultValue){
                $rootScope.queryString = unpack($location.$$search.options);
                return $rootScope.queryString[name] ? $rootScope.queryString[name] : defaultValue;
            },
            getAll : function(){
                $rootScope.queryString = unpack($location.$$search.options);

                return $rootScope.queryString ? $rootScope.queryString : {};
            }
        };
    }]);
};