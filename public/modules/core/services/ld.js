'use strict';

module.exports = function (module) {
    module.factory('ld',['$q',function($q) {
        return function(){
            var args = Array.prototype.slice.call(arguments);
            var $scope = args.pop();
            $scope.loading = true;
            $q.all(args).finally(function () {
                $scope.loading = false;
                $scope.loaded = true;
            });
        };
    }]);
};