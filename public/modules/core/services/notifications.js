'use strict';

module.exports = function (module) {
    module.factory('notify', ['$rootScope', function ($rootScope) {

        $rootScope.notification = null;

        var error = function (message) {
            $rootScope.notification = { icon: 'fa fa-ban', message: message, type: 'danger' };
        };

        var warning = function (message) {
            $rootScope.notification = { icon: 'fa fa-bell', message: message, type: 'warning' };
        };

        var success = function (message) {
            $rootScope.notification = { icon: 'fa fa-check', message: message, type: 'mint' };
        };

        var close = function () {
            $rootScope.notification = null;
        };

        var has = function () {
            return $rootScope.notification !== null;
        };

        var notification = function () {
            return $rootScope.notification;
        };

        return {
            error: error,
            warning: warning,
            success: success,
            close: close,
            has: has,
            notification : notification
        };
    }]);
};