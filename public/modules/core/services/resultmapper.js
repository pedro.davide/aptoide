'use strict';

module.exports = function (module) {
    module.factory('rm', ['$q', function ($q) {
        return function ($scope, result) {
            if (result.item) {
                $scope.item = result.item;
            }
            if (result.items) {
                $scope.items = result.items;
            }
            if (!$scope.meta) {
                $scope.meta = result.meta;
            }
            if (result.pagination) {
                $scope.pagination = result.pagination;
                $scope.pagination.order = result.pagination.order;
            }
        };
    }]);
};