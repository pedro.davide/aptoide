'use strict';

module.exports = function (module) {
    var _ = window._;
    module.factory('checkboxes',['$q',function($q) {
        return function(selected){

            var toggle = function (value) {
                var idx = selected.indexOf(value);
                if (idx > -1) {
                    selected.splice(idx, 1);
                } else {
                    selected.push(value);
                }

            };

            var isChecked = function (item) {
                var idx = selected.indexOf(item);
                if (idx > -1) {
                    return true;
                } else {
                    return false;
                }
            };

            var isCheckedAll = function (items) {
                if (items && selected.length === items.length) {
                    return true;
                } else {
                    return false;
                }
            };

            var toggleAll = function (value) {
                if (selected.length === value.length) {
                    _.remove(selected);
                } else {
                    _.each(value, function (elm) {
                        if(!_.includes(selected,elm)){
                            selected.push(elm);
                        }
                    });
                }
            };

            var getSelected = function () {
                return _.map(selected,function(item){return item.id;});
            };

            var reset = function () {
                _.remove(selected,function(){return true;});
            };

            return {
                toggle : toggle,
                isChecked : isChecked,
                isCheckedAll : isCheckedAll,
                toggleAll : toggleAll,
                getSelected : getSelected,
                reset : reset
            };
        };
    }]);
};