'use strict';

module.exports = function (module) {
    module.factory('refresher',['$q','qs',function($q,qs) {
        return function($scope){
            return function (one, two) {
                $scope.selected = [];
                if (arguments.length === 1) {
                    var orderDir = {
                        order: one
                    };
                    qs.setValues(orderDir).then(function(){
                        $scope.pagination.order = orderDir.order;
                    }).then(function(){
                        $scope.promise = $scope.data();
                    });
                }
                if (arguments.length === 2){
                    var pageLimit = {
                        page: one,
                        limit: two
                    };
                    qs.setValues(pageLimit).then(function(){
                        $scope.pagination.page = pageLimit.page;
                        $scope.pagination.limit = pageLimit.limit;
                    }).then(function(){
                        $scope.promise = $scope.data();
                    });
                }

            };
        };
    }]);
};