/**
 * Core run
 * @module: app.core
 */
module.exports = function(module) {
    var _ = window._;
    var DOMParser = window.DOMParser;
    var ActiveXObject = window.ActiveXObject;

    module.run(['$rootScope', '$state', '$stateParams','notify','qs','checkboxes', function($rootScope, $state, $stateParams,notify, qs, checkboxes) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        
        $rootScope.errorHandler = function (message, form) {
            return function (result) {
                notify.error(message);

                if (result.data.errors) {
                    angular.forEach(result.data.errors, function (errors, key) {
                        angular.forEach(errors, function (e) {
                            form[key].$dirty = true;
                            form[key].$setValidity(e, false);
                        }, this);
                    }, this);
                }
            };
        };

        $rootScope.qs = qs;

        $rootScope.selected = [];
        $rootScope.checkboxes = checkboxes($rootScope.selected);

        $rootScope.fixNameAndId = function(arr, type) {
            if (!type) {
                type = 'name';
            }

            return _.map(arr, function(item) {
                return { id: item.id, txName: item[type] };
            });
        };

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            notify.close();
        });
    }])
    ;
};
