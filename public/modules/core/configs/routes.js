'use strict';

/**
 * Core routes
 * @module: app.core
 */
var moment = window.moment;
module.exports = function(module) {
    module.config([
        '$locationProvider',
        '$urlRouterProvider',
        '$stateProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$httpProvider',
        '$provide',
        function($locationProvider, $urlRouterProvider, $stateProvider, $controllerProvider, $compileProvider, $filterProvider,$httpProvider, $provide) {
            /** store a reference to various provider functions */
            module.controller = $controllerProvider.register;
            module.directive = $compileProvider.directive;
            module.filter = $filterProvider.register;
            module.factory = $provide.factory;
            module.provider = $provide.provider;
            module.service = $provide.service;
            module.constant = $provide.constant;
            module.value = $provide.value;

            /** default route */
            $urlRouterProvider.otherwise('/superheros/list');

            /** parent route */
            $stateProvider.state('default', {
                abstract: true,
                url: '',
                templateUrl: 'modules/core/views/layouts/default.html'
            });
        }
    ])
    .config(function($breadcrumbProvider) {
        $breadcrumbProvider.setOptions({
            templateUrl: '/modules/core/views/partials/breadcrumb.html'
        });
    })
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }])

    ;
};
