'use strict';

module.exports = function (module) {
    return module.factory('Teams', ['$resource',function ($resource) {
        return $resource('/api/teams/:teamId', { teamId: '@teamId' }, {
            query : {
                isArray:false
            },
            update :{
                method : 'PUT'
            },
            create : {
                url : '/api/teams/create',
                method : 'GET'
            },
            bulkDelete : {
                url : '/api/teams/bulk/delete',
                method : 'POST'
            }
        });
    }]);
};