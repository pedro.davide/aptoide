'use strict';

var mod = angular.module('app.teams',[]);

require('moduleDir/teams/configs/routes')(mod);
require('moduleDir/teams/controllers/teams')(mod);
require('moduleDir/teams/services/teams')(mod);