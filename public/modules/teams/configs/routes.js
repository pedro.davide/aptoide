'use strict';

module.exports = function (module) {
    module.config(['$stateProvider', function ($stateProvider) {
        
        $stateProvider.state('default.teams', {
            url: '/teams',
            template: '<ui-view></ui-view>',
            abstract: true,
            ncyBreadcrumb: {skip : true},
            data: {
                title: 'Teams'
            }
        })
        .state('default.teams.list', {
            url: '/list?options',
            controller: 'default.teams',
            templateUrl: 'modules/teams/views/index.html',
            ncyBreadcrumb: {
                parent: 'default.home',
                label: 'Teams'
            }
        })
        .state('default.teams.edit', {
            url: '/:categoryId/edit',
            controller: 'default.teams.edit',
            templateUrl: 'modules/teams/views/edit.html',
            abstract: true,
            ncyBreadcrumb: {
                parent:'default.teams.list',
                label: '{{ item.name }}'
            }
        })
        .state('default.teams.edit.details', {
            url: '/details',
            controller: 'default.teams.edit.details',
            templateUrl: 'modules/teams/views/details.html',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.teams.list'
            }
        })
        .state('default.teams.new', {
            url: '/new',
            controller: 'default.teams.edit.new',
            templateUrl: 'modules/teams/views/details.html',
            ncyBreadcrumb: {
                parent:'default.teams.list',
                label: 'New team'
            }
        })
        .state('default.teams.edit.delete', {
            url: '/delete',
            templateUrl: 'modules/teams/views/delete.html',
            controller: 'default.teams.edit.delete',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.teams.edit.details'
            }
        })
        ;
    }
    ]);
};