'use strict';

var app = angular.module('app', [
    'angular-loading-bar',
    'ncy-angular-breadcrumb',
    'ui.bootstrap.showErrors',
    'ui.bootstrap',
    'ngSanitize',
    'gaDatePickerRange',
    'ui.select',
    'bw.paging',
    'yaru22.angular-timeago',
    'ngTagsInput',
    'app.core',
    'app.dashboard',
    'app.users',
    'app.teams',
    'app.powers',
    'app.superheros'
]);

require('moduleDir/core/core');
require('moduleDir/dashboard/dashboard');
require('moduleDir/users/users');
require('moduleDir/teams/teams');
require('moduleDir/powers/powers');
require('moduleDir/superheros/superheros');