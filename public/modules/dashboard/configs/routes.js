'use strict';

module.exports = function (module) {
    module.config(['$stateProvider',function ($stateProvider) {

	    $stateProvider.state('default.home', {
		    url: '/dashboard?options',
		    controller : 'default.home',
		    templateUrl: 'modules/dashboard/views/index.html',
		    data: {
			    title: 'Dashboard'
		    },
            ncyBreadcrumb: {
                label: 'Home'
            }
	    });
    }]);
};