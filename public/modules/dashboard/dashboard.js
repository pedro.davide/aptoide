'use strict';

var mod = angular.module('app.dashboard',[]);

require('moduleDir/dashboard/configs/routes')(mod);
require('moduleDir/dashboard/services/dashboard')(mod);
require('moduleDir/dashboard/controllers/dashboard')(mod);