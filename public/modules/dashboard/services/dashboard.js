'use strict';

module.exports = function (module) {
    return module.factory('Dashboard', ['$resource',function ($resource) {
        return $resource('/dashboard/:orderId',{orderId:'@orderId'},{
            query : {
                isArray:false
            },
            update :{
                method : 'PUT'
            },
            stats: {
                method: 'GET',
                url: '/dashboard/stats'
            },
            cart: {
                method: 'GET',
                url: '/dashboard/cart'
            },

        });
    }]);
};

