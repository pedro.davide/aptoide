'use strict';

global.moment = require('moment');
global._ = require('lodash');

/**
 * Vendors
 * @desc: Load up application needed script
 */
require('expose?jQuery!expose?$!jquery');
require('expose?Tether!expose?$!tether');
require('bootstrap');
require('angular');
require('angular-animate');
require('angular-ui-router');
require('angular-resource');
require('angular-breadcrumb');
require('angular-bootstrap');
require('angular-bootstrap-show-errors');
require('angular-loading-bar');
require('angular-sanitize');
require('ng-gadaterangepicker');
require('angular-ui-select');
require('angular-paging');
require('angular-timeago');
require('ng-tags-input');