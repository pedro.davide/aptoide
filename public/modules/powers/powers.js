'use strict';

var mod = angular.module('app.powers',[]);

require('moduleDir/powers/configs/routes')(mod);
require('moduleDir/powers/controllers/powers')(mod);
require('moduleDir/powers/services/powers')(mod);