'use strict';

module.exports = function (module) {
    return module.factory('Powers', ['$resource',function ($resource) {
        return $resource('/api/powers/:powerId', { powerId: '@powerId' }, {
            query : {
                isArray:false
            },
            update :{
                method : 'PUT'
            },
            create : {
                url : '/api/powers/create',
                method : 'GET'
            },
            bulkDelete : {
                url : '/api/powers/bulk/delete',
                method : 'POST'
            }
        });
    }]);
};