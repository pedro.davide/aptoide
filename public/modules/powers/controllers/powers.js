'use strict';

module.exports = function (module) {
    var _ = window._;
    var moment = window.moment;

    module.controller('default.powers', ['$scope', 'Powers', 'qs','notify','ld','$timeout','$window','checkboxes','$state','$stateParams', function ($scope, Powers, qs,notify,ld,$timeout,$window,checkboxes,$state,$stateParams) {
        $scope.items = [];
        $scope.meta = {};
        $scope.pagination = {};
        $scope.loading = true;

        $scope.bulkDelete = function (items, $event) {
            if ($window.confirm('Are you sure to delete ' + items.length + ' items?')) {
                ld(Powers.bulkDelete({ items: items }).$promise.then(function (result) {
                    checkboxes($scope.selected).reset();
                    return $state.reload().then(function () {
                        notify.success('Deleted successfully');
                    });
                }),$scope);
            }
        };

        ld(Powers.query(angular.extend($stateParams, qs.getAll())).$promise.then(function (result) {
            $scope.meta = result.meta;
            $scope.items = result.items;
            $scope.pagination = result.pagination;

            $scope.loading = false;

            $scope.filters = [
                    /*
                {
                    txName: 'created_at',
                    type: 'daterange',
                    display: 'Created',
                    defaultValue: {start: moment(new Date()).format('YYYY-MM-DD'), end: moment(new Date()).format('YYYY-MM-DD')},
                    labels: {custom: 'Custom', today: 'Today', yesterday: 'Yesterday', thisWeek: 'This week', lastWeek: 'Last week', thisMonth: 'This month', lastMonth: 'Last month', last7Days: 'Last 7 days', last30Days: 'Last 30 days', apply: 'Apply', period: 'Period', cancel: 'Cancel'}
                },
                */
                { txName: 'name', type: 'text', display: 'Name' }
            ];
        }),$scope);
    }])
    .controller('default.powers.edit', ['$scope', 'Powers', 'ld','notify','$stateParams', function ($scope, Powers, ld, notify,$stateParams) {
        $scope.item = {};
        $scope.meta = {};
        $scope.loading = true;

        $scope.sections = [
            {
                divider: true,
                name: 'Main',
                showWhenNew: true
            },
            {
                divider: false,
                name: 'Details',
                icon: 'edit',
                state: 'default.powers.edit.details',
                showWhenNew: true
            },
            {
                divider: true,
                name: 'Options',
                showWhenNew: false
            },
            {
                divider: false,
                name: 'Delete',

                icon: 'delete',
                state: 'default.powers.edit.delete',
                showWhenNew: false
            }
        ];

        ld(Powers.get($stateParams).$promise.then(function (result) {
            $scope.item = result.item;
            $scope.meta = result.meta;
        }),$scope);
    }])
    .controller('default.powers.edit.new', ['$scope', 'Powers', 'ld','notify','$state','$stateParams', function ($scope, Powers, ld, notify,$state,$stateParams) {
        $scope.item = {};
        $scope.progress = 0;

        ld(Powers.create().$promise.then(function (result) {
            $scope.meta = result.meta;
        }), $scope);

        $scope.save = function(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid) {
                ld(Powers.save(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
                    $state.go('^.list', {}, {reload: true}).then(function () {
                        notify.success('Saved successfully');
                    });
                }).catch($scope.errorHandler('Failed to create power',form)), $scope);
            }
        };
    }]).controller('default.powers.edit.delete', ['$scope', 'Powers', 'ld','notify','$stateParams','$state', function ($scope, Powers, ld, notify, $stateParams,$state) {
        $scope.delete = function() {
            ld(Powers.delete($stateParams).$promise.then(function (result) {
                $state.go('^.^.list', {reload: true}).then(function () {
                    notify.success('Deleted successfully');
                });
            }).catch($scope.errorHandler('Failed to delete power', null)), $scope);
        };
    }]).controller('default.powers.edit.details', ['$scope', 'Powers', 'ld','notify','$stateParams','$state', function ($scope, Powers, ld, notify, $stateParams, $state) {

        $scope.save = function(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (form.$valid) {
                ld(Powers.update(angular.extend($stateParams, $scope.item)).$promise.then(function (result) {
                    return $state.reload().then(function () {
                        $scope.item = result.item;
                        notify.success('Save successfully');
                    });
                }).catch($scope.errorHandler('Failed to update power', form)), $scope);
            }
        };
    }])
    ;
};