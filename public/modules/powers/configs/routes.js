'use strict';

module.exports = function (module) {
    module.config(['$stateProvider', function ($stateProvider) {
        
        $stateProvider.state('default.powers', {
            url: '/powers',
            template: '<ui-view></ui-view>',
            abstract: true,
            ncyBreadcrumb: {skip : true},
            data: {
                title: 'Powers'
            }
        })
        .state('default.powers.list', {
            url: '/list?options',
            controller: 'default.powers',
            templateUrl: 'modules/powers/views/index.html',
            ncyBreadcrumb: {
                parent: 'default.home',
                label: 'Powers'
            }
        })
        .state('default.powers.edit', {
            url: '/:powerId/edit',
            controller: 'default.powers.edit',
            templateUrl: 'modules/powers/views/edit.html',
            abstract: true,
            ncyBreadcrumb: {
                parent:'default.powers.list',
                label: '{{ item.name }}'
            }
        })
        .state('default.powers.edit.details', {
            url: '/details',
            controller: 'default.powers.edit.details',
            templateUrl: 'modules/powers/views/details.html',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.powers.list'
            }
        })
        .state('default.powers.new', {
            url: '/new',
            controller: 'default.powers.edit.new',
            templateUrl: 'modules/powers/views/details.html',
            ncyBreadcrumb: {
                parent:'default.powers.list',
                label: 'New power'
            }
        })
        .state('default.powers.edit.delete', {
            url: '/delete',
            templateUrl: 'modules/powers/views/delete.html',
            controller: 'default.powers.edit.delete',
            ncyBreadcrumb: {
                label: '{{ item.name }}',
                parent: 'default.powers.edit.details'
            }
        })
        ;
    }
    ]);
};